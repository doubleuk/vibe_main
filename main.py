# import os
# os.system('chcp 65001')

import SuperReolution
import CleanMatte
import FusionSegmentation

if __name__ == '__main__':
    ## Launcher for All applications include in VIBE project

    # 0. Set arguments
    # For Clean Matte
    maskPath = r'X:\uuK_\JW2uuk\Clean_matte\P51_walkway\mask\resized_2'

    # For Fusion Segmentation
    imgPath = r'D:\FS_TEST_SET\TEST'
    outputPath = r'D:\FS_TEST_SET\TEST_RESULT'

    # Full TEST용
    # imgPath = r'D:\RUST_TEST\input'
    # outputPath = r'D:\RUST_TEST\output'

    # Sample TEST용
    imgPath = r"D:\MGD\input"
    outputPath = r'D:\MGD\output_test'

    # imgPath = r"D:\DRSTRNG\input\Doctor_Strange_1"
    # outputPath = r"D:\DRSTRNG\output\Doctor_Strange_1"

    # imgPath = r'D:\SAMPLE_TEST\input(147)'  # input, input(147)
    # outputPath = r'D:\SAMPLE_TEST\output'

    # imgPath = r"L:\4_Results\Cinema\RustCity\Segmentation"
    # outputPath = "L:/4_Results/Cinema/RustCity/Segmentation"

    # imgPath = "L:/4_Results/Cinema/RustCity/left_content"
    # outputPath = r"L:\4_Results\Cinema\RustCity\test"

    mode = 'FS'

    if mode == 'SR':
        # 1. Super-resolution
        SuperReolution.superResolution(imgPath, outputPath)
    elif mode == 'CM':
        # 2. Clean-matte
        CleanMatte.cleanMatte(imgPath, maskPath, outputPath)
    elif mode == 'FS':
        # 3. Fusion-Segmentation
        FusionSegmentation.fusionSegmentation(imgPath, outputPath, mode=0, save_mode=0, debug_mode=0, exe_mode=1)
        # USAGE READ -> FusionSegmentation/README.txt
