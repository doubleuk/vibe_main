import os
import cv2
import numpy as np
from PIL import Image


def read_images(path):
    imgs = []
    files_list = os.listdir(path)
    for file in files_list:
        name, ext = os.path.splitext(file)
        if ext == ".tif" or ext == ".tiff" or ext == ".png" or ext == ".jpg" or ext == ".jpeg":
            img_path = path + "/" + file
            bgr_img = cv2.imread(img_path, 1)
            rgb_img = cv2.cvtColor(bgr_img, cv2.COLOR_BGR2RGB)
            # rgb_img = Image.open(img_path).convert("RGB")
            h, w, _ = rgb_img.shape
            if h * w >= 100:
                imgs.append(rgb_img)
            else:
                tmp_img = np.zeros([120, 120, 3], dtype=np.uint8)
                tmp_img.fill(255)
                imgs.append(tmp_img)
    return imgs


def get_class_faces(faces_in_frames, rects_in_frames, num_face):
    classes_faces = []
    classes_rects = []
    for n in range(num_face):
        class_faces = []
        class_rects = []
        for faces_in_frame, rects_in_frame in zip(faces_in_frames, rects_in_frames):
            if n <= len(faces_in_frame) - 1:
                class_faces.append(faces_in_frame[n])
                class_rects.append(rects_in_frame[n])
        classes_faces.append(class_faces)
        classes_rects.append(class_rects)

    return classes_faces, classes_rects


def get_query_table_image(query_table, view_info=False):
    table_img = np.zeros((360, 600, 3), np.uint8)

    for idx, data in enumerate(query_table):
        if data is not None:
            yaw = data[0]
            pitch = data[1]
            face_img = data[2]
            score = data[3]
        else:
            yaw = "None"
            pitch = "None"
            face_img = np.zeros((120, 120, 3), np.uint8)
            score = 0

        resize_face = face_img
        face_h, face_w, _ = face_img.shape
        if face_h < 240 or face_w < 240:
            resize_face = cv2.resize(face_img, dsize=(120, 120), interpolation=cv2.INTER_LINEAR)
        elif face_h > 240 or face_w > 240:
            resize_face = cv2.resize(face_img, dsize=(120, 120), interpolation=cv2.INTER_AREA)
        elif face_h == 120 and face_w == 120:
            resize_face = face_img.copy()

        if view_info:
            cv2.putText(resize_face, str(yaw), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)
            cv2.putText(resize_face, str(pitch), (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)

        if idx > 4:
            px = idx % 5
            py = 1
            if idx > 9:
                py = 2
        else:
            px = idx
            py = 0

        x = px * 120
        y = py * 120
        table_img[y: y + 120, x: x + 120] = resize_face
        query_table_image = cv2.cvtColor(table_img, cv2.COLOR_RGB2BGR)

    return query_table_image


def view_query_tables(query_tables):
    for n, query_table in enumerate(query_tables):
        print("View Query Table : {}".format(n))
        table_img = np.zeros((360, 600, 3), np.uint8)
        for idx, data in enumerate(query_table):
            if data is not None:
                face_img = data[2]
            else:
                face_img = np.zeros((120, 120, 3), np.uint8)

            resize_face = face_img
            face_h, face_w, _ = face_img.shape
            if face_h < 240 or face_w < 240:
                resize_face = cv2.resize(face_img, dsize=(120, 120), interpolation=cv2.INTER_LINEAR)
            elif face_h > 240 or face_w > 240:
                resize_face = cv2.resize(face_img, dsize=(120, 120), interpolation=cv2.INTER_AREA)
            elif face_h == 120 and face_w == 120:
                resize_face = face_img.copy()

            if idx > 4:
                px = idx % 5
                py = 1
                if idx > 9:
                    py = 2
            else:
                px = idx
                py = 0

            x = px * 120
            y = py * 120
            table_img[y: y + 120, x: x + 120] = resize_face

        # cv2.namedWindow("Query Table {}".format(n))
        cv2.imshow("Query Table {}".format(n), cv2.cvtColor(table_img, cv2.COLOR_RGB2BGR))
        cv2.waitKey(0)
        cv2.destroyAllWindows()
