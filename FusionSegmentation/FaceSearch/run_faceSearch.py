import torch
import torchvision.transforms as transforms
from FaceSearch_utils.ddfa import ToTensor, Normalize
from model_building import SynergyNet
from FaceSearch_utils.inference import predict_sparseVert, draw_landmarks, predict_denseVert, predict_pose, draw_axis
import argparse
import torch.backends.cudnn as cudnn

import face_recognition
import shutil
import csv, json

from img_util import *
from face_util import *
from query_util import register_query_table, load_query_encoding, best_match_query_table
from folder_util import _make_imageFolderMap

cudnn.benchmark = True

# Following 3DDFA-V2, we also use 120x120 resolution
IMG_SIZE = 120
DEBUG = False

faceSearchPath = './FusionSegmentation/FaceSearch/'


def face_query_register(faces_data, query_table, angle_table):
    faces_imgs, faces_rects = faces_data
    new_query_table = None
    faces_infoes = []

    # Get Face Angles (Yaw, Pitch) -> Registering Face Query Table
    for faces, rects in zip(faces_imgs, faces_rects):
        # Face Angles Information = [[class id, yaw, pitch, face image], ...]
        faces_info = face_pose_estimation(faces, rects)

    ###### POSE ESTIMATION RESULT -> SMOOTHING CODE (~ING) #######
    #     faces_infoes.append(faces_info)
    #
    # normalized_faces_infoes = face_pose_normalize(faces_infoes)
    #
    # for faces_info in normalized_faces_infoes:
        for face_info in faces_info:
            # class_id = face_info[0]
            # query_table = query_tables[class_id]
            new_query_table = register_query_table(face_info, query_table, angle_table)
            # query_tables[class_id] = query_table

    if new_query_table is not None:
        if len(new_query_table) == new_query_table.count(None):
            new_query_table = None

    return new_query_table


def face_recognizer(images, query_tables):
    faces_imgs, faces_rects, num_face = get_face_information(images)
    cls_faces_imgs, cls_faces_rects = get_class_faces(faces_imgs, faces_rects, num_face)
    query_tables_encodings, query_tables_names = load_query_encoding(query_tables)

    update_list = []
    update_candidate_list = []
    register_list = False

    match_rate = None
    distance_rate = None

    for query_table_idx, query_encodings in enumerate(query_tables_encodings):
        for faces_idx, face_imgs in enumerate(cls_faces_imgs):
            match_score = 0
            distance_score = 0
            face_cnt = 0
            for face_img in face_imgs:
                if face_img is not None:
                    face_cnt += 1
                    face_h, face_w, _ = face_img.shape
                    face_location = [(0, face_w, face_h, 0)]
                    face_encodings = face_recognition.face_encodings(face_img, face_location, num_jitters=10)

                    for face_encoding in face_encodings:
                        matches = face_recognition.compare_faces(query_encodings, face_encoding, tolerance=0.58)
                        face_distance = face_recognition.face_distance(query_encodings, face_encoding)
                        best_match_idx = np.argmin(face_distance)
                        distance_cof = face_distance_to_conf(face_distance[best_match_idx])
                        if matches[best_match_idx]:
                            match_score += 1.0
                            distance_score += distance_cof

            if match_score == 0 or face_cnt == 0:
                match_rate = 0
            elif distance_score == 0 or face_cnt == 0:
                distance_rate = 0
            else:
                match_rate = round(match_score / face_cnt, 2)
                distance_rate = round(distance_score / face_cnt, 3)

            print("Update Query Table : ", query_table_idx, "/ match_rate : ", match_rate, "/ distance_rate : ", distance_rate)

            if match_rate >= 0.8:
                update_candidate_list.append([query_table_idx, distance_rate])
            else:
                register_list = True

    # Find Best Matched Query Table (This Query Table will be updated)
    update_query_table_idx = best_match_query_table(update_candidate_list)
    if update_query_table_idx is not None:
        update_list.append(update_query_table_idx)

    faces_data = [faces_imgs, faces_rects]

    return update_list, register_list, faces_data


def face_query_updater(query_table, faces_data, angle_table):
    # TODO : Query Table Update
    faces_imgs, faces_rects = faces_data
    updated_query_table = None

    for faces, rects in zip(faces_imgs, faces_rects):
        faces_info = face_pose_estimation(faces, rects)
        for face_info in faces_info:
            updated_query_table = register_query_table(face_info, query_table, angle_table)

    return updated_query_table


def move_image_file(image_path, sort_path, class_name):
    save_path = sort_path + "/" + class_name
    os.makedirs(save_path, exist_ok=True)
    image_name = str(os.path.basename(image_path))

    if os.listdir(sort_path):
        saved_image_names = os.listdir(save_path)
        if image_name in saved_image_names:
            new_image_name = str(int(len(saved_image_names)) + 1)
            copy_path = save_path + "/" + new_image_name
        else:
            copy_path = save_path + "/" + image_name
    else:
        copy_path = save_path + "/" + image_name

    print("Process Copy {} ---> {}".format(image_path, copy_path))
    shutil.copytree(image_path, copy_path)


def run(input_path, query_tables, query_table, angle_table, sort_path):
    if query_tables is None:
        query_tables = []

    # img_folders = os.listdir(input_path)
    img_folders = input_path
    hash_table = []
    for n, img_folder in enumerate(img_folders):
        empty_query_table = query_table.copy()
        # image_path = input_path + "/" + img_folder
        image_path = img_folder
        print("Processing : {} ({}/{})".format(image_path, n, len(img_folders) - 1))
        images = read_images(image_path)

        if not query_tables:  # Query Register (Initialize)
            faces_imgs, faces_rects, num_faces = get_face_information(images)
            faces_data = [faces_imgs, faces_rects]
            # query_tables = [query_table for i in range(num_faces)]
            new_query_table = face_query_register(faces_data, empty_query_table, angle_table)
            if new_query_table is not None:
                query_tables.append(new_query_table)

                # Person + table number 폴더로 이동 (Only 테스트용 - move_image_file())
                query_index = len(query_tables) - 1
                class_name = "Person_" + str(query_index)
                # move_image_file(image_path, sort_path, class_name)
                query_table_image = get_query_table_image(query_tables[0])

                np.save(sort_path + "/" + class_name, query_tables[0])
                cv2.imwrite(sort_path + "/" + class_name + ".png", query_table_image)

                hash_element = [class_name, image_path]
                hash_table.append(hash_element)

            if DEBUG:
                print("## Registered Initial Query Table ##")
                print("#  Registered Query table Count : {}".format(len(query_tables)))
                view_query_tables(query_tables)

        else:  # Query Update & New Face Register
            update_lists, register, faces_data = face_recognizer(images, query_tables)
            if update_lists:
                print("Update query table index : ", update_lists)
                # TODO : Update Query Table
                query_index = update_lists[0]
                print("Query Index : ", query_index)
                updated_query_table = face_query_updater(query_tables[query_index], faces_data, angle_table)

                if updated_query_table is not None:
                    query_tables[query_index] = updated_query_table

                    # Person + table number 폴더로 이동 (Only 테스트용 - move_image_file())
                    class_name = "Person_" + str(query_index)
                    # move_image_file(image_path, sort_path, class_name)
                    query_table_image = get_query_table_image(query_tables[query_index])
                    np.save(sort_path + "/" + class_name, query_tables[query_index])
                    cv2.imwrite(sort_path + "/" + class_name + ".png", query_table_image)

                    hash_element = [class_name, image_path]
                    hash_table.append(hash_element)

            elif register:
                print("New Face Detected Registering ...!")
                new_query_table = face_query_register(faces_data, empty_query_table, angle_table)
                if new_query_table is not None:
                    query_tables.append(new_query_table)

                # Person + table number 폴더로 이동 (Only 테스트용 - move_image_file())
                    query_index = len(query_tables) - 1
                    class_name = "Person_" + str(query_index)
                    # move_image_file(image_path, sort_path, class_name)
                    query_table_image = get_query_table_image(query_tables[query_index])
                    np.save(sort_path + "/" + class_name, query_tables[query_index])
                    cv2.imwrite(sort_path + "/" + class_name + ".png", query_table_image)

                    hash_element = [class_name, image_path]
                    hash_table.append(hash_element)

                if DEBUG:
                    print("## Registered New Query Table ##")
                    print("#  Registered Query table Count : {}".format(len(query_tables)))
                    view_query_tables(query_tables)

            else:
                print("Face is not Detection, Classification to Unknown")
                class_name = "Unknown"
                # move_image_file(image_path, sort_path, class_name)

                hash_element = [class_name, image_path]
                hash_table.append(hash_element)

    return hash_table

def get_classes_in_shot(output_path):
    csv_path = output_path + "/searching_result"
    f = open(csv_path + '/path_result.csv', 'r', encoding='utf-8')
    rdr = csv.reader(f)

    class_names = []
    shot_names = []
    shot_list = []
    for line in rdr:
        class_name, result_path = line
        shot_path = result_path.replace(output_path, "")
        shot_path = shot_path.split("\\")
        shot_name = shot_path[0]

        class_names.append(class_name)
        shot_names.append(shot_name)
        if shot_name not in shot_list:
            shot_list.append(shot_name)
    f.close()

    shot_in_classes = {}
    for shot in shot_list:
        labels = []
        for cls_name, shot_name in zip(class_names, shot_names):
            if shot == shot_name:
                if cls_name not in labels and cls_name != "Unknown":
                    labels.append(cls_name)
        shot_in_classes[shot] = labels
    # print(shot_in_classes)
    with open(csv_path + '/labels_in_scene.json', 'w') as f:
        json.dump(shot_in_classes, f)


def relabeling_data(output_path):
    csv_path = output_path + "/searching_result"
    f = open(csv_path + '/path_result.csv', 'r', encoding='utf-8')
    rdr = csv.reader(f)

    result_names = []
    class_names = []
    result_paths = []
    obj_info_paths = []
    for line in rdr:
        class_name, result_path = line
        result_class = result_path.split("\\")
        result_class = result_class[-1]
        shot_name = result_path.replace(output_path, "")
        shot_name = shot_name.split("\\")
        shot_name = shot_name[0]

        class_names.append(class_name)
        result_names.append(result_class)

        obj_info_path = output_path + "/" + shot_name
        result_paths.append(obj_info_path)
        if obj_info_path not in obj_info_paths:
            obj_info_paths.append(obj_info_path)
    f.close()

    obj_boxes = []
    obj_frames = []
    for obj_info_path in obj_info_paths:
        with open(obj_info_path + "/object_box.json", "r") as box_data:
            obj_box = json.load(box_data)
        with open(obj_info_path + "/object_frame.json", "r") as frame_data:
            obj_frame = json.load(frame_data)

        obj_boxes.append(obj_box)
        obj_frames.append(obj_frame)

    for cnt, obj_info_path in enumerate(obj_info_paths):
        obj_box = obj_boxes[cnt]
        obj_frame = obj_frames[cnt]
        new_datas = []
        for cls_name, result_name, result_path in zip(class_names, result_names, result_paths):
            # print(cls_name, result_name, result_path)
            if obj_info_path == result_path:
                if cls_name == result_name:
                    box_data = obj_box[str(result_name)]
                    frame_data = obj_frame[str(result_name)]
                    new_data = [result_name, box_data, frame_data]
                    new_data = new_data + frame_data
                else:
                    box_data = obj_box[str(result_name)]
                    frame_data = obj_frame[str(result_name)]
                    new_data = [cls_name, box_data, frame_data]
                    new_data = new_data + frame_data
                new_datas.append(new_data)

        with open(obj_info_path + '/labels_info.json', 'w') as f:
            json.dump(new_datas, f)

## TODO : FACE DETECTION 성능에 약간의 문제 발생, 얼굴과 유사한 물체 검출 --> 알고리즘 교체 혹은 예외처리 진행 필요
## TODO : FACE 뒷통수 관련 탐지, 예외 처리 관련 추가 필요


def main(image_path, result_path, query_tables, query_table, angle_table):
    result_hash_table = run(image_path, query_tables, query_table, angle_table, result_path)

    # # Convert List -> Numpy -> CSV file (Result File)
    # result = np.array(result_hash_table)
    # shot_name = os.path.basename(input_path)
    # np.savetxt(result_path + "/" + shot_name + "_sort.csv", result, fmt="%s", delimiter=",")

    return result_hash_table

if __name__ == '__main__':
    yaw_table = [-90, -45, 0, 45, 90]
    pitch_table = [-45, 0, 45]
    deviation = 15

    angle_table = [yaw_table, pitch_table, deviation]
    query_table = [None for i in range(len(yaw_table) * len(pitch_table))]

    # input_path = r"F:\Project\SeqNet\TEST_FACE_SEARCH\input"
    # sort_path = r"F:\Project\SeqNet\TEST_FACE_SEARCH\result"

    input_path = r"L:\4_Results\Cinema\RustCity\Segmentation\rus_2d_r02_v174_1119_201120_07mj_g_xyz(sx)"
    shot_name = os.path.basename(input_path)

    # Create FolderMap
    folderMap = []
    additional_forbidden_words = ["chroma_key", "mask"]
    input_folderMap = _make_imageFolderMap(input_path, folderMap, additional_forbidden_words)
    print("### Make Input FolderMap is Finished...!")

    sort_path = r"L:\4_Results\Cinema\RustCity\Segmentation\rus_2d_r02_v174_1119_201120_07mj_g_xyz(sx)\result"
    os.makedirs(sort_path, exist_ok=True)

    input_path = input_folderMap

    # input_path = r"L:\4_Results\Cinema\RustCity\Segmentation\rus_2d_r02_v174_1119_201120_07mj_g_xyz(sx)\003\rgba"

    result_hash_table = run(input_path, query_table, angle_table, sort_path)

    # Convert List -> Numpy -> CSV file (Result File)
    result = np.array(result_hash_table)
    np.savetxt(sort_path + "/" + shot_name + "_sort.csv", result, fmt="%s", delimiter=",")




