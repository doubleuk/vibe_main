import cv2


def IoU(box1, box2):
    # box = (x1, y1, x2, y2)
    box1_area = (box1[2] - box1[0] + 1) * (box1[3] - box1[1] + 1)
    box2_area = (box2[2] - box2[0] + 1) * (box2[3] - box2[1] + 1)

    # obtain x1, y1, x2, y2 of the intersection
    x1 = max(box1[0], box2[0])
    y1 = max(box1[1], box2[1])
    x2 = min(box1[2], box2[2])
    y2 = min(box1[3], box2[3])

    # compute the width and height of the intersection
    w = max(0, x2 - x1 + 1)
    h = max(0, y2 - y1 + 1)

    inter = w * h
    iou = inter / (box1_area + box2_area - inter)
    return iou


def check_overlap_bbox(box1, box2):
    # box => (top, right, bottom, left) == (y1, x2, y2, x1)
    box1_area = (box1[2] - box1[0] + 1) * (box1[1] - box1[3] + 1)
    box2_area = (box2[2] - box2[0] + 1) * (box2[1] - box2[3] + 1)

    # obtain x1, y1, x2, y2 of the intersection
    x1 = max(box1[3], box2[3])
    y1 = max(box1[0], box2[0])
    x2 = min(box1[1], box2[1])
    y2 = min(box1[2], box2[2])

    # compute the width and height of the intersection
    w = max(0, x2 - x1 + 1)
    h = max(0, y2 - y1 + 1)
    overlap_area = w * h

    overlap = None
    if overlap_area > 0:
        if overlap_area >= box1_area or overlap_area >= box2_area:
            if box1_area > box2_area:
                overlap = box2
            elif box1_area < box2_area:
                overlap = box1

    return overlap


def remove_overlap_bbox(boxes):
    overlap_idx = []
    for n, box in enumerate(boxes):
        box1 = box
        for i in range(n + 1, len(boxes)):
            box2 = boxes[i]
            overlap = check_overlap_bbox(box1, box2)
            if overlap is not None:
                if overlap == box1:
                    # if n not in overlap_idx:
                    overlap_idx.append(n)
                elif overlap == box2:
                    # if i not in overlap_idx:
                    overlap_idx.append(i)

    for idx in overlap_idx:
        boxes[idx] = None

    new_boxes = []
    for box in boxes:
        if box is not None:
            new_boxes.append(box)

    return new_boxes

def sort_bbox(pre_boxes, new_box):
    max_iou = 0
    tmp = 0
    for n, pre_box in enumerate(pre_boxes):
        if pre_box is not None:
            iou = IoU(pre_box, new_box)
            if iou > max_iou:
                max_iou = iou
                tmp = n

    return tmp


def convert_box_format(face_locations):
    rects = []
    for face_location in face_locations:
        ymin, xmax, ymax, xmin = face_location
        bbox = [xmin, ymin, xmax, ymax]
        rects.append(bbox)
    return rects


def convert_location_format(face_locations):
    rects = []
    for face_location in face_locations:
        xmin, ymin, xmax, ymax = face_location
        bbox = [ymin, xmax, ymax, xmin]
        rects.append(bbox)
    return rects


def get_face_image(img, box):
    xmin, ymin, xmax, ymax, score = box
    crop_img = img[int(ymin): int(ymax), int(xmin): int(xmax)]
    return crop_img


def close_target_angle(target_angle_faces, angle, margin):
    select_img = None
    min_gap_angle = margin * 2
    if target_angle_faces is not None:
        for target_angle_face in target_angle_faces:
            yaws, pitch, face_img = target_angle_face

            # print("Yaw : ", yaws)
            # print("Pitch : ", pitch)

            gap_yaw_angle = abs(angle[0] - yaws)
            gap_pitch_angle = abs(angle[1] - pitch)

            gap_angle = gap_yaw_angle + gap_pitch_angle

            if min_gap_angle > gap_angle:
                min_gap_angle = gap_angle
                select_img = face_img.copy()
                select_yaws = yaws
                select_pitch = pitch

    select_face = [select_yaws, select_pitch, select_img]
    return select_face


def sort_face_angle(class_faces, target_angle, margin):
    target_angle_faces = []
    for cn, class_face in enumerate(class_faces):
        yaws, pitch, face_img = class_face
        target_yaw, target_pitch = target_angle

        if target_yaw - margin <= yaws <= target_yaw + margin and target_pitch - margin < pitch < target_pitch + margin:
            target_angle_faces.append([yaws, pitch, face_img])

    if not target_angle_faces:
        target_angle_face = None
    else:
        target_angle_face = close_target_angle(target_angle_faces, target_angle, margin)
    return target_angle_face


def get_direct(angle_info):
    if isinstance(angle_info, list):
        yaw, pitch, _ = angle_info
        direct = None
        if yaw == 90 and pitch == 45: direct = 0
        if yaw == 45 and pitch == 45: direct = 1
        if yaw == 0 and pitch == 45: direct = 2
        if yaw == -45 and pitch == 45: direct = 3
        if yaw == -90 and pitch == 45: direct = 4

        if yaw == 90 and pitch == 0: direct = 5
        if yaw == 45 and pitch == 0: direct = 6
        if yaw == 0 and pitch == 0: direct = 7
        if yaw == -45 and pitch == 0: direct = 8
        if yaw == -90 and pitch == 0: direct = 9

        if yaw == 90 and pitch == -45: direct = 10
        if yaw == 45 and pitch == -45: direct = 11
        if yaw == 0 and pitch == -45: direct = 12
        if yaw == -45 and pitch == -45: direct = 13
        if yaw == -90 and pitch == -45: direct = 14

        return direct


def get_face_angle_query(class_faces, class_num, target_yaw_angles, target_pitch_angle, margin_value):
    target_angles_faces = []
    for yaw in target_yaw_angles:
        for pitch in target_pitch_angle:
            angle = [yaw, pitch]
            direct = get_direct(angle)
            target_angle_face = sort_face_angle(class_faces, angle, margin_value)

            if target_angle_face is not None:
                target_angle_face = [direct, target_angle_face]
            else:
                target_angle_face = [direct, None]

            target_angles_faces.append(target_angle_face)

    return target_angles_faces


def get_class_info(frames_faces, class_num):
    class_faces = []
    for frame_faces in frames_faces:
        for face_info in frame_faces:
            face_idx, yaws, pitch, face_img = face_info
            if class_num == face_idx:
                new_face_info = [yaws, pitch, face_img]

                class_faces.append(new_face_info)

    return class_faces


def get_candidate_score(candidate, target_angle):
    # Normalize Angle Score (Close to Target Angle)
    target_yaw, target_pitch, thresh = target_angle

    # Angle Score Calculate
    yaw = candidate[0]
    pitch = candidate[1]

    yaw_score = (thresh - abs(abs(target_yaw) - abs(yaw))) / thresh * 1.0
    pitch_score = (thresh - abs(abs(target_pitch) - abs(pitch))) / thresh * 1.0
    angle_score = (yaw_score + pitch_score) / 2

    # Normalize Size Score (120px size) -> Ref. Pose Estimation Size
    max_size = 120 * 120

    # Size Score Calculate
    img = candidate[2]
    h, w, _ = img.shape
    size = h * w
    if size >= 120 * 120:
        size = 120 * 120

    size_score = size / max_size * 1.0

    candidate_score = round((size_score + angle_score) / 2, 2)

    return candidate_score
