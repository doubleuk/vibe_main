### Face Search Installation ###

1. REQUIREMENT Libs
    - Pytorch (1.0+ version) : Pytorch Version Able Check == 1.7.0, 1.7.1
    - TorchVision
    - OpenCV
    - Scipy
    - Matplotlib
    - Cython  # Require to Install Sim3DR (in FACE POSE ESTIMATION)

    ### Required Libs Install ###
    - conda install pytorch==1.7 torchvision cudatoolkit=11.0 -c pytorch
    - pip install opencv-python scipy matplotlib Cython

2. FACE RECOGNITION Installation
    ### Before Installation ###
    - Install cmake (add cmake to path for all users)
      (Download Link : https://cmake.org/download/)

    ### Recommendation Install ###
    - 가상환경 설치 경로 이동 (python.exe 위치)
       ex) C:\USER\VIBE\anaconda3\envs\segtools
           (현재 가상환경 이름이 segtools 인 경우)
    - python -m pip install dlib
    - python -m pip install face_recognition

    ### Easy Install ###
    (Not Recommend, Error Occurs not expected)
    - pip install dilb
    - pip install face_recognition

3. FACE POSE ESTIMATION Installation

    ### Sim3DR Install ###
    - Face_Search SourceCode Root 이동
    - Sim3DR 폴더로 이동 => cd Sim3DR
    - python setup.py build_ext --inplace

    ### FaceBoxes Install (Not Required) ###
    * 공식적으로 FaceBoxes 설치 필요하나, Windows 지원 불가 확인
    * 기존 사용하던 FaceBoxes 모듈 역할 -> Face Recognition 라이브러리 내 Face Location 모듈로 대체
      (Face Detection 기능 수행)
    * 현재 Face_Search 내에서 FaceBoxes 설치 불필요
