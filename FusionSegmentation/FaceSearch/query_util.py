import cv2
import face_recognition
import os
from PIL import Image
import numpy as np

from box_util import get_direct, get_candidate_score
from face_util import face_pose_estimation


# 1. Register Query Table
def register_query_table(face_info, query_table, angle_table):
    _, yaw, pitch, face = face_info
    table_idx, table_angle = get_candidate_idx(angle_table, face_info)
    if table_idx is not None:
        if query_table[table_idx] is None:
            candidate = [yaw, pitch, face]
            score = get_candidate_score(candidate, table_angle)
            query_table[table_idx] = [yaw, pitch, face, score]
            # print("Registered in query table ...!")
        else:
            candidate = query_table[table_idx]
            new_candidate = [yaw, pitch, face]
            score = get_candidate_score(new_candidate, table_angle)
            new_candidate = [yaw, pitch, face, score]
            selected_candidate = select_candidate(candidate, new_candidate)
            query_table[table_idx] = selected_candidate
            # print("Registered in query table ...!")

    return query_table


def get_candidate_idx(angle_table, face_info):
    yaw_table, pitch_table, thresh = angle_table
    # face_info => [idx, yaw, pitch, face_img]

    candidate_idx = None
    angle = None
    for yaw in yaw_table:
        candidate = check_angle(face_info[1], yaw, thresh)
        if candidate is True:
            for pitch in pitch_table:
                candidate = check_angle(face_info[2], pitch, thresh)
                if candidate is True:
                    angle = [yaw, pitch, thresh]
                    direct = get_direct(angle)
                    candidate_idx = direct

    return candidate_idx, angle


def check_angle(face_angle, target_angle, margin):
    candidate = None
    if target_angle - margin <= face_angle <= target_angle + margin:
        candidate = True
    return candidate


def select_candidate(candidate, new_candidate):
    candidate_score = candidate[3]
    new_candidate_score = new_candidate[3]

    if candidate_score > new_candidate_score:
        selected_candidate = candidate
    elif candidate_score < new_candidate_score:
        selected_candidate = new_candidate
    else:
        selected_candidate = candidate

    return selected_candidate


# 2. Update Query Table
def update_query_table(update_lists, query_tables, face_images, angle_table):
    for update_list in update_lists:
        query_idx, gallery_idx = update_list

        new_faces = face_images[gallery_idx]
        new_rects = face_images[gallery_idx]

        faces_info = face_pose_estimation(new_faces, new_rects)  # [[Class id, yaw, pitch, face img] ..]

        for face_info in faces_info:
            yaw = face_info[1]
            pitch = face_info[2]
            face = face_info[3]

            query_table = query_tables[query_idx]
            query_table = register_query_table(face_info, query_table, angle_table)
            query_tables[query_idx] = query_table


def load_query_encoding(query_tables):
    query_tables_encodings = []
    query_tables_names = []

    for idx, query_table in enumerate(query_tables):
        class_name = "Person_" + str(idx).zfill(3)
        query_encodings = []
        query_names = []

        for query_data in query_table:
            if query_data is not None:
                query_face = query_data[2]
                face_h, face_w, _ = query_face.shape
                query_face_location = [(0, face_w, face_h, 0)]
                query_face_encoding = face_recognition.face_encodings(query_face, query_face_location,
                                                                      num_jitters=10)[0]
                query_encodings.append(query_face_encoding)
                query_names.append(class_name)

        query_tables_encodings.append(query_encodings)
        query_tables_names.append(query_names)

    return query_tables_encodings, query_tables_names


def best_match_query_table(candidate_update_list):
    max_distance_rate = 0
    best_query_table_idx = None
    for candidate in candidate_update_list:
        query_table_idx, distance_rate = candidate
        if max_distance_rate < distance_rate:
            max_distance_rate = distance_rate
            best_query_table_idx = query_table_idx

    print("Best Matched Query Table Index : ", best_query_table_idx)
    if max_distance_rate != 0:
        print("Distance Score : ", max_distance_rate)

    return best_query_table_idx


def read_query_tables(query_table_datas_path):
    print("Read Query Tables ...")

    query_tables = []
    image_files = os.listdir(query_table_datas_path)
    for image_file in image_files:
        _, ext = os.path.splitext(image_file)
        if ext == ".npy":
            data_path = query_table_datas_path + "/" + image_file
            print(data_path)
            query_table = np.load(data_path, allow_pickle=True)
            query_tables.append(query_table)

    return query_tables

# def read_query_tables(query_table_images_path):
#     print("Read Query Tables ...")
#
#     query_tables = []
#     image_files = os.listdir(query_table_images_path)
#     for image_file in image_files:
#         _, ext = os.path.splitext(image_file)
#         if ext == ".png" or ext == ".jpg" or ext == ".tif" or ext == ".jpeg":
#             image_path = query_table_images_path + "/" + image_file
#             image = Image.open(image_path)
#             np_img = np.array(image)
#             h, w, _ = np_img.shape
#
#             row = int(h / 120)
#             col = int(w / 120)
#
#             query_table = []
#             # Load Query Table Image in row & col
#             for r in range(1, row + 1):
#                 for c in range(1, col + 1):
#                     x1 = c * 120
#                     y1 = r * 120
#                     x0 = x1 - 120
#                     y0 = y1 - 120
#
#                     crop_area = (x0, y0, x1, y1)
#                     crop_img = image.crop(crop_area)
#
#                     # Image is Black => None
#                     extrema = crop_img.convert("L").getextrema()
#                     if extrema == (0, 0) or extrema == (1, 1):
#                         query_table.append(None)
#                     else:
#                         query_image = np.array(crop_img)
#                         query_table.append(query_image)
#             query_tables.append(query_table)
#
#     return query_tables
