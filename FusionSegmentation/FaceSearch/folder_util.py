from os import listdir
from os.path import isfile, join
import os


def _getCurDirName(path):
    return (os.path.split(os.path.abspath(path)))[1]


def _IsThisImageFile(ext):
    imageFileExts = {".png", ".jpg", ".tif", ".tiff", ".TIF", ".TIFF"}
    flag = False
    if ext in imageFileExts:
        flag = True
    return flag


def _make_imageFolderMap(path, folderMap, additional_forbidden_words=[]):
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    onlyfolders = [f for f in listdir(path) if not isfile(join(path, f))]

    forbidden_words = []

    for forbidden_key in additional_forbidden_words:
        forbidden_words.append(forbidden_key)

    flag = False
    if len(onlyfiles) != 0:
        flag = True
        for filename in onlyfiles:
            if filename == 'Thumbs.db':
                continue

            _, ext = os.path.splitext(filename)
            flag = flag and _IsThisImageFile(ext)

        if flag:
            # image folder has only image files
            folderName = _getCurDirName(path)

            # forbidden words check for image folder
            forbidden_flag = False
            for forbidden_word in forbidden_words:
                if forbidden_word in folderName:
                    forbidden_flag = True
                    break
            if not forbidden_flag:
                print('[%03d] add tartget folder : %s' % (len(folderMap) + 1, path))
                folderMap.append(path)

    if not flag:
        if len(onlyfolders) != 0:
            for folderPath in onlyfolders:
                # forbidden words check for main folders
                folderName = folderPath
                forbidden_flag = False
                for forbidden_word in forbidden_words:
                    if forbidden_word in folderName:
                        forbidden_flag = True
                        break
                if not forbidden_flag:
                    _make_imageFolderMap(join(path, folderPath), folderMap, forbidden_words)

    return folderMap
