import cv2
import os, glob
import numpy as np
import argparse

image_path = 'D:/ALTER/JENNIE_SOLO/solo_source/01/'
save_path = 'D:/ALTER/JENNIE_SOLO/solo_source/01-hist-2.0/'
cnt = 0
# for filename in glob.glob(image_path + '*.png'):
#     img = cv2.imread(filename)
#     # img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
#     img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YCrCb)22
#     ycrcb = cv2.split(img_yuv)
#     # cv2.imwrite(save_path + "{:05d}.png".format(cnt), img_yuv)
#
#     # img_yuv[:, :, 0] = cv2.equalizeHist(img_yuv[:, :, 0])
#     ycrcb[0] = cv2.equalizeHist(ycrcb[0])
#     dst_ycrcb = cv2.merge(ycrcb)
#     img_rgb = cv2.cvtColor(dst_ycrcb, cv2.COLOR_YUV2BGR)
#
#     cv2.imwrite(save_path + "{:05d}.png".format(cnt), img_rgb)
#     cnt +=1

def adjust_gamma(image, gamma=1.0):
    # build a lookup table mapping the pixel values [0, 255] to
    # their adjusted gamma values
	invGamma = 1.0 / gamma
	table = np.array([((i / 255.0) ** invGamma) * 255
		for i in np.arange(0, 256)]).astype("uint8")
	# apply gamma correction using the lookup table
	return cv2.LUT(image, table)

for filename in glob.glob(image_path + '*.png'):
    original = cv2.imread(filename)

    adjusted = adjust_gamma(original, gamma=2.0)

    cv2.imwrite(save_path + "{:05d}.png".format(cnt), adjusted)
    cnt += 1

    # # loop over various values of gamma
    # for gamma in np.arange(0.0, 3.5, 0.5):
    #     # ignore when gamma is 1 (there will be no change to the image)
    #     if gamma == 1:
    #         continue
    #     # apply gamma correction and show the images
    #     gamma = gamma if gamma > 0 else 0.1
    #     adjusted = adjust_gamma(original, gamma=gamma)
    #     cv2.putText(adjusted, "g={}".format(gamma), (10, 30),
    #                 cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 3)
    #     cv2.imshow("Images", np.hstack([original, adjusted]))
    #     cv2.waitKey(0)


