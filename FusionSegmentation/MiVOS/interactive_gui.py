
"""
Based on https://github.com/seoungwugoh/ivs-demo

The entry point for the user interface
It is terribly long... GUI code is hard to write!
"""

fusionSegmentationPath = './FusionSegmentation/MiVOS/'

import sys
import os
from os import path
import time
import functools
from argparse import ArgumentParser

import json
import cv2
from PIL import Image
import numpy as np
import torch
from collections import deque
from copy import deepcopy
import datetime

from PyQt5.QtWidgets import (QWidget, QApplication, QMainWindow, QComboBox, QGridLayout, 
    QGroupBox, QHBoxLayout, QLabel, QMenu, QMenuBar, QPushButton, QTextEdit, 
    QPlainTextEdit, QVBoxLayout, QAction, QSizePolicy, QButtonGroup, QSlider, 
    QLCDNumber, QShortcut, QRadioButton, QProgressBar, QFileDialog, qApp)

from PyQt5.QtGui import QIcon, QPixmap, QPainter, QColor, QKeySequence, QImage, QTextCursor
from PyQt5.QtCore import Qt, QTimer 
from PyQt5 import QtCore

from inference_core import InferenceCore
from interact.s2m_controller import S2MController
from interact.fbrs_controller import FBRSController
from model.propagation.prop_net import PropagationNetwork
from model.fusion_net import FusionNet
from model.s2m.s2m_network import deeplabv3plus_resnet50 as S2M
from model.aggregate import aggregate_sbg, aggregate_wbg
from util.tensor_util import pad_divide_by, unpad, unpad_3dim, compute_multi_class_iou_both_idx
from util.palette import pal_color_map, get_binary_map

from interact.interactive_utils import *
from interact.interaction import *
from interact.timer import Timer
from interact.connected_component import *
from interact.instance import *
from interact.labeling import *

torch.set_grad_enabled(False)

# DAVIS palette
palette = pal_color_map()

from addon.AdelaiDet import *


class App(QWidget):
    # def __init__(self, prop_net, fuse_net, s2m_ctrl:S2MController, fbrs_ctrl:FBRSController, masks, num_objects, mem_freq, imgPath, outputPath, mode, save_mode, debug_mode, exe_mode):
    def __init__(self, prop_net, fuse_net, s2m, fbrs, masks, num_objects, mem_freq, imgPath, outputPath, mode, save_mode, debug_mode, exe_mode):
    # def __init__(self, masks, num_objects, mem_freq, imgPath, outputPath, mode, save_mode, debug_mode, exe_mode):
        super().__init__()

        # Loads the images/masks
        # folder_path = str(QFileDialog.getOpenFileName(self, "Select Video or Image", './'))

        # input_path = QFileDialog.getOpenFileName(self, "Select Video or Image", './')
        # images = select_files(input_path[0])
        # images = select_files('T:/MGD/Clip/RND_TST_09.mov')

        print("[Selected Options]")
        if mode == 0:  # Non-GUI Mode
            print("- Non GUI Mode")
            self.mode = 0
            self.hide()
        elif mode == 1:  # Display Mode
            print("- Display Mode")
        elif mode == 2:  # GUI Mode
            print("- GUI Mode")

        if debug_mode == 0:
            self.debug_mode = False
            print("- Debug Mode : Off")
        elif debug_mode == 1:
            self.debug_mode = True
            print("- Debug Mode : On")

        if save_mode == 0:
            print("- Save Mode : Single Object")
        elif save_mode == 1:
            print("- Save Mode : All Object")
        elif save_mode == 2:
            print("- Save Mode : Single / All Object")

        self.modelInfo = [prop_net, fuse_net, s2m, fbrs]
        self.mem_freq = mem_freq

        self.selected_class = ["person"]

        if mode == 0 and exe_mode == 1:
            self.user_timer = None
            self.progress = None
            self.console_push_text = None
            self.user_timer = None

            folder_outputPath = outputPath
            os.makedirs(outputPath, exist_ok=True)

            images = load_images(imgPath, None)
            print("Image Loading Finished")

            self.images = images
            self.torch_images = images_to_torch(self.images, device='cpu')
            self.num_frames, self.height, self.width = self.images.shape[:3]
            self.masks = masks
            self.outputPath = folder_outputPath
            self.save_mode = save_mode
            self.num_objects = num_objects
            self.trackingInfo = None
            self.labelInfo = None

            if self.masks is not None:
                self.current_mask = self.masks
                # IOU computation
                self.ious = np.zeros(self.num_frames)
                self.iou_curve = []
            else:
                self.current_mask = np.zeros((self.num_frames, self.height, self.width), dtype=np.uint8)

            # Instance Segmentation 진행
            instance_s = time.time()
            self.run_instanceSeg()
            instance_time = time.time() - instance_s

            if self.current_mask:
                # Tracking 진행
                tracking_s = time.time()
                self.run_trackingObj()
                self.save_tracking_info()
                tracking_time = time.time() - tracking_s

                # Video Segmentation 진행
                propagate_s = time.time()
                self.run_propagate()
                propagate_time = time.time() - propagate_s

                record_time = np.array([f"{instance_time:.5f} sec", f"{tracking_time:.5f} sec", f"{propagate_time:.5f} sec"])
                np.savetxt(self.outputPath + "/seg_time_record.txt", record_time, delimiter=" ", fmt="%s")
                print("Fusion Segmentation Finished ...!")
                pass

        else:
            img_folders = os.listdir(imgPath)
            for n, img_folder in enumerate(img_folders):
                folder_imgPath = imgPath + '/' + img_folder
                folder_outputPath = outputPath + '/' + img_folder
                os.makedirs(folder_outputPath, exist_ok=True)

                images = load_images(folder_imgPath, None)
                print("Image Loading Finished")

                self.images = images
                self.num_frames, self.height, self.width = self.images.shape[:3]
                self.masks = masks
                self.outputPath = folder_outputPath
                self.save_mode = save_mode
                self.num_objects = num_objects
                self.trackingInfo = None
                self.labelInfo = None

                with torch.cuda.amp.autocast(enabled=True):
                    # Load our checkpoint
                    prop_saved = torch.load(prop_net)
                    prop_model = PropagationNetwork().cuda().eval()
                    prop_model.load_state_dict(prop_saved)

                    fusion_saved = torch.load(fuse_net)
                    fusion_model = FusionNet().cuda().eval()
                    fusion_model.load_state_dict(fusion_saved)

                    # Loads the S2M model
                    if s2m is not None:
                        s2m_saved = torch.load(s2m)
                        s2m_model = S2M().cuda().eval()
                        s2m_model.load_state_dict(s2m_saved)
                    else:
                        s2m_model = None

                    s2m_controller = S2MController(s2m_model, self.num_objects, ignore_class=255)
                    if fbrs is not None:
                        fbrs_controller = FBRSController(fbrs)
                    else:
                        fbrs_controller = None

                    self.s2m_controller = s2m_controller
                    self.fbrs_controller = fbrs_controller

                    self.processor = InferenceCore(prop_model, fusion_model, images_to_torch(self.images, device='cpu'),
                                                       self.num_objects, mem_freq=self.mem_freq, mem_profile=0)

                if self.masks is not None:
                    self.current_mask = self.masks
                    # IOU computation
                    self.ious = np.zeros(self.num_frames)
                    self.iou_curve = []
                else:
                    self.current_mask = np.zeros((self.num_frames, self.height, self.width), dtype=np.uint8)

                # set window
                self.setWindowTitle('Segmentation Support Tool')
                self.setGeometry(100, 100, self.width, self.height+100)

                # some buttons
                self.play_button = QPushButton('Play/Stop')
                self.play_button.clicked.connect(self.on_play)
                self.run_button = QPushButton('Propagate')
                self.run_button.clicked.connect(self.on_run)
                self.run_with_mask_button = QPushButton('Load mask')
                self.run_with_mask_button.clicked.connect(self.on_run_with_mask)
                self.instance_seg_button = QPushButton('Instance')
                self.instance_seg_button.clicked.connect(self.run_instance)
                self.commit_button = QPushButton('Commit')
                self.commit_button.clicked.connect(self.on_commit)
                self.undo_button = QPushButton('Undo')
                self.undo_button.clicked.connect(self.on_undo)
                self.reset_button = QPushButton('Reset Frame')
                self.reset_button.clicked.connect(self.on_reset)
                self.save_button = QPushButton('Save')
                self.save_button.clicked.connect(self.save)
                self.save_binary_button = QPushButton('Binary')
                self.save_binary_button.clicked.connect(self.binary_mask)

                # LCD
                self.lcd = QTextEdit()
                self.lcd.setReadOnly(True)
                self.lcd.setMaximumHeight(28)
                self.lcd.setMaximumWidth(120)
                self.lcd.setText('{: 4d} / {: 4d}'.format(0, self.num_frames-1))

                # timeline slider
                self.tl_slider = QSlider(Qt.Horizontal)
                self.tl_slider.valueChanged.connect(self.tl_slide)
                self.tl_slider.setMinimum(0)
                self.tl_slider.setMaximum(self.num_frames-1)
                self.tl_slider.setValue(0)
                self.tl_slider.setTickPosition(QSlider.TicksBelow)
                self.tl_slider.setTickInterval(1)

                # brush size slider
                self.brush_label = QLabel()
                self.brush_label.setAlignment(Qt.AlignCenter)
                self.brush_label.setMinimumWidth(100)

                self.brush_slider = QSlider(Qt.Horizontal)
                self.brush_slider.valueChanged.connect(self.brush_slide)
                self.brush_slider.setMinimum(1)
                self.brush_slider.setMaximum(100)
                self.brush_slider.setValue(3)
                self.brush_slider.setTickPosition(QSlider.TicksBelow)
                self.brush_slider.setTickInterval(2)
                self.brush_slider.setMinimumWidth(300)

                # combobox
                self.combo = QComboBox(self)
                self.combo.addItem("davis")
                self.combo.addItem("fade")
                self.combo.addItem("light")
                self.combo.currentTextChanged.connect(self.set_viz_mode)

                # Radio buttons for type of interactions
                self.curr_interaction = 'Click'
                self.interaction_group = QButtonGroup()
                self.radio_fbrs = QRadioButton('Click')
                self.radio_s2m = QRadioButton('Scribble')
                self.radio_free = QRadioButton('Free')
                self.interaction_group.addButton(self.radio_fbrs)
                self.interaction_group.addButton(self.radio_s2m)
                self.interaction_group.addButton(self.radio_free)
                self.radio_fbrs.toggled.connect(self.interaction_radio_clicked)
                self.radio_s2m.toggled.connect(self.interaction_radio_clicked)
                self.radio_free.toggled.connect(self.interaction_radio_clicked)
                self.radio_fbrs.toggle()

                # Main canvas -> QLabel
                self.main_canvas = QLabel()
                self.main_canvas.setSizePolicy(QSizePolicy.Expanding,
                        QSizePolicy.Expanding)
                self.main_canvas.setAlignment(Qt.AlignCenter)
                self.main_canvas.setMinimumSize(100, 100)

                self.main_canvas.mousePressEvent = self.on_press
                self.main_canvas.mouseMoveEvent = self.on_motion
                self.main_canvas.setMouseTracking(True) # Required for all-time tracking
                self.main_canvas.mouseReleaseEvent = self.on_release

                # Minimap -> Also a QLbal
                self.minimap = QLabel()
                self.minimap.setSizePolicy(QSizePolicy.Expanding,
                        QSizePolicy.Expanding)
                self.minimap.setAlignment(Qt.AlignTop)
                self.minimap.setMinimumSize(100, 100)

                # Zoom-in buttons
                self.zoom_p_button = QPushButton('Zoom +')
                self.zoom_p_button.clicked.connect(self.on_zoom_plus)
                self.zoom_m_button = QPushButton('Zoom -')
                self.zoom_m_button.clicked.connect(self.on_zoom_minus)
                self.finish_local_button = QPushButton('Finish Local')
                self.finish_local_button.clicked.connect(self.on_finish_local)
                self.finish_local_button.setDisabled(True)

                # Console on the GUI
                self.console = QPlainTextEdit()
                self.console.setReadOnly(True)
                self.console.setMinimumHeight(100)
                self.console.setMaximumHeight(100)

                # progress bar
                self.progress = QProgressBar(self)
                self.progress.setGeometry(0, 0, 300, 25)
                self.progress.setMinimumWidth(300)
                self.progress.setMinimum(0)
                self.progress.setMaximum(100)
                self.progress.setFormat('Idle')
                self.progress.setStyleSheet("QProgressBar{color: black;}")
                self.progress.setAlignment(Qt.AlignCenter)

                # navigator
                navi = QHBoxLayout()
                navi.addWidget(self.lcd)
                navi.addWidget(self.play_button)

                interact_subbox = QVBoxLayout()
                interact_topbox = QHBoxLayout()
                interact_botbox = QHBoxLayout()
                interact_topbox.setAlignment(Qt.AlignCenter)
                interact_topbox.addWidget(self.radio_s2m)
                interact_topbox.addWidget(self.radio_fbrs)
                interact_topbox.addWidget(self.radio_free)
                interact_topbox.addWidget(self.brush_label)
                interact_botbox.addWidget(self.brush_slider)
                interact_subbox.addLayout(interact_topbox)
                interact_subbox.addLayout(interact_botbox)
                navi.addLayout(interact_subbox)

                navi.addStretch(1)
                navi.addWidget(self.undo_button)
                navi.addWidget(self.reset_button)

                navi.addStretch(1)
                navi.addWidget(self.progress)
                navi.addWidget(QLabel('Overlay Mode'))
                navi.addWidget(self.combo)
                navi.addStretch(1)
                navi.addWidget(self.commit_button)
                navi.addWidget(self.run_button)
                navi.addWidget(self.run_with_mask_button)
                navi.addWidget(self.instance_seg_button)
                navi.addWidget(self.save_button)
                navi.addWidget(self.save_binary_button)

                # Drawing area, main canvas and minimap
                draw_area = QHBoxLayout()
                draw_area.addWidget(self.main_canvas, 4)

                # Minimap area
                minimap_area = QVBoxLayout()
                minimap_area.setAlignment(Qt.AlignTop)
                mini_label = QLabel('Minimap')
                mini_label.setAlignment(Qt.AlignTop)
                minimap_area.addWidget(mini_label)
                # Minimap zooming
                minimap_ctrl = QHBoxLayout()
                minimap_ctrl.setAlignment(Qt.AlignTop)
                minimap_ctrl.addWidget(self.zoom_p_button)
                minimap_ctrl.addWidget(self.zoom_m_button)
                minimap_ctrl.addWidget(self.finish_local_button)
                minimap_area.addLayout(minimap_ctrl)
                minimap_area.addWidget(self.minimap)
                minimap_area.addWidget(self.console)

                draw_area.addLayout(minimap_area, 1)

                layout = QVBoxLayout()
                layout.addLayout(draw_area)
                layout.addWidget(self.tl_slider)
                # layout.addWidget(self.tl_slider2)
                layout.addLayout(navi)
                self.setLayout(layout)

                # timer
                self.timer = QTimer()
                self.timer.setSingleShot(False)
                self.timer.timeout.connect(self.on_time)

                # Local mode related states
                self.ctrl_key = False
                self.in_local_mode = False
                self.local_bb = None
                self.local_interactions = {}
                self.this_local_interactions = []
                self.local_interaction = None

                # initialize visualization
                self.viz_mode = 'davis'

                self.vis_map = np.zeros((self.height, self.width, 3), dtype=np.uint8)
                self.vis_alpha = np.zeros((self.height, self.width, 1), dtype=np.float32)
                self.brush_vis_map = np.zeros((self.height, self.width, 3), dtype=np.uint8)
                self.brush_vis_alpha = np.zeros((self.height, self.width, 1), dtype=np.float32)
                self.vis_hist = deque(maxlen=100)
                self.cursur = 0
                self.on_showing = None

                # initialize local visualization (which is mostly unknown at this point)
                self.local_vis_map = None
                self.local_vis_alpha = None
                self.local_brush_vis_map = None
                self.local_brush_vis_alpha = None
                self.local_vis_hist = deque(maxlen=100)

                # Zoom parameters
                self.zoom_pixels = 150

                # initialize action
                self.interactions = {}
                self.interactions['interact'] = [[] for _ in range(self.num_frames)]
                self.interactions['annotated_frame'] = []
                self.this_frame_interactions = []
                self.interaction = None
                self.reset_this_interaction()
                self.pressed = False
                self.right_click = False
                self.ctrl_size = False
                self.current_object = 1
                self.last_ex = self.last_ey = 0

                # Objects shortcuts
                for i in range(1, num_objects+1):
                    QShortcut(QKeySequence(str(i)), self).activated.connect(functools.partial(self.hit_number_key, i))

                # <- and -> shortcuts
                QShortcut(QKeySequence(Qt.Key_Left), self).activated.connect(self.on_prev)
                QShortcut(QKeySequence(Qt.Key_Right), self).activated.connect(self.on_next)

                # Mask saving
                # QShortcut(QKeySequence('s'), self).activated.connect(self.save)
                # QShortcut(QKeySequence('l'), self).activated.connect(self.debug_pressed)

                self.interacted_mask = None
                self.show_current_frame()

            print("[Selected Options]")
            if mode != 0:  # Not Non-GUI Mode
                if mode == 1:  # Display Mode
                    print("- Display Mode")
                    self.setWindowTitle('Segmentation Support Tool (Display Mode)')
                    self.show()
                elif mode == 2:  # GUI Mode
                    print("- GUI Mode")
                    self.setWindowTitle('Segmentation Support Tool (GUI Mode)')
                    self.show()

            self.waiting_to_start = True
            self.global_timer = Timer().start()
            self.algo_timer = Timer()
            self.user_timer = Timer()
            self.console_push_text('Initialized.')

            if mode != 2:
                self.run_instance()
    # pass

    def resizeEvent(self, event):
        self.show_current_frame()

    # TODO : SAVE OPTION ADD -> (ALL, RGB, MASK, OVERLAY, RGBA, ROTO)
    def save(self, size):
        folder_path = str(QFileDialog.getExistingDirectory(self, "Select Save Directory"))

        self.console_push_text('Saving masks and overlays...')
        mask_dir = path.join(folder_path, 'mask')
        overlay_dir = path.join(folder_path, 'overlay')
        roto_dir = path.join(folder_path, 'roto')
        rgba_dir = path.join(folder_path, 'rgba')

        os.makedirs(mask_dir, exist_ok=True)
        os.makedirs(overlay_dir, exist_ok=True)
        os.makedirs(roto_dir, exist_ok=True)
        os.makedirs(rgba_dir, exist_ok=True)

        for i in range(self.num_frames):
            # Save RGB image
            # image = self.images[i]
            # image = Image.fromarray(image)
            # image.save(os.path.join(rgba_dir, '{:05d}.png'.format(i)))

            # Save maskpalette = pal_color_map()
            mask = Image.fromarray(self.current_mask[i]).convert('P')
            mask.putpalette(palette)
            mask.save(os.path.join(mask_dir, '{:05d}.png'.format(i)))

            # Save overlay
            overlay = overlay_davis(self.images[i], self.current_mask[i])
            overlay = Image.fromarray(overlay)
            overlay.save(os.path.join(overlay_dir, '{:05d}.png'.format(i)))

            # Save RGBA
            mask = Image.fromarray(self.current_mask[i])
            mask = get_binary_map(np.array(mask))
            rgba_data = convertRGBtoRGBA(self.images[i], mask)
            plt.imsave(os.path.join(rgba_dir, '{:05d}.png'.format(i)), rgba_data)

            # Save Rotoscope
            mask = Image.fromarray(self.current_mask[i])
            roto_arr = overlay_image(self.images[i], mask)
            roto = Image.fromarray(roto_arr)
            roto.save(os.path.join(roto_dir, '{:05d}.png'.format(i)))

        self.console_push_text('Done.')

    def binary_mask(self):
        folder_path = str(QFileDialog.getExistingDirectory(self, "Select Saved Directory"))
        self.console_push_text('Saving Binary Masks...')
        b_mask_dir = path.join(folder_path, 'binary')
        os.makedirs(b_mask_dir, exist_ok=True)

        for i in range(self.num_frames):
            # Save Binary mask
            mask_gray = Image.fromarray(self.current_mask[i])
            gray = np.array(mask_gray)
            b_mask_arr = get_binary_map(gray)
            b_mask = Image.fromarray(b_mask_arr)
            b_mask.save(os.path.join(b_mask_dir, '{:05d}.png'.format(i)))
            # cv2.imwrite(b_mask_dir + '/' + str(i).zfill(5) + '.png', b_mask)
        self.console_push_text('Done')

    def console_push_text(self, text):
        text = '[A: %s, U: %s]: %s' % (self.algo_timer.format(), self.user_timer.format(), text)
        self.console.appendPlainText(text)
        self.console.moveCursor(QTextCursor.End)
        print(text)

    def interaction_radio_clicked(self, event):
        self.last_interaction = self.curr_interaction
        if self.radio_s2m.isChecked():
            self.curr_interaction = 'Scribble'
            self.brush_size = 3
            self.brush_slider.setDisabled(True)
        elif self.radio_fbrs.isChecked():
            self.curr_interaction = 'Click'
            self.brush_size = 3
            self.brush_slider.setDisabled(True)
        elif self.radio_free.isChecked():
            self.brush_slider.setDisabled(False)
            self.brush_slide()
            self.curr_interaction = 'Free'
        if self.curr_interaction == 'Scribble':
            self.commit_button.setEnabled(True)
        else:
            self.commit_button.setEnabled(False)

        # if self.last_interaction != self.curr_interaction:
            # self.console_push_text('Interaction changed to ' + self.curr_interaction + '.')

    def compose_current_im(self):
        if self.in_local_mode:
            if self.viz_mode == 'fade':
                self.viz = overlay_davis_fade(self.local_np_im, self.local_np_mask) 
            elif self.viz_mode == 'davis':
                self.viz = overlay_davis(self.local_np_im, self.local_np_mask) 
            elif self.viz_mode == 'light':
                self.viz = overlay_davis(self.local_np_im, self.local_np_mask, 0.9) 
            else:
                raise NotImplementedError
        else:
            if self.viz_mode == 'fade':
                self.viz = overlay_davis_fade(self.images[self.cursur], self.current_mask[self.cursur]) 
            elif self.viz_mode == 'davis':
                self.viz = overlay_davis(self.images[self.cursur], self.current_mask[self.cursur]) 
            elif self.viz_mode == 'light':
                self.viz = overlay_davis(self.images[self.cursur], self.current_mask[self.cursur], 0.9)
            else:
                raise NotImplementedError

    def update_interact_vis(self):
        # Update the interactions without re-computing the overlay
        height, width, channel = self.viz.shape
        bytesPerLine = 3 * width

        if self.in_local_mode:
            vis_map = self.local_vis_map
            vis_alpha = self.local_vis_alpha
            brush_vis_map = self.local_brush_vis_map
            brush_vis_alpha = self.local_brush_vis_alpha
        else:
            vis_map = self.vis_map
            vis_alpha = self.vis_alpha
            brush_vis_map = self.brush_vis_map
            brush_vis_alpha = self.brush_vis_alpha

        self.viz_with_stroke = self.viz*(1-vis_alpha) + vis_map*vis_alpha
        self.viz_with_stroke = self.viz_with_stroke*(1-brush_vis_alpha) + brush_vis_map*brush_vis_alpha
        self.viz_with_stroke = self.viz_with_stroke.astype(np.uint8)

        qImg = QImage(self.viz_with_stroke.data, width, height, bytesPerLine, QImage.Format_RGB888)
        self.main_canvas.setPixmap(QPixmap(qImg.scaled(self.main_canvas.size(),
                Qt.KeepAspectRatio, Qt.FastTransformation)))

        self.main_canvas_size = self.main_canvas.size()
        self.image_size = qImg.size()

    def update_minimap(self):
        # Limit it within the valid range
        if self.in_local_mode:
            if self.minimap_in_local_drawn:
                # Do not redraw
                return
            self.minimap_in_local_drawn = True
            patch = self.minimap_in_local.astype(np.uint8)
        else:
            ex, ey = self.last_ex, self.last_ey
            r = self.zoom_pixels//2
            ex = int(round(max(r, min(self.width-r, ex))))
            ey = int(round(max(r, min(self.height-r, ey))))

            patch = self.viz_with_stroke[ey-r:ey+r, ex-r:ex+r, :].astype(np.uint8)

        height, width, channel = patch.shape
        bytesPerLine = 3 * width
        qImg = QImage(patch.data, width, height, bytesPerLine, QImage.Format_RGB888)
        self.minimap.setPixmap(QPixmap(qImg.scaled(self.minimap.size(),
                Qt.KeepAspectRatio, Qt.FastTransformation)))

    def show_current_frame(self):
        # Re-compute overlay and show the image
        self.compose_current_im()
        self.update_interact_vis()
        self.update_minimap()
        self.lcd.setText('{: 3d} / {: 3d}'.format(self.cursur, self.num_frames-1))
        self.tl_slider.setValue(self.cursur)

    def get_scaled_pos(self, x, y):
        # Un-scale and un-pad the label coordinates into image coordinates
        oh, ow = self.image_size.height(), self.image_size.width()
        nh, nw = self.main_canvas_size.height(), self.main_canvas_size.width()

        h_ratio = nh/oh
        w_ratio = nw/ow
        dominate_ratio = min(h_ratio, w_ratio)

        # Solve scale
        x /= dominate_ratio
        y /= dominate_ratio

        # Solve padding
        fh, fw = nh/dominate_ratio, nw/dominate_ratio
        x -= (fw-ow)/2
        y -= (fh-oh)/2

        if self.in_local_mode:
            x = max(0, min(self.local_width-1, x))
            y = max(0, min(self.local_height-1, y))
        else:
            x = max(0, min(self.width-1, x))
            y = max(0, min(self.height-1, y))

        # return int(round(x)), int(round(y))
        return x, y

    def clear_visualization(self):
        if self.in_local_mode:
            self.local_vis_map.fill(0)
            self.local_vis_alpha.fill(0)
            self.local_vis_hist.clear()
            self.local_vis_hist.append((self.local_vis_map.copy(), self.local_vis_alpha.copy()))
        else:
            self.vis_map.fill(0)
            self.vis_alpha.fill(0)
            self.vis_hist.clear()
            self.vis_hist.append((self.vis_map.copy(), self.vis_alpha.copy()))

    def reset_this_interaction(self):
        self.complete_interaction()
        self.clear_visualization()
        if self.in_local_mode:
            self.local_interaction = None
            self.local_interactions['interact'] = self.local_interactions['interact'][:1]
        else:
            self.interaction = None
            self.this_frame_interactions = []
        self.undo_button.setDisabled(True)
        if self.fbrs_controller is not None:
            self.fbrs_controller.unanchor()

    def set_viz_mode(self):
        self.viz_mode = self.combo.currentText()
        self.show_current_frame()

    def tl_slide(self):
        if self.waiting_to_start:
            self.waiting_to_start = False
            self.algo_timer.start()
            self.user_timer.start()
            self.console_push_text('Timers started.')

        self.reset_this_interaction()
        self.cursur = self.tl_slider.value()
        self.show_current_frame()

    def brush_slide(self):
        self.brush_size = self.brush_slider.value()
        self.brush_label.setText('Brush size: %d' % self.brush_size)
        try:
            if type(self.interaction) == FreeInteraction:
                self.interaction.set_size(self.brush_size)
        except AttributeError:
            # Initialization, forget about it
            pass

    def progress_step_cb(self):
        self.progress_num += 1
        ratio = self.progress_num/self.progress_max
        self.progress.setValue(int(ratio*100))
        self.progress.setFormat('%2.1f%%' % (ratio*100))
        QApplication.processEvents()

    def progress_total_cb(self, total):
        self.progress_max = total
        self.progress_num = -1
        self.progress_step_cb()

    def run_instanceSeg(self):
        masks_frames, frame_masks, class_frames, class_labels, class_boxes, classes = run_video_instance(
            self.images, seleteced_classes=self.selected_class)

        instanced_classes = []
        for labels in class_labels:
            for label in labels:
                if label not in instanced_classes:
                    instanced_classes.append(label)
        classes_info = {}
        true_class = []
        false_class = []
        for cls in classes:
            if cls not in instanced_classes:
                false_class.append(cls)
            else:
                true_class.append(cls)
        classes_info["True"] = true_class
        classes_info["False"] = false_class

        with open(self.outputPath + '/classes.json', 'w') as f:
            json.dump(classes_info, f)

        self.class_frames = class_frames
        self.current_mask = frame_masks
        self.current_labels = class_labels
        self.current_boxes = class_boxes
        torch.cuda.empty_cache()

    def run_trackingObj(self):
        # label_infos, bbox_infos = connected_component(self.current_mask)
        # shot_info, seed_info, tracked_box_info = run_tracking(self.images, label_infos, bbox_infos, self.debug_mode)

        shot_info, seed_info, tracked_box_info = run_tracking(self.images, self.class_frames, self.current_labels, self.current_boxes, self.debug_mode)
        self.trackingInfo = [shot_info, seed_info, tracked_box_info]

        # self.current_mask 초기화
        init_mask = np.zeros_like(self.current_mask)
        self.current_mask = init_mask

        labels = list(shot_info.keys())

        labels_frame = []
        labels_instance = []
        for label in labels:
            instance_frame = seed_info[label]
            frame_masks, instance_masks = run_image_instance(self.images, self.num_objects, instance_frame,
                                                             self.selected_class, self.debug_mode)
            labels_frame.append(frame_masks)
            labels_instance.append(instance_masks)

        self.labelInfo = [labels, labels_frame, labels_instance]
        torch.cuda.empty_cache()

    def save_tracking_info(self):
        labels, labels_frame, labels_instance = self.labelInfo
        shot_info, seed_info, tracked_box_info = self.trackingInfo

        class_labels = self.current_labels
        class_boxes = self.current_boxes

        print("Labels :", labels)
        print("seed infoes :", seed_info)
        print("shot cut list : ", shot_info)

        tracking_info = {}
        box_info = {}
        obj_names = []
        for num, label in enumerate(labels):
            obj_frame = shot_info[label]
            seed_frame = seed_info[label]

            tracked_box = tracked_box_info[seed_frame][label]
            class_box = class_boxes[seed_frame]
            class_idx = class_box.index(tracked_box)
            class_name = class_labels[seed_frame][class_idx]
            obj_name = str(class_name) + "_" + str(num)
            tracking_info[obj_name] = obj_frame

            obj_boxInfo = self.get_boxSize(label, tracked_box_info)
            box_info[obj_name] = obj_boxInfo
            obj_names.append(obj_name)

        self.obj_names = obj_names
        print("obj names : ", obj_names)
        with open(self.outputPath + '/object_frame.json', 'w') as f:
            json.dump(tracking_info, f)
        with open(self.outputPath + '/object_box.json', 'w') as f:
            json.dump(box_info, f)
        with open(self.outputPath + '/object_classes.json', 'w') as f:
            json.dump(obj_names, f)

    def get_boxSize(self, label, tracked_box_info):
        label_boxes = []
        for tracked_box in tracked_box_info:
            keys = tracked_box.keys()
            if label in keys:
                value = tracked_box[label]
                label_boxes.append(value)

        min_size = 0
        max_size = 0
        min_box = None
        max_box = None
        for cnt, label_box in enumerate(label_boxes):
            x0, y0, x1, y1, _ = label_box
            size = (y1 - y0) * (x1 - x0)
            if cnt == 0:
                min_size = size
                max_size = size
            else:
                if size > max_size:
                    max_size = size
                    max_box = [x0, y0, x1, y1, max_size]
                if size < min_size:
                    min_size = size
                    min_box = [x0, y0, x1, y1, min_size]

        obj_boxInfo = [min_size, max_size]
        return obj_boxInfo


    def run_propagate(self):
        # Propagation 파트
        prop_net, fuse_net, s2m, fbrs = self.modelInfo

        propa_start = time.time()
        with torch.cuda.amp.autocast(enabled=True):
            # Load our checkpoint
            prop_saved = torch.load(prop_net)
            prop_model = PropagationNetwork().cuda().eval()
            prop_model.load_state_dict(prop_saved)

            fusion_saved = torch.load(fuse_net)
            fusion_model = FusionNet().cuda().eval()
            fusion_model.load_state_dict(fusion_saved)

            # Loads the S2M model
            if s2m is not None:
                s2m_saved = torch.load(s2m)
                s2m_model = S2M().cuda().eval()
                s2m_model.load_state_dict(s2m_saved)
            else:
                s2m_model = None

            s2m_controller = S2MController(s2m_model, self.num_objects, ignore_class=255)
            if fbrs is not None:
                fbrs_controller = FBRSController(fbrs)
            else:
                fbrs_controller = None

            self.s2m_controller = s2m_controller
            self.fbrs_controller = fbrs_controller
            self.processor = InferenceCore(prop_model, fusion_model, self.torch_images,
                                           self.num_objects, mem_freq=self.mem_freq, mem_profile=0)
            # self.processor = InferenceCore(prop_model, fusion_model, None,
            #                                self.num_objects, mem_freq=self.mem_freq, mem_profile=0)

            shot_info, seed_info, tracked_box_info = self.trackingInfo
            labels, labels_frame, labels_instance = self.labelInfo
            label_names = self.obj_names

            for n, label in enumerate(labels):
                label_name = label_names[n]
                shot_cut_list = shot_info[label]
                length = len(shot_cut_list)
                start = shot_cut_list[0]
                end = shot_cut_list[length - 1] + 1
                propagate_range = shot_cut_list
                instance_frame = seed_info[label]

                if len(propagate_range) <= 1:
                    pass
                else:
                    print("")
                    print("---------------------------------------")
                    print("Label number :", label)
                    print("Label name :", label_name)
                    print("Instance Seed Frame :", instance_frame)
                    print("Propagate_range : ({} ~ {})".format(start, end - 1))
                    print("---------------------------------------")

                    self.cursur = instance_frame
                    # frame_masks, instance_masks = run_image_instance(self.images, self.num_objects, instance_frame,
                    #                                                  debug_mode)
                    frame_masks = labels_frame[n]
                    instance_masks = labels_instance[n]

                    self.current_mask = frame_masks
                    interected, padd = pad_divide_by(instance_masks, 16, instance_masks.shape[-2:])
                    self.interacted_mask = interected
                    torch.cuda.empty_cache()

                    print("Propagation is started (Label : {})".format(label))

                    oom = False
                    try:
                        self.current_mask = self.processor.interact_frame(self.interacted_mask, self.cursur,
                                                                    None, None, propagate_range)
                        self.processor.interact_clear(self.torch_images, self.num_objects)
                    except RuntimeError:
                        oom = True
                        print('\033[31m' + 'Warning : Out of Memory !!' + '\033[0m')
                        self.processor.interact_clear(self.torch_images, self.num_objects)

                    if oom:
                        torch.cuda.empty_cache()
                        # interact 내부 tensor 관련 변수 초기화 함수
                        self.processor.interact_clear(self.torch_images, self.num_objects)
                        print(torch.cuda.memory_reserved())
                        pass

                        # try:
                        #     # interact 함수 재실행
                        #     self.current_mask = self.processor.interact(self.interacted_mask, self.cursur,
                        #                                                       None, None, propagate_range)
                        #     self.processor.interact_clear(self.torch_images, self.num_objects)
                        #     oom = False
                        # except RuntimeError:
                        #     oom = True
                        #     print('\033[31m' + 'Warning : Out of Memory !!' + '\033[0m')
                        #     self.processor.interact_clear(self.torch_images, self.num_objects)
                        #     pass

                    if not oom:
                        labeling_mask(self.current_mask, tracked_box_info, label, self.images,
                                      propagate_range, label_name, self.outputPath, self.save_mode)

                    self.interacted_mask = None

                    # labeling_mask(self.current_mask, tracked_box_info, label, self.images,
                    #               propagate_range, self.outputPath, self.save_mode)

                    # self.current_mask 초기화
                    init_mask = np.zeros_like(self.current_mask)
                    del self.current_mask
                    self.current_mask = init_mask
                    torch.cuda.empty_cache()
        torch.cuda.empty_cache()

    def run_instance(self):
        debug_mode = None
        if self.debug_mode:
            debug_mode = True

        print("[Execute Run Instance]")
        if self.user_timer is not None:
            self.user_timer.pause()
        outputPath = self.outputPath
        save_mode = self.save_mode
        frames = self.images  # Images from Video
        frames_length = len(frames)

        # == interected in not support, cause tensor array error == #
        print("=======================================")
        print("Run Instance Segmentation Start")
        print("GPU Allocate : ", torch.cuda.memory_allocated())
        print("GPU reserved : ", torch.cuda.memory_reserved())

        instance_start = time.time()
        predicted, frame_masks = run_video_instance(frames, self.num_objects) ##
        torch.cuda.empty_cache()

        self.current_mask = frame_masks
        init_mask = np.zeros_like(self.current_mask)
        # interected, padd = pad_divide_by(interected_masks, 16, interected_masks.shape[-2:])
        # self.interacted_mask = interected
        # print("interacted_mask : ", self.interacted_mask.shape)
        instance_end = time.time()
        instance_time = instance_end - instance_start

        print("=======================================")
        print("Run Tracking Start")
        print("GPU Allocate : ", torch.cuda.memory_allocated())
        print("GPU reserved : ", torch.cuda.memory_reserved())
        tracking_start = time.time()

        # Connected Component 파트
        label_infos, bbox_infos = connected_component(frame_masks, frames)
        # Tracking 파트
        shot_info, seed_info, tracked_box_info = run_tracking(frames, label_infos, bbox_infos, debug_mode)
        self.current_mask = init_mask

        tracking_end = time.time()
        tracking_time = tracking_end - tracking_start

        labels = list(shot_info.keys())
        print("=======================================")
        print("Run Propagate Start")
        print("GPU Allocate : ", torch.cuda.memory_allocated())
        print("GPU reserved : ", torch.cuda.memory_reserved())
        if self.console_push_text is not None:
            self.console_push_text('Propagation started.')
        propagate_times = 0
        save_times = 0
        for n, label in enumerate(labels):
            propagate_start = time.time()

            shot_cut_list = shot_info[label]
            length = len(shot_cut_list)
            start = shot_cut_list[0]
            end = shot_cut_list[length - 1] + 1
            propagate_range = shot_cut_list
            instance_frame = seed_info[label]

            print("")
            print("---------------------------------------")
            print("Label :", label)
            print("Instance Seed Frame :", instance_frame)
            print("Propagate_range : ({} ~ {})".format(start, end - 1))
            print("---------------------------------------")

            self.cursur = instance_frame
            frame_masks, instance_masks = run_image_instance(frames, self.num_objects, instance_frame, debug_mode)
            self.current_mask = frame_masks
            interected, padd = pad_divide_by(instance_masks, 16, instance_masks.shape[-2:])
            self.interacted_mask = interected

            print("GPU Allocate : ", torch.cuda.memory_allocated())
            print("GPU reserved : ", torch.cuda.memory_reserved())
            torch.cuda.empty_cache()

            if self.interacted_mask is None:
                self.console_push_text('Cannot propagate! No interacted mask!')

            print("Propagation is started (Label : {})".format(label))

            if self.mode == 0:
                self.current_mask = self.processor.interact(self.interacted_mask, self.cursur,
                                                            None, None, propagate_range)
            else:
                self.current_mask = self.processor.interact(self.interacted_mask, self.cursur,
                                                        self.progress_total_cb, self.progress_step_cb, propagate_range)
            self.interacted_mask = None

            propagate_end = time.time()
            propagate_time = propagate_end - propagate_start
            print("Propagate time : ", propagate_time)
            propagate_times += propagate_time

            if self.progress is not None:
                self.progress.setFormat('Idle')
                self.progress.setValue(0)

            print("Propagation is finished (Label : {})".format(label))
            print("GPU Allocate : ", torch.cuda.memory_allocated())
            print("GPU reserved : ", torch.cuda.memory_reserved())
            # mask_result = labeling_mask(self.current_mask, tracked_box_info, frames, save=True)

            save_start = time.time()
            labeling_mask(self.current_mask, tracked_box_info, label, frames, propagate_range, outputPath, save_mode)
            save_end = time.time()
            save_time = save_end - save_start
            save_times += save_time

            # TODO : 임시적으로 마스크 초기화 진행, 각 레이블 별 결과물에 대한 통합 결과물 구현 필요 (겹치는 프레임 존재 및 해당 부분 처리 방안 필요)
            self.current_mask = init_mask
            torch.cuda.empty_cache()

        print("============================================")
        print("Instance Segmentation Time : ", instance_time)
        print("Tracking Time : ", tracking_time)
        print("Propagate Time : ", propagate_times)
        print("Image Save Time : ", save_times)
        print("============================================")

        time_result = [["instance_time : ", str(instance_time)],
                       ["tracking_time : ", str(tracking_time)],
                       ["propagate_time : ", str(propagate_times)],
                       ["image_save_time : ", str(save_times)]
                       ]
        np.savetxt(outputPath + "/time_record.txt", np.array(time_result), fmt="%s", delimiter="")

        print("All Finished!!")

        torch.cuda.empty_cache()
        if self.user_timer is not None:
            self.user_timer.start()

    def run_instance_need_to_add_shot_cut(self):
        self.user_timer.pause()
        frames = self.images  # Images from Video
        frames_length = len(frames)

        # Run Instance Part, after Run Propagate Part
        propagate_range = self.propagate_range  # ex) [0, 8, 75]
        instance_frames = self.instance_frames  # ex) [2, 24, 90]
        i = 0
        for instance_frame in instance_frames:  # ex) 2, 24, 90
            self.cursur = instance_frame
            frame_masks, instance_masks = run_image_instance(frames, self.num_objects, instance_frame)
            self.current_mask = frame_masks
            interected, padd = pad_divide_by(instance_masks, 16, instance_masks.shape[-2:])
            self.interacted_mask = interected
            print("interacted_mask : ", self.interacted_mask.shape)

            if self.interacted_mask is None:
                self.console_push_text('Cannot propagate! No interacted mask!')
                return

            self.console_push_text('Propagation started.')
            if i != (len(propagate_range) - 1):
                self.propagate_range = [propagate_range[i], propagate_range[i + 1]]
            elif i == (len(propagate_range) - 1):
                self.propagate_range = [propagate_range[i], frames_length]

            self.current_mask = self.processor.interact(self.interacted_mask, self.cursur,
                                                        self.progress_total_cb, self.progress_step_cb, self.propagate_range)
            i += 1
            self.interacted_mask = None
            # clear scribble and reset
            self.show_current_frame()
            self.reset_this_interaction()
            self.progress.setFormat('Idle')
            self.progress.setValue(0)

        # connected_component(self.current_mask)
        label_infos, bbox_infos = connected_component(self.current_mask, frames)  # TEST for USE frames
        # box_info, iou_info, id_info, tracked_info = run_tracking(frames, label_infos, bbox_infos)
        box_info, iou_info, tracked_info = run_tracking(frames, label_infos, bbox_infos)
        mask_result = labeling_mask(self.current_mask, tracked_info, frames)
        self.current_mask = mask_result
        self.console_push_text('Propagation All finished!')
        self.show_current_frame()
        self.user_timer.start()

    def run_instance_ORIGINAL(self):
        self.user_timer.pause()
        frames = self.images  # Images from Video

        from interact.instance import run_image_instance, run_video_instance

        # frame_masks, instance_masks, load_cursur = run_video_instance(frames, self.num_objects)
        frame_masks, instance_masks = run_video_instance(frames, self.num_objects)
        # print("BOX Info : ", shot_list)

        # Single Image -> Segmentation Mask
        # instance_mask = run_image_instance(frames, cursur, self.num_objects)
        # interected, padd = pad_divide_by(instance_mask, 16, instance_mask.shape[-2:])
        # self.interacted_mask = interected

        # for i in range(len(instance_masks)):
        #     instance_mask = instance_masks[i]
        #     frame_mask = frame_masks[i]
        #
        #     self.current_mask = frame_mask
        #     self.cursur = load_cursur[i]
        #     print("CURSUR NUM", self.cursur)
        #     interected, padd = pad_divide_by(instance_mask, 16, instance_mask.shape[-2:])
        #     self.interacted_mask = interected

        self.current_mask = frame_masks
        self.cursur = 2
        interected, padd = pad_divide_by(instance_masks, 16, instance_masks.shape[-2:])
        self.interacted_mask = interected
        self.show_current_frame()
        # self.update_interacted_mask()
        self.user_timer.start()

    def on_run_with_mask(self):  # Currently, Only Activate Single Mask Load...
        # TODO : ERROR FIX -> (The expanded size of the tensor (1) must match the existing size (126) at non-singleton dimension 1.  Target sizes: [11, 1, 432, 1024].  Tensor sizes: [3, 126, 432, 1024])
        self.user_timer.pause()

        mask_path = QFileDialog.getExistingDirectory(self, "Select mask folder", './')
        # mask_path = QFileDialog.getOpenFileNames(self, "Select mask images", './')

        # mask_path = 'C:/Users/VIBE/Desktop/MGD-TEST/Interactive/47/test_mask/'
        mask, interected, cursur = load_masks(mask_path, self.num_frames, self.num_objects)
        print(interected.shape)
        self.current_mask = mask
        self.cursur = cursur
        interected, padd = pad_divide_by(interected, 16, interected.shape[-2:])
        print("interected :", interected)
        self.interacted_mask = interected
        print(interected.shape)

        # interacted_mask = convert_masks(self.current_mask)
        # self.interacted_mask = None
        # clear scribble and reset
        # self.show_current_frame()
        # print("Before update interected mask")

        # self.update_interacted_mask()
        self.reset_this_interaction()
        # self.progress.setFormat('Idle')
        # self.progress.setValue(0)
        # self.console_push_text('Mask Load finished!')


        # interected_masks = interected
        # for inter_mask in interected_masks:
        #     self.interacted_mask = inter_mask
        #     self.update_interacted_mask()

        self.user_timer.start()

    def on_run(self):
        prop_net, fuse_net, s2m, fbrs = self.modelInfo
        self.user_timer.pause()
        print("@@ On Run @@")
        print("interacted_mask : ", self.interacted_mask.shape)

        if self.interacted_mask is None:
            self.console_push_text('Cannot propagate! No interacted mask!')
            return

        self.console_push_text('Propagation started.')
        # self.interacted_mask = torch.softmax(self.interacted_mask*1000, dim=0)

        ## interact(rgb frame list -> [0, 7] / ->  for ti in range(l0, l1) : Propagate apply range
        ## interacted_mask = single instance mask
        self.current_mask = self.processor.interact(self.interacted_mask, self.cursur,
                            self.progress_total_cb, self.progress_step_cb)

        self.interacted_mask = None
        # clear scribble and reset
        self.show_current_frame()
        self.reset_this_interaction()
        self.progress.setFormat('Idle')
        self.progress.setValue(0)
        self.console_push_text('Propagation finished!')
        self.user_timer.start()

    def on_commit(self):
        self.complete_interaction()
        self.update_interacted_mask()

    def on_prev(self):
        # self.tl_slide will trigger on setValue
        self.cursur = max(0, self.cursur-1)
        self.tl_slider.setValue(self.cursur)

    def on_next(self):
        # self.tl_slide will trigger on setValue
        self.cursur = min(self.cursur+1, self.num_frames-1)
        self.tl_slider.setValue(self.cursur)

    def on_time(self):
        self.cursur += 1
        if self.cursur > self.num_frames-1:
            self.cursur = 0
        self.tl_slider.setValue(self.cursur)

    def on_play(self):
        if self.timer.isActive():
            self.timer.stop()
        else:
            self.timer.start(1000 / 25)

    def on_undo(self):
        if self.in_local_mode:
            if self.local_interaction is None:
                if len(self.local_interactions['interact']) > 1:
                    self.local_interactions['interact'] = self.local_interactions['interact'][:-1]
                else:
                    self.reset_this_interaction()
                self.local_interacted_mask = self.local_interactions['interact'][-1].predict()
            else:
                if self.local_interaction.can_undo():
                    self.local_interacted_mask = self.local_interaction.undo()
                else:
                    if len(self.local_interactions['interact']) > 1:
                        self.local_interaction = None
                    else:
                        self.reset_this_interaction()
                    self.local_interacted_mask = self.local_interactions['interact'][-1].predict()

            # Update visualization
            if len(self.local_vis_hist) > 0:
                # Might be empty if we are undoing the entire interaction
                self.local_vis_map, self.local_vis_alpha = self.local_vis_hist.pop()
        else:
            if self.interaction is None:
                if len(self.this_frame_interactions) > 1:
                    self.this_frame_interactions = self.this_frame_interactions[:-1]
                    self.interacted_mask = self.this_frame_interactions[-1].predict()
                else:
                    self.reset_this_interaction()
                    self.interacted_mask = self.processor.prob[:, self.cursur].clone()
            else:
                if self.interaction.can_undo():
                    self.interacted_mask = self.interaction.undo()
                else:
                    if len(self.this_frame_interactions) > 0:
                        self.interaction = None
                        self.interacted_mask = self.this_frame_interactions[-1].predict()
                    else:
                        self.reset_this_interaction()
                        self.interacted_mask = self.processor.prob[:, self.cursur].clone()

            # Update visualization
            if len(self.vis_hist) > 0:
                # Might be empty if we are undoing the entire interaction
                self.vis_map, self.vis_alpha = self.vis_hist.pop()

        # Commit changes
        self.update_interacted_mask()

    def on_reset(self):
        # Edit prod2 but not prod1 -- we still need the mask diff
        # self.processor.prob2[:, self.cursur].zero_()
        # self.processor.prob2[0, self.cursur] = 1e-7
        # DO not edit prob -- we still need the mask diff
        self.processor.masks[self.cursur].zero_()
        self.processor.np_masks[self.cursur].fill(0)
        self.current_mask[self.cursur].fill(0)
        self.reset_this_interaction()
        self.show_current_frame()

    def on_zoom_plus(self):
        self.zoom_pixels -= 25
        self.zoom_pixels = max(50, self.zoom_pixels)
        self.update_minimap()

    def on_zoom_minus(self):
        self.zoom_pixels += 25
        self.zoom_pixels = min(self.zoom_pixels, 300)
        self.update_minimap()

    def set_navi_enable(self, boolean):
        self.zoom_p_button.setEnabled(boolean)
        self.zoom_m_button.setEnabled(boolean)
        self.run_button.setEnabled(boolean)

        self.run_with_mask_button.setEnabled(boolean)

        self.tl_slider.setEnabled(boolean)
        self.play_button.setEnabled(boolean)
        self.lcd.setEnabled(boolean)

    def on_finish_local(self):
        self.complete_interaction()
        self.finish_local_button.setDisabled(True)
        self.in_local_mode = False
        self.set_navi_enable(True)

        # Push the combined local interactions as a global interaction
        if len(self.this_frame_interactions) > 0:
            prev_soft_mask = self.this_frame_interactions[-1].out_prob
        else:
            # prev_soft_mask = self.processor.prob2[1:, self.cursur]
            prev_soft_mask = self.processor.prob[1:, self.cursur]
        image = self.processor.images[:,self.cursur]

        self.interaction = LocalInteraction(
            image, prev_soft_mask, (self.height, self.width), self.local_bb, 
            self.local_interactions['interact'][-1].out_prob, 
            self.processor.pad, self.local_pad
        )
        self.interaction.storage = self.local_interactions
        self.interacted_mask = self.interaction.predict()
        self.complete_interaction()
        self.update_interacted_mask()
        self.show_current_frame()

        self.console_push_text('Finished local control.')

    def hit_number_key(self, number):
        if number == self.current_object:
            return
        self.current_object = number
        if self.fbrs_controller is not None:
            self.fbrs_controller.unanchor()
        self.console_push_text('Current object changed to %d!' % number)
        self.clear_brush()
        self.vis_brush(self.last_ex, self.last_ey)
        self.update_interact_vis()
        self.show_current_frame()

    def clear_brush(self):
        self.brush_vis_map.fill(0)
        self.brush_vis_alpha.fill(0)
        if self.local_brush_vis_map is not None:
            self.local_brush_vis_map.fill(0)
            self.local_brush_vis_alpha.fill(0)

    # GUI 모드 실행 후, 이미지 보드 선택 시 오류 발생 위치 #
    def vis_brush(self, ex, ey):
        if self.ctrl_key:
            # Visualize the control region
            lx = int(round(min(self.local_start[0], ex)))
            ux = int(round(max(self.local_start[0], ex)))
            ly = int(round(min(self.local_start[1], ey)))
            uy = int(round(max(self.local_start[1], ey)))
            self.brush_vis_map = cv2.rectangle(self.brush_vis_map, (lx, ly), (ux, uy), 
                        (128,255,128), thickness=-1)
            self.brush_vis_alpha = cv2.rectangle(self.brush_vis_alpha, (lx, ly), (ux, uy), 
                        0.5, thickness=-1)
        else:
            # Visualize the brush (yeah I know)
            # print("current_object", self.current_object)
            # print("COLOR : ", color_map[self.current_object])
            if self.in_local_mode:
                self.local_brush_vis_map = cv2.circle(self.local_brush_vis_map, 
                        (int(round(ex)), int(round(ey))), self.brush_size//2+1, color_map[self.current_object], thickness=-1)
                self.local_brush_vis_alpha = cv2.circle(self.local_brush_vis_alpha, 
                        (int(round(ex)), int(round(ey))), self.brush_size//2+1, 0.5, thickness=-1)
            else:
                self.brush_vis_map = cv2.circle(self.brush_vis_map, 
                        (int(round(ex)), int(round(ey))), self.brush_size//2+1, color_map[self.current_object], thickness=-1)
                self.brush_vis_alpha = cv2.circle(self.brush_vis_alpha, 
                        (int(round(ex)), int(round(ey))), self.brush_size//2+1, 0.5, thickness=-1)

    def enter_local_control(self):
        self.in_local_mode = True
        lx = int(round(min(self.local_start[0], self.local_end[0])))
        ux = int(round(max(self.local_start[0], self.local_end[0])))
        ly = int(round(min(self.local_start[1], self.local_end[1])))
        uy = int(round(max(self.local_start[1], self.local_end[1])))

        # Reset variables
        self.local_bb = (lx, ux, ly, uy)
        self.local_interactions = {}
        self.local_interactions['interact'] = []
        self.local_interaction = None

        # Initial info
        if len(self.this_local_interactions) == 0:
            # prev_soft_mask = self.processor.prob2[1:, self.cursur]
            prev_soft_mask = self.processor.prob[1:, self.cursur]
        else:
            prev_soft_mask = self.this_local_interactions[-1].out_prob
        self.local_interactions['bounding_box'] = self.local_bb
        self.local_interactions['cursur'] = self.cursur
        init_interaction = CropperInteraction(self.processor.images[:,self.cursur], 
                                    prev_soft_mask, self.processor.pad, self.local_bb)
        self.local_interactions['interact'].append(init_interaction)

        self.local_interacted_mask = init_interaction.out_mask
        self.local_torch_im = init_interaction.im_crop
        self.local_np_im = self.images[self.cursur][ly:uy+1, lx:ux+1, :]
        self.local_pad = init_interaction.pad

        # initialize the local visualization maps
        h, w = init_interaction.h, init_interaction.w
        self.local_vis_map = np.zeros((h, w, 3), dtype=np.uint8)
        self.local_vis_alpha = np.zeros((h, w, 1), dtype=np.float32)
        self.local_brush_vis_map = np.zeros((h, w, 3), dtype=np.uint8)
        self.local_brush_vis_alpha = np.zeros((h, w, 1), dtype=np.float32)
        self.local_vis_hist = deque(maxlen=100)
        self.local_height, self.local_width = h, w

        # Refresh self.viz
        self.minimap_in_local_drawn = False
        self.minimap_in_local = self.viz_with_stroke
        self.update_interacted_mask()
        self.finish_local_button.setEnabled(True)
        self.undo_button.setEnabled(False)
        self.set_navi_enable(False)

        self.console_push_text('Entered local control.')

    def on_press(self, event):
        if self.waiting_to_start:
            self.waiting_to_start = False
            self.algo_timer.start()
            self.user_timer.start()
            self.console_push_text('Timers started.')
        print("CLICK EVENT")
        self.user_timer.pause()
        ex, ey = self.get_scaled_pos(event.x(), event.y())
        # Check for ctrl key
        modifiers = QApplication.keyboardModifiers()
        if not self.in_local_mode and modifiers == QtCore.Qt.ControlModifier:
            # Start specifying the local mode
            self.ctrl_key = True
        else:
            self.ctrl_key = False

        self.pressed = True
        self.right_click = (event.button() != 1)
        # Push last vis map into history
        if self.in_local_mode:
            self.local_vis_hist.append((self.local_vis_map.copy(), self.local_vis_alpha.copy()))
        else:
            self.vis_hist.append((self.vis_map.copy(), self.vis_alpha.copy()))
        if self.ctrl_key:
            # Wrap up the last interaction
            self.complete_interaction()
            # Labeling a local control field
            self.local_start = ex, ey
        else:
            # Ordinary interaction (might be in local mode)
            if self.in_local_mode:
                if self.local_interaction is None:
                    prev_soft_mask = self.local_interactions['interact'][-1].out_prob
                else:
                    prev_soft_mask = self.local_interaction.out_prob
                prev_hard_mask = self.local_max_mask
                image = self.local_torch_im
                h, w = self.local_height, self.local_width
            else:
                if self.interaction is None:
                    if len(self.this_frame_interactions) > 0:
                        prev_soft_mask = self.this_frame_interactions[-1].out_prob
                    else:
                        # prev_soft_mask = self.processor.prob2[1:, self.cursur]
                        prev_soft_mask = self.processor.prob[1:, self.cursur]
                        print("this_frame_interactions == 0")
                else:
                    # Not used if the previous interaction is still valid
                    # Don't worry about stacking effects here
                    prev_soft_mask = self.interaction.out_prob

                prev_hard_mask = self.processor.masks[self.cursur]
                image = self.processor.images[:,self.cursur]
                h, w = self.height, self.width

                print("prev_soft_mask")
                print(prev_soft_mask.shape)

            last_interaction = self.local_interaction if self.in_local_mode else self.interaction
            new_interaction = None
            if self.curr_interaction == 'Scribble':
                if last_interaction is None or type(last_interaction) != ScribbleInteraction:
                    self.complete_interaction()
                    new_interaction = ScribbleInteraction(image, prev_hard_mask, (h, w), 
                                self.s2m_controller, self.num_objects)
            elif self.curr_interaction == 'Free':
                if last_interaction is None or type(last_interaction) != FreeInteraction:
                    self.complete_interaction()
                    if self.in_local_mode:
                        new_interaction = FreeInteraction(image, prev_soft_mask, (h, w), 
                                self.num_objects, self.local_pad)
                    else:
                        new_interaction = FreeInteraction(image, prev_soft_mask, (h, w), 
                                self.num_objects, self.processor.pad)
                    new_interaction.set_size(self.brush_size)
            elif self.curr_interaction == 'Click':
                if (last_interaction is None or type(last_interaction) != ClickInteraction 
                        or last_interaction.tar_obj != self.current_object):
                    self.complete_interaction()
                    self.fbrs_controller.unanchor()
                    new_interaction = ClickInteraction(image, prev_soft_mask, (h, w), 
                                self.fbrs_controller, self.current_object, self.processor.pad)
            print("NEW INTERACTION : ", new_interaction)
            if new_interaction is not None:
                if self.in_local_mode:
                    self.local_interaction = new_interaction
                else:
                    self.interaction = new_interaction

        # Just motion it as the first step
        self.on_motion(event)
        self.user_timer.start()

    def on_motion(self, event):
        ex, ey = self.get_scaled_pos(event.x(), event.y())
        self.last_ex, self.last_ey = ex, ey
        self.clear_brush()
        # Visualize
        self.vis_brush(ex, ey)
        if self.pressed:
            if not self.ctrl_key:
                if self.curr_interaction == 'Scribble' or self.curr_interaction == 'Free':
                    obj = 0 if self.right_click else self.current_object
                    # Actually draw it if dragging
                    if self.in_local_mode:
                        self.local_vis_map, self.local_vis_alpha = self.local_interaction.push_point(
                            ex, ey, obj, (self.local_vis_map, self.local_vis_alpha)
                        )
                    else:
                        self.vis_map, self.vis_alpha = self.interaction.push_point(
                            ex, ey, obj, (self.vis_map, self.vis_alpha)
                        )
        self.update_interact_vis()
        self.update_minimap()

    def update_interacted_mask(self):
        if self.in_local_mode:
            self.local_max_mask = torch.argmax(self.local_interacted_mask, 0)
            max_mask = unpad_3dim(self.local_max_mask, self.local_pad)
            self.local_np_mask = (max_mask.detach().cpu().numpy()[0]).astype(np.uint8)
        else:
            self.processor.update_mask_only(self.interacted_mask, self.cursur)
            self.current_mask[self.cursur] = self.processor.np_masks[self.cursur]
        self.show_current_frame()

    def complete_interaction(self):
        if self.in_local_mode:
            if self.local_interaction is not None:
                self.clear_visualization()
                self.local_interactions['interact'].append(self.local_interaction)
                self.local_interaction = None
                self.undo_button.setDisabled(False)
        else:
            if self.interaction is not None:
                self.clear_visualization()
                self.interactions['annotated_frame'].append(self.cursur)
                self.interactions['interact'][self.cursur].append(self.interaction)
                self.this_frame_interactions.append(self.interaction)
                self.interaction = None
                self.undo_button.setDisabled(False)

    def on_release(self, event):
        self.user_timer.pause()
        ex, ey = self.get_scaled_pos(event.x(), event.y())
        if self.ctrl_key:
            # Enter local control mode
            self.clear_visualization()
            self.local_end = ex, ey
            self.enter_local_control()
        else:
            self.console_push_text('Interaction %s at frame %d.' % (self.curr_interaction, self.cursur))
            # Ordinary interaction (might be in local mode)
            if self.in_local_mode:
                interaction = self.local_interaction
            else:
                interaction = self.interaction

            if self.curr_interaction == 'Scribble' or self.curr_interaction == 'Free':
                self.on_motion(event)
                interaction.end_path()
                if self.curr_interaction == 'Free':
                    self.clear_visualization()
            elif self.curr_interaction == 'Click':
                ex, ey = self.get_scaled_pos(event.x(), event.y())
                if self.in_local_mode:
                    self.local_vis_map, self.local_vis_alpha = interaction.push_point(ex, ey,
                        self.right_click, (self.local_vis_map, self.local_vis_alpha))
                else:
                    self.vis_map, self.vis_alpha = interaction.push_point(ex, ey,
                        self.right_click, (self.vis_map, self.vis_alpha))

            if self.in_local_mode:
                self.local_interacted_mask = interaction.predict()
            else:
                self.interacted_mask = interaction.predict()
            self.update_interacted_mask()

        self.pressed = self.ctrl_key = self.right_click = False
        self.undo_button.setDisabled(False)
        self.user_timer.start()

    def debug_pressed(self):
        self.debug_mask, self.interacted_mask = self.interacted_mask, self.debug_mask

        self.processor.update_mask_only(self.interacted_mask, self.cursur)
        self.current_mask[self.cursur] = self.processor.np_masks[self.cursur]
        self.show_current_frame()

    def wheelEvent(self, event):
        ex, ey = self.get_scaled_pos(event.x(), event.y())
        if self.curr_interaction == 'Free':
            self.brush_slider.setValue(self.brush_slider.value() + event.angleDelta().y()//30)
        self.clear_brush()
        self.vis_brush(ex, ey)
        self.update_interact_vis()
        self.update_minimap()


def main(args):
    # Loads the masks
    if args.masks is not None:
        masks = load_masks(args.masks)
    else:
        masks = None

    # Determine the number of objects
    num_objects = args.num_objects
    if num_objects is None:
        if masks is not None:
            num_objects = masks.max()
        else:
            num_objects = 1

    app = QApplication(sys.argv)
    # ex = App(prop_model, fusion_model, s2m_controller, fbrs_controller, masks, num_objects, args.mem_freq,
    #                      args.images, args.output, args.mode, args.save_mode, args.debug_mode, args.exe_mode)
    ex = App(args.prop_model, args.fusion_model, args.s2m_model, args.fbrs_model, masks, num_objects, args.mem_freq,
             args.images, args.output, args.mode, args.save_mode, args.debug_mode, args.exe_mode)

    if args.mode != 0:
        sys.exit(app.exec_())

if __name__ == '__main__':
    # Arguments parsing
    parser = ArgumentParser()
    parser.add_argument('--prop_model', default='saves/propagation_model.pth')
    parser.add_argument('--fusion_model', default='saves/fusion.pth')
    parser.add_argument('--s2m_model', default='saves/s2m.pth')

    # parser.add_argument('--fbrs_model', default='saves/fbrs.pth')
    parser.add_argument('--fbrs_model', default='saves/resnet50_dh128_lvis.pth') # fbrs
    # parser.add_argument('--fbrs_model', default='saves/coco_lvis_h32_itermask.pth') # ritm

    parser.add_argument('--images', help='Folders containing input images. Either this or --video need to be specified.')
    parser.add_argument('--output', help='Folders containing output masks.')
    # parser.add_argument('--video', help='Video file readable by OpenCV. Either this or --images need to be specified.', default='example/p51_fly.mp4')
    parser.add_argument('--num_objects', help='Default: 1 if no masks provided, masks.max() otherwise', type=int, default=10)
    parser.add_argument('--mem_freq', default=5, type=int)
    # parser.add_argument('--masks', help='Optional, Ground truth masks', default='C:/Users/VIBE/Desktop/MGD-TEST/Interactive/47/mask/')
    parser.add_argument('--masks', help='Optional, Ground truth masks')
    args = parser.parse_args()

    # Debug Mode (0 = non debug print, 1 = debug print)
    Debug = 0

    # Load our checkpoint
    prop_saved = torch.load(args.prop_model)
    prop_model = PropagationNetwork().cuda().eval()
    prop_model.load_state_dict(prop_saved)

    fusion_saved = torch.load(args.fusion_model)
    fusion_model = FusionNet().cuda().eval()
    fusion_model.load_state_dict(fusion_saved)

    # Loads the S2M model
    if args.s2m_model is not None:
        s2m_saved = torch.load(args.s2m_model)
        s2m_model = S2M().cuda().eval()
        s2m_model.load_state_dict(s2m_saved)
    else:
        s2m_model = None

    # Loads the images/masks
    # if args.images is not None:
    #     images = load_images(args.images, 480)
    # elif args.video is not None:
    #     images = load_video(args.video, 480)
    #     size = input_status(args.video)
    # else:
    #     raise NotImplementedError('You must specify either --images or --video!')

    if args.masks is not None:
        masks = load_masks(args.masks)
    else:
        masks = None

    # Determine the number of objects
    num_objects = args.num_objects
    if num_objects is None:
        if masks is not None:
            num_objects = masks.max()
        else:
            num_objects = 1

    app = QApplication(sys.argv)
    # ex = App(prop_model, fusion_model, s2m_controller, fbrs_controller, masks, num_objects, args.mem_freq, args.images, args.output, args.save_mode, args.debug_mode)
    ex = App(prop_model, fusion_model, masks, num_objects, args.mem_freq, args.images,
             args.output, args.save_mode, args.debug_mode)
    # sys.exit(app.exec_())

