import argparse
import multiprocessing as mp
import cv2
from PIL import Image
import numpy as np
import os, glob
import time

from interact.interactive_utils import *
from util.palette import pal_color_map, get_binary_map
from detectron2.data.detection_utils import read_image, convert_PIL_to_numpy
from addon.AdelaiDet.demo.predictor import VisualizationDemo
from adet.config import get_cfg

confidence_threshold = 0.3
fusionSegmentationPath = './FusionSegmentation/MiVOS/'

def setup_cfg(args):
    # load config from file and command-line arguments
    cfg = get_cfg()
    cfg.merge_from_file(r"F:\VIBE\VIBE\FusionSegmentation\MiVOS\interact\config\R_101_dcni3_5x.yaml")
    cfg.merge_from_list(['MODEL.WEIGHTS', r"F:\VIBE\VIBE\FusionSegmentation\MiVOS\interact\model\R_101_dcni3_5x.pth"])
    # Set score_threshold for builtin models
    cfg.MODEL.RETINANET.SCORE_THRESH_TEST = confidence_threshold
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = confidence_threshold
    cfg.MODEL.FCOS.INFERENCE_TH_TEST = confidence_threshold
    cfg.MODEL.MEInst.INFERENCE_TH_TEST = confidence_threshold
    cfg.MODEL.PANOPTIC_FPN.COMBINE.INSTANCES_CONFIDENCE_THRESH = confidence_threshold
    cfg.freeze()

    return cfg

def get_parser():
    parser = argparse.ArgumentParser(description="Detectron2 Demo")
    parser.add_argument(
        "--config-file",
        default=r"F:\Project\MiVOS\interact\config\R_101_dcni3_5x.yaml",
        metavar="FILE",
        help="path to config file",
    )
    parser.add_argument(
        "--confidence-threshold",
        type=float,
        default=0.4,
        help="Minimum score for instance predictions to be shown",
    )
    parser.add_argument(
        "--opts",
        help="Modify config options using the command-line 'KEY VALUE' pairs",
        default=[],
        nargs=argparse.REMAINDER,
    )
    parser.add_argument(
        "--class_id",
        type=int,
        default=None,
        help="Class id for get unique object"
    )
    return parser

palette = pal_color_map()

def run_image_instance(input, save_path):
    """
    input: RGB image or images(video)

    Returns: mask(segmentation) result, base on class (temporary id : 0 -> person)
    """
    mp.set_start_method("spawn", force=True)
    args = get_parser().parse_args()

    cfg = setup_cfg(args)
    demo = VisualizationDemo(cfg)

    fn, h, w, ch = input.shape  # Frames / height / width / channel
    print("Frame cnt : {}".format(fn))

    origin_dir = save_path + '/origin'
    mask_dir = save_path + '/mask/'
    overlay_dir = save_path + '/overlay'
    rgba_dir = save_path + '/rgba'
    os.makedirs(origin_dir, exist_ok=True)
    os.makedirs(mask_dir, exist_ok=True)
    os.makedirs(overlay_dir, exist_ok=True)
    os.makedirs(rgba_dir, exist_ok=True)

    start = time.time()

    for num in range(fn):
        print("Processing Frame number : ", num)
        input_array = Image.fromarray(input[num])
        frame = convert_PIL_to_numpy(input_array, format="BGR")

        # # Original RGB Result
        # input_array.save(os.path.join(origin_dir, '{:05d}.png'.format(num)))

        voc_masks = demo.run_on_image(frame)
        # predictions, voc_masks, voc_array = demo.run_on_image(frame)
        mask = voc_masks

        # # Segmentation Mask Result
        mask.save(os.path.join(mask_dir, '{:05d}.png'.format(num)))

        mask_array = np.array(mask)
        overlay = overlay_davis(input[num], mask_array)
        overlay = Image.fromarray(overlay)
        overlay.save(os.path.join(overlay_dir, '{:05d}.png'.format(num)))

        # # # RGBA Result
        # mask = get_binary_map(np.array(mask))
        # rgba_data = convertRGBtoRGBA(input[num], mask)
        # plt.imsave(os.path.join(rgba_dir, '{:05d}.png'.format(num)), rgba_data)

    print("Total Frames : ", fn)
    print("Process Time : ", time.time() - start)

if __name__ == '__main__':
    video_dir = r'U:\MGD\Clip\segmentation\RND_TST_10.mov'
    save_path = r'F:\Project\UniTrack\results\test\10_TEST'
    images = select_files(video_dir, None)
    run_image_instance(images, save_path)