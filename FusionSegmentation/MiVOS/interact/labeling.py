from interact.interactive_utils import *
from util.palette import *

palette = pal_color_map()


def labeling_mask(current_masks, tracked_boxes, label, frames, label_range, label_name, outputPath, save_mode):
    # def labeling_mask(current_masks, tracked_boxes, frames, save):
    """
    Args:
        current_masks: VOS segmentation result masks(0 , 1)
        tracked_boxes : label coordinates ([{1: [x0, y0, x1, y1], 2: [x0, y0, x1, y1] ... }, {1: [x0, y0, x1, y1], 2: [x0, y0, x1, y1], ... } ... ])

    Returns: current_masks are colored by label id (0, 1, 2, ... n)
    """
    total_label_masks = []
    frames_num = len(current_masks)

    print("Output Path : ", outputPath)

    save = False
    if outputPath is not None:
        save = True

    if label is None and len(label_range) > 0:
        print("=== No Label Value ===")
        for num in range(frames_num):
            current_mask = current_masks[num]
            frame_height = current_masks.shape[1]
            tracked_id_box = tracked_boxes[num]
            tracked_id_key = list(tracked_id_box.keys())
            total_label_mask = np.copy(current_mask)

            print("Frame num : ", num)
            print("tracked_id_box : ", tracked_id_box)
            print("tracked_id_key : ", tracked_id_key)

            for id in tracked_id_key:

                x0 = tracked_id_box[id][0]
                y0 = tracked_id_box[id][1]
                x1 = tracked_id_box[id][2]
                y1 = tracked_id_box[id][3]

                w = x1 - x0
                x = x0
                h = y1
                y = frame_height - h - y0

                print(id, "(Label id) is labeling, Frame num :", num)
                for i in range(y, y + h):
                    for j in range(x, x + w):
                        if total_label_mask[i][j] != 0:
                            total_label_mask[i][j] = total_label_mask[i][j] * id

            if save:
                str_label = str(id)
                mask_dir = 'TEST_TRACKING_RESULT/test1/mask/' + str_label
                os.makedirs(mask_dir, exist_ok=True)

                save_mask = Image.fromarray(total_label_mask)
                save_mask.putpalette(palette)
                save_mask.save(os.path.join(mask_dir, '{:05d}.png'.format(num)))

                # rgba_dir = 'TEST_TRACKING_RESULT/test1/rgba/' + str_label
                # os.makedirs(rgba_dir, exist_ok=True)
                #
                # mask = Image.fromarray(total_label_mask)
                # mask = get_binary_map(np.array(mask))
                # rgba_data = convertRGBtoRGBA(frames[num], mask)
                # plt.imsave(os.path.join(rgba_dir, '{:05d}.png'.format(num)), rgba_data)

                print("Save Frame Num : ", num, "/", frames_num)

            total_label_masks.append(total_label_mask)
            tracked_id_box.clear()
            tracked_id_key.clear()

    elif label is not None and len(label_range) > 0:
        print("=======================================")
        print("Run Labeling Mask (Label : {})".format(label))
        label_frames = []
        label_num = len(label_range)

        for i, num in enumerate(label_range):
            current_mask = current_masks[i]  # // num -> i로 수정
            tracked_id_box = tracked_boxes[num]
            single_label_mask = np.copy(current_mask)
            total_label_mask = np.copy(current_mask)
            ids = list(tracked_id_box.keys())
            frame = frames[num]

            frame_height = current_masks.shape[1]
            frame_width = current_masks.shape[2]

            if save_mode == 0:
                # Labeling (Only input Label Value)
                single_label_mask = single_label_mask * int(1)

            else:
                # Labeling (All labels in frame)
                for id in ids:
                    total_label_mask = total_label_mask * int(id)

            label_box = tracked_id_box[label]
            crop_mask, crop_img = crop_roi_in_labeling_image(single_label_mask, frame, label_box, frame_height)

            if save:
                str_label = label_name
                if save_mode == 0:
                    save_single(outputPath, str_label, single_label_mask, crop_mask, crop_img, num)
                elif save_mode == 1:
                    save_all(outputPath, str_label, total_label_mask, frames, num)
                elif save_mode == 2:
                    save_all_singel(outputPath, str_label, single_label_mask, crop_mask, crop_img, total_label_mask, frames, num)

                # print("Save Frame : ({} / {})".format(num, ends), end="\r", flush=True)
                print('\r', "Save Frame : " + outputPath + "({} / {})".format(i, label_num - 1), end='')

            total_label_masks.append(total_label_mask)
            label_frames.append(frame)

        # save_dir = 'TEST_TRACKING_RESULT/test2/rgba/'
        # label_in_frame(total_label_masks, frames, label, save_dir)
    # return total_label_masks


def crop_roi_in_labeling_image(mask, image, box, frame_height):
    x0 = box[0]
    y0 = box[1]
    x1 = box[2]
    y1 = box[3]

    w = x1 - x0
    x = x0
    h = y1
    y = frame_height - h - y0

    # crop_mask = mask[y: y + h, x: x + w]
    # crop_img = image[y: y + h, x: x + w]
    crop_mask = mask[y0:y1, x0:x1]
    crop_img = image[y0:y1, x0:x1]

    return crop_mask, crop_img


def save_single(outputPath, str_label, single_label_mask, crop_mask, crop_img, num):
    # save_label_mask (single)
    mask_dir = outputPath + '/mask/' + str_label
    os.makedirs(mask_dir, exist_ok=True)

    # SAVE Pascal VOC Format Mask
    # save_mask = Image.fromarray(single_label_mask)
    # save_mask.putpalette(palette)
    # save_mask.save(os.path.join(mask_dir, '{:05d}_voc.png'.format(num)))

    # SAVE Binary Mask
    save_binary_mask = get_binary_map(single_label_mask)
    save_binary_mask = Image.fromarray(save_binary_mask)
    # save_binary_mask_resize = save_binary_mask.resize((2048, 858), Image.LANCZOS)
    # save_binary_mask_resize.save(os.path.join(mask_dir, '{:05d}.png'.format(num)))
    save_binary_mask.save(os.path.join(mask_dir, '{:05d}.png'.format(num)))

    # save_label_rgba (single)
    s_rgba_dir = outputPath + '/rgba/' + str_label
    os.makedirs(s_rgba_dir, exist_ok=True)

    binary_mask = get_binary_map(crop_mask)
    rgba_data = convertRGBtoRGBA(crop_img, binary_mask)
    plt.imsave(os.path.join(s_rgba_dir, '{:05d}.png'.format(num)), rgba_data)


def save_all(outputPath, str_label, total_label_mask, frames, num):
    # save_all_labels_mask
    mask_dir = outputPath + '/mask/all/' + str_label
    os.makedirs(mask_dir, exist_ok=True)

    save_mask = Image.fromarray(total_label_mask)
    save_mask.putpalette(palette)
    save_mask.save(os.path.join(mask_dir, '{:05d}.png'.format(num)))

    # save_all_labels_rgba
    rgba_dir = outputPath + '/rgba/all/' + str_label
    os.makedirs(rgba_dir, exist_ok=True)

    mask = Image.fromarray(total_label_mask)
    mask = get_binary_map(np.array(mask))
    rgba_data = convertRGBtoRGBA(frames[num], mask)
    plt.imsave(os.path.join(rgba_dir, '{:05d}.png'.format(num)), rgba_data)


def save_all_singel(outputPath, str_label, single_label_mask, crop_mask, crop_img, total_label_mask, frames, num):
    save_single(outputPath, str_label, single_label_mask, crop_mask, crop_img, num)
    save_all(outputPath, str_label, total_label_mask, frames, num)
