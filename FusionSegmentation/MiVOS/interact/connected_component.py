import cv2
import numpy as np
from PIL import Image
from util.palette import *
import time

palette = pal_color_map()

def get_binary_mask(mask):
    # print("mask shape", mask.shape)
    h, w = mask.shape
    # print("Get Binary Mask : mask.shape -> {}".format(mask.shape))

    binary_mask = mask * 255

    # for i in range(h):
    #     for j in range(w):
    #         if mask[i][j] != 0:
    #             mask[i][j] = mask[i][j] * 255

    return binary_mask
    # return mask

def stats_convert(stats):
    new_stats = []
    new_labels = []
    # print("STATS ConCom : ", stats)
    frame_w = stats[0][2]
    frame_h = stats[0][3]
    frame_s = stats[0][4]
    # masks_s = frame_w * frame_h - frame_s
    # min_mask_s = masks_s / (len(stats) - 1)
    # print("Frame Height :: ", frame_h)
    for i in range(len(stats)):
        # stats = [[0, 0, w, h, size], [x, y, w, h, size], [x, y, w, h, size] ... ]
        #           Frame info          label 1 info        label 2 info      ...  (size = pixel cnt in box)
        if i > 0:
            x, y, w, h, size = stats[i]
            # print("STAT C-C ", i, " : ", stats[i])
            label = i
            new_labels.append(label)

            # stat = [x, y, w, h, size]
            x0 = x
            y0 = frame_h - (y + h)
            x1 = x0 + w
            y1 = h

            new_stat = [x0, y0, x1, y1, size]
            # print("NEW STAT ", i, " : ", new_stat)
            # print("NEW STAT C-C ", i, " : ", new_stat)
            new_stats.append(new_stat)

    return new_labels, new_stats

def connected_component(current_masks):
    # GET Labels, Bbox info in frame -> forward to Tracking
    label_lists = []
    bounding_boxes = []
    masks = np.copy(current_masks)

    for mask in masks:
        # print("MASK SHAPE ::", mask.shape)
        binary = get_binary_mask(mask)
        # mask.putpalette(palette)

        cnt, labels, stats, centroids = cv2.connectedComponentsWithStats(binary)
        # print("STATS ConCom : ", stats)

        ## visualize stat info for Check
        # frame = frames[i]
        # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        # color = [(0, 0, 0), (0, 0, 255), (255, 0, 0), (0, 255, 0)]
        # for i in range(1, cnt):
        #     x, y, w, h, size = stats[i]
        #     colour = color[i]
        #     cv2.rectangle(frame, (x, y, w, h), colour)
        # cv2.imshow('bounding Box', frame)
        # cv2.waitKey()
        # cv2.destroyAllWindows()
        ##

        # stats = [[x0, y0, w, h]...]
        # print("cnt, labels, stats, centroids", cnt, labels, stats, centroids)
        # print("num_labes, labels", num_labels, label)
        # print("Frame num : ", i)
        label_list, bounding_box = stats_convert(stats)  # Using this value in "tracking.py"
        # bounding_box = [x0, y0, x1, y1]
        # print("Label List : ", label_list)
        # print("Bounding Box : ", bounding_box)

        label_lists.append(label_list)
        bounding_boxes.append(bounding_box)

        print("label : ", label_list)
        print("boxes : ", bounding_box)

    return label_lists, bounding_boxes
