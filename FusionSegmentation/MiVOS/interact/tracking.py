import cv2

def boxColor(id):
    color = [
        (255, 0, 0),
        (0, 255, 0),
        (0, 0, 255),
        (255, 255, 0),
        (255, 0, 255),
        (0, 255, 255),
        (238, 130, 238),
        (255, 165, 0),
        (75, 0, 130),
        (128, 128, 0),
        (0, 0, 128),
        (139, 69, 19),
        (255, 20, 147),
        (46, 139, 87),
        (184, 134, 11),
        (133, 255, 0),
        (255, 0, 235),
        (245, 0, 255),
        (255, 0, 122),
        (255, 245, 0),
        (10, 190, 212),
        (214, 255, 0),
        (0, 204, 255),
        (20, 0, 255),
        (255, 255, 0),
        (0, 153, 255),
        (0, 41, 255),
        (0, 255, 204),
        (41, 0, 255),
        (41, 255, 0),
        (173, 0, 255),
        (0, 245, 255),
        (71, 0, 255),
        (122, 0, 255),
        (0, 255, 184),
        (0, 92, 255),
        (184, 255, 0),
        (0, 133, 255),
        (255, 214, 0),
        (25, 194, 194),
        (102, 255, 0),
        (92, 0, 255),
    ]

    return color[id]

def draw_boundingBox(image, boxes_info):
    h, w, _ = image.shape
    for id in range(len(boxes_info)):

        value = boxes_info[id][0]  # [x0, y0, x1, y1] -> [left, bottom, right, top]

        if value != [-1, -1, -1, -1]:

            label = str(id)

            left = int(value[0])
            right = int(value[2])
            bottom = int(value[1])
            top = int(value[3])

            color = boxColor(id)

            thick = int((h + w) // 900)

            cv2.rectangle(image, (left, top), (right, bottom), color, thick)
            cv2.putText(image, label, (left, top -12), 0, 1e-2 * h, color, thick)
        # elif value == [-1, -1, -1, -1]:
            # print("ignore ", id, " box info")

    return image

def IoU(be_box, pr_box):
    # box = (x0, y0, x1, y1)
    b_box = be_box
    p_box = pr_box
    #
    # print(b_box)
    # print(p_box)

    b_box_area = (b_box[2] - b_box[0] + 1) * (b_box[3] - b_box[1] + 1)
    p_box_area = (p_box[2] - p_box[0] + 1) * (p_box[3] - p_box[1] + 1)

    x1 = max(b_box[0], p_box[0])
    y1 = max(b_box[1], p_box[1])
    x2 = min(b_box[2], p_box[2])
    y2 = min(b_box[3], p_box[3])

    w = max(0, x2 - x1 + 1)
    h = max(0, y2 - y1 + 1)

    inter = w * h
    iou = inter / (b_box_area + p_box_area - inter)

    return iou

def mean_iou(iou_array):
    if iou_array == []:
        mean = [0]
    else:
        num_obj = len(iou_array)
        total = 0

        for i in range(num_obj):
            total += iou_array[i]

        mean = total / num_obj
    return mean

def update_box(trackers, candidate, debug):
    if debug:
        # print("Update_BOX")
        print("Tracker Input : ", trackers)
        print("Candidate Input : ", candidate)
        # print("Tracker in objects : ", len(trackers))

    trackers_id = []
    # candidated = []
    t_matched = []
    c_matched = []
    del_trackers = []
    lost_trackers = {}
    final_matched = []
    frame_ious = []
    frame_mean_ious = []

    t_ids = list(trackers.keys())

    tracked_boxes = []

    for i in range(len(candidate)):
        c_obj = candidate[i][0]
        max_iou = 0
        candidated = list(range(len(candidate)))

        # Find match trackers in present frame, with candidate
        # for t_id in range(len(trackers)):  # [id0: [box_info0], id1: [box_info1], ...]
        for t_id in t_ids:  # [id0: [box_info0], id1: [box_info1], ...]
            t_val = trackers[t_id][0]  # [box_info] -> [x0, y0, x1, y1]
            # t_life = trackers[t_id][1]  # [life] -> 5

            if t_val != [-1, -1, -1, -1]:  # ignore tracking failed box_info
                iou = IoU(t_val, c_obj)

                if iou > max_iou and iou >= 0.4:
                    max_iou = iou

                    if t_id not in t_matched:
                        t_matched.append(t_id)

                    trackers[t_id] = candidate[i]
                    if debug:
                        print("update tracker from candidate : ", i, " -> tracker : ", t_id)

                    if i not in c_matched:
                        c_matched.append(i)

        frame_ious.append(max_iou)

        # Find match lost_trackers in previous frame, with candidate
        max_iou = 0
        for lt_id in range(len(lost_trackers)):
            lt_val = lost_trackers[lt_id]

            for i in range(len(candidated)):
                if i not in c_matched:
                    c_obj = candidate[i]
                    iou = IoU(lt_val, c_obj)

                    if iou > max_iou and iou >= 0.4:
                        max_iou = iou
                        find_id = lt_id
                        trackers[find_id] = candidate[i]
                        # print("*************************************update lost tracker from candidate")

                        c_matched.append(i)

    if debug:
        print("Matched List (trakcer) : ", t_matched)
        print("=== Search all candinates in frame is done ===")

    iou_mean = mean_iou(frame_ious)
    frame_mean_ious.append(iou_mean)

    # finding and life control & remove : tracker in present frame, which not match with any candidates
    for t in t_ids:
        if t not in t_matched:
            if trackers[t][1] == 0:  # Life is gone, Delete tracker
                trackers[t][0] = [-1, -1, -1, -1]
                # trackers[t][1] = -1
            elif trackers[t][1] > 0 or trackers[t][0] != [-1, -1, -1, -1]:
                trackers[t][1] -= 1

            # print("### delete tracker : ", t, ", cause not matched with any candidate")

    # Adding new tracker from candidate, which not match with any trackers in present frame
    for r in range(len(candidate)):
        if r not in c_matched:
            new_val = candidate[r]
            new_key = len(trackers) + 1
            trackers[new_key] = new_val  # Add new object in tracker, which is found in candidate
            # print("@@@ new object is detected ", "candidate : ", r, "is registered")
            # print("tracker is update key : ", trackers.keys())

    final_matched = t_matched.copy()

    trackers_id.clear()  # clear trackers id list
    t_matched.clear()  # clear registered list
    c_matched.clear()
    del_trackers.clear()

    # print("tracker result in objects : ", len(trackers))

    if debug:
        print("tracking result : ", trackers)

    return trackers, final_matched, frame_mean_ious

def main_tracker(candidate, tracker, frame, debug):
    # box_info => object's box in frame => [[x0, y0, x1, y1], [x0, y0, x1, y1], ...]

    update_tracker, matched_list_in_frame, frame_iou_mean = update_box(tracker, candidate, debug)
    # box_img = draw_boundingBox(frame, update_tracker)

    # return update_tracker, box_img
    return update_tracker, matched_list_in_frame, frame_iou_mean