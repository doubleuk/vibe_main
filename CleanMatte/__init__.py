import video_completion_copy as cm



def cleanMatte(imgPath, maskPath, outputPath):
    parser = cm.argparse.ArgumentParser()

    # video completion
    parser.add_argument('--seamless', action='store_true', default=True, help='Whether operate in the gradient domain')
    # parser.add_argument('--edge_guide', action='store_true', default=True, help='Whether use edge as guidance to complete flow')
    parser.add_argument('--edge_guide', action='store_true', help='Whether use edge as guidance to complete flow')
    parser.add_argument('--mode', default='object_removal', help="modes : object_removal / video_extrapolation")
    # source path
    parser.add_argument('--path', default=imgPath, help="dataset for evaluation")
    # mask path
    parser.add_argument('--path_mask', default=maskPath, help="mask for object removal")
    # output path
    parser.add_argument('--outroot', default=outputPath, help="output directory")
    parser.add_argument('--consistencyThres', dest='consistencyThres', default=cm.np.inf, type=float, help='flow consistency error threshold')
    parser.add_argument('--alpha', dest='alpha', default=0.1, type=float)
    parser.add_argument('--Nonlocal', dest='Nonlocal', default=False, type=bool)
    # RAFT
    # parser.add_argument('--model', default='../weight/raft-things.pth', help="restore checkpoint")
    parser.add_argument('--model', default=cm.cleanMattePath + 'weight/raft-things.pth', help="restore checkpoint")
    parser.add_argument('--small', action='store_true', help='use small model')
    parser.add_argument('--mixed_precision', action='store_true', help='use mixed precision')
    parser.add_argument('--alternate_corr', action='store_true', help='use efficent correlation implementation')
    # Deepfill
    parser.add_argument('--deepfill_model', default=cm.cleanMattePath + 'weight/imagenet_deepfill.pth', help="restore checkpoint")
    # Edge completion
    parser.add_argument('--edge_completion_model', default=cm.cleanMattePath + 'weight/edge_completion.pth', help="restore checkpoint")
    # extrapolation
    parser.add_argument('--H_scale', dest='H_scale', default=2, type=float, help='H extrapolation scale')
    parser.add_argument('--W_scale', dest='W_scale', default=2, type=float, help='W extrapolation scale')

    args = parser.parse_args()

    # cm.video_completion(args)

    cm.main(args)

    #   --mode object_removal \
    #   --path X:\uuK_\JW2uuk\Clean_matte\P51_walkway\original\deblur\
    #   --path_mask X:\uuK_\JW2uuk\Clean_matte\P51_walkway\mask\resized_2\
    #   --outroot W:\002_CleanMatte\IslandPlaza\002_test
    #   --seamless