import datetime
import time

debug = True

class timeStamper:
    def __init__(self):
        if debug == True:
            print("init")
        self.init
        # make init time for whole measuring of accumulated time

    def __new__(cls):
        if debug == True:
            print('new')

    def perLoopTimeAverage(self):
        if debug == True:
            print('loop time measure')


    def makeStamp(self):
        stamp = '['
        time = datetime.datetime.now().strftime('%y-%m-%d %H:%M:%S')
        if debug == True:
            print('make Time-stamp')
        stamp += time
        stamp += '] '
        return stamp

    def sprint(self, msg):
        stamp = self.makeStamp()
        print(stamp, end='')
        print(msg)
