from os import listdir, makedirs
from os.path import isfile, join, splitext
import os
import shutil
import sys
## need to install
import cv2
import numpy

import timeStamp
import multiprocessing as mp

divideFolder_prefix = 'dividedSource_'

def _getCurDirName(path):
    return (os.path.split(os.path.abspath(path)))[1]


make_margin = False
x_margin = 50
y_margin = 50


def imageSub(src, sub, x, y, w=0, h=0):
    return dst


def makeEdge(src):
    return dst

def divide(path, debug_mode=False, backup_postfix='_orgBak'):
    make_orgBackup = True

    imgCnt = 0
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

    if len(onlyfiles) == 0:
        print('there is no file in \'{}\' folder'.format(path))
        return -1

    makedirs(path, exist_ok=True)
    makedirs(join(path, 'lt'), exist_ok=True)
    makedirs(join(path, 'rt'), exist_ok=True)
    makedirs(join(path, 'lb'), exist_ok=True)
    makedirs(join(path, 'rb'), exist_ok=True)

    if make_orgBackup == True:
        makedirs(join(path, '..', _getCurDirName(path) + backup_postfix), exist_ok=True)

    dot = 0
    print("\nfolder path : " + path)
    print("")
    for n in range(0, len(onlyfiles)):
        print("\rdivide images (%.1f/100%%)" %(n/len(onlyfiles)*100), end="")
        for i in range(0, dot):
            print(".", end="")
        for i in range(0, 5 - dot):
            print(" ", end="")
        if dot == 5:
            dot = 0
        else:
            dot = dot + 1
        # set size from source image
        filepath = join(path, onlyfiles[n])
        onlyname, ext = splitext(onlyfiles[n])

        if ext != '.png' and ext != '.tif':
            continue

        img = cv2.imread(filepath, flags=cv2.IMREAD_UNCHANGED)
        height, width, channels = img.shape

        crop_width = int(width * 0.5)
        crop_height = int(height * 0.5)

        # left-top
        img_lt = img[0:crop_height, 0:crop_width]
        # right-top
        img_rt = img[0:crop_height, crop_width:width]
        # left-bottom
        img_lb = img[crop_height:height, 0:crop_width]
        # right-bottom
        img_rb = img[crop_height:height, crop_width:width]

        if debug_mode == True:
            cv2.imshow('left-top', img_lt)
            cv2.imshow('right-top', img_rt)
            cv2.imshow('left-bottom', img_lb)
            cv2.imshow('right-bottom', img_rb)

        if make_margin == True:
            # TODO
            x_margin = x_margin
            y_margin = y_margin

        fileName = join(path, 'lt', (onlyname + '.png'))
        cv2.imwrite(fileName, img_lt)
        fileName = join(path, 'rt', onlyname + '.png')
        cv2.imwrite(fileName, img_rt)
        fileName = join(path, 'lb', onlyname + '.png')
        cv2.imwrite(fileName, img_lb)
        fileName = join(path, 'rb', onlyname + '.png')
        cv2.imwrite(fileName, img_rb)

        if make_orgBackup == True:
            shutil.move(filepath, join(path, '..', _getCurDirName(path) + '_orgBak', onlyfiles[n]))

        if debug_mode == True:
            cv2.waitKey(1000 / fps)

        imgCnt = imgCnt + 1

    if debug_mode == True:
        cv2.destroyAllWindows()


subFolders = {'lt', 'rt', 'lb', 'rb'}
subFolders16 = {'1st_lt', '1st_rt', '1st_lb', '1st_rb'}
param1 = 3  # kernal width size for gaussian blur
param2 = param1  # kernal height size for gaussian blur
param3 = 0  # sigma X for gaussian blur


def divideFor16(path, debug_mode=False, makeBlurForMpeg=False):
    makeOrgFolder = True

    imgCnt = 0
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

    if len(onlyfiles) == 0:
        print('there is no file in \'{}\' folder'.format(path))
        return -1

    makedirs(path, exist_ok=True)
    for folderPath in subFolders16:
        makedirs(join(path, folderPath), exist_ok=True)

    if makeOrgFolder == True:
        makedirs(join(path, _getCurDirName(path)), exist_ok=True)

    dot = 0
    print("\nfolder path : " + path)
    for n in range(0, len(onlyfiles)):
        print("\rdivide images (%.1f/100%%)" %(n/len(onlyfiles)*100), end="")
        for i in range(0, dot):
            print(".", end="")
        for i in range(0, 5 - dot):
            print(" ", end="")
        if dot == 5:
            dot = 0
        else:
            dot = dot + 1
        # set size from source image
        filepath = join(path, onlyfiles[n])
        onlyname, ext = splitext(onlyfiles[n])

        if ext != '.png' and ext != '.tif':
            continue

        img = cv2.imread(filepath, flags=cv2.IMREAD_UNCHANGED)
        height, width, channels = img.shape

        if makeBlurForMpeg == True:
            img = cv2.GaussianBlur(img, (param1, param2), param3)

        crop_width = int(width * 0.5)
        crop_height = int(height * 0.5)

        # left-top
        img_lt = img[0:crop_height, 0:crop_width]
        # right-top
        img_rt = img[0:crop_height, crop_width:width]
        # left-bottom
        img_lb = img[crop_height:height, 0:crop_width]
        # right-bottom
        img_rb = img[crop_height:height, crop_width:width]

        if debug_mode == True:
            cv2.imshow('left-top', img_lt)
            cv2.imshow('right-top', img_rt)
            cv2.imshow('left-bottom', img_lb)
            cv2.imshow('right-bottom', img_rb)

        if make_margin == True:
            # TODO
            x_margin = x_margin
            y_margin = y_margin

        fileName = join(path, '1st_lt', (onlyname + '.png'))
        cv2.imwrite(fileName, img_lt)
        fileName = join(path, '1st_rt', onlyname + '.png')
        cv2.imwrite(fileName, img_rt)
        fileName = join(path, '1st_lb', onlyname + '.png')
        cv2.imwrite(fileName, img_lb)
        fileName = join(path, '1st_rb', onlyname + '.png')
        cv2.imwrite(fileName, img_rb)

        if makeOrgFolder == True:
            shutil.move(filepath, join(path, _getCurDirName(path), onlyfiles[n]))

        if debug_mode == True:
            cv2.waitKey(1000 / fps)

        imgCnt = imgCnt + 1

    if debug_mode == True:
        cv2.destroyAllWindows()


def merge(path, debug_mode=False, targetPath='merged', del_mode=False):
    imgCnt = 0
    onlyfiles = [f for f in listdir(join(path, 'lt')) if isfile(join(path, 'lt', f))]

    if len(onlyfiles) == 0:
        print('there is no file in \'{}\' folder'.format(path))
        return -1

    if targetPath == '':
        outputPath = path
    else:
        outputPath = join(path, targetPath)
        makedirs(outputPath, exist_ok=True)

    print("Divided images from %s" % path)
    for n in range(0, len(onlyfiles)):
        # set size from source image
        filepath = join(path, onlyfiles[n])
        _, ext = splitext(filepath)

        if ext != '.png' and ext != '.tif':
            if del_mode == True:
                if onlyfiles[n] == 'Thumbs.db':
                    os.remove(filepath)
            continue

        print("\r[%d/%d] merging...(file name : %s)\r" % (n + 1, len(onlyfiles), onlyfiles[n]), end='')
        try:
            # left-top
            img_lt = cv2.imread(join(path, 'lt', onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
            if len(img_lt) == 0:
                print("there is no file : {}".format(join(path, 'lt', onlyfiles[n])))
                continue
            # right-top
            img_rt = cv2.imread(join(path, 'rt', onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
            if len(img_rt) == 0:
                print("there is no file : {}".format(join(path, 'rt', onlyfiles[n])))
                continue
            # left-bottom
            img_lb = cv2.imread(join(path, 'lb', onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
            if len(img_lb) == 0:
                print("there is no file : {}".format(join(path, 'lb', onlyfiles[n])))
                continue
            # right-bottom
            img_rb = cv2.imread(join(path, 'rb', onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
            if len(img_rb) == 0:
                print("there is no file : {}".format(join(path, 'rb', onlyfiles[n])))
                continue
        except:
            print('some error has occured, file name : %s' %onlyfiles[n])
            continue
        if debug_mode == True:
            cv2.imshow('left-top', img_lt)
            cv2.imshow('right-top', img_rt)
            cv2.imshow('left-bottom', img_lb)
            cv2.imshow('right-bottom', img_rb)

        if make_margin == True:
            # TODO : blend margin
            x_margin = x_margin
            y_margin = y_margin

        # merge
        img_upper = cv2.hconcat([img_lt, img_rt])
        img_lower = cv2.hconcat([img_lb, img_rb])
        img = cv2.vconcat([img_upper, img_lower])

        # fileName = join(outputPath, '%06d.png' % imgCnt)
        fileName = join(outputPath, onlyfiles[n])
        cv2.imwrite(fileName, img)

        if debug_mode == True:
            cv2.imshow('merged', img)
            cv2.waitKey(1000 / fps)

        if del_mode == True:
            del_path = join(path, 'lt', onlyfiles[n])
            if os.path.isfile(del_path) == True:
                os.remove(del_path)
            del_path = join(path, 'rt', onlyfiles[n])
            if os.path.isfile(del_path) == True:
                os.remove(del_path)
            del_path = join(path, 'lb', onlyfiles[n])
            if os.path.isfile(del_path) == True:
                os.remove(del_path)
            del_path = join(path, 'rb', onlyfiles[n])
            if os.path.isfile(del_path) == True:
                os.remove(del_path)

        imgCnt = imgCnt + 1

    if del_mode == True:
        src_folders = {'lt', 'rt', 'lb', 'rb'}
        for subFolder in src_folders:
            onlyfiles = [f for f in listdir(join(path, subFolder)) if isfile(join(path, subFolder, f))]
            if len(onlyfiles) == 0:
                os.rmdir(join(path, subFolder))
            else:
                print('Cannot delete %s folder, there is not empty folder. check inside' %subFolders)

    if debug_mode is True:
        cv2.destroyAllWindows()

    print("Merge complete")


def split(path, limit, is_unzipsize=True):
    start_num = 0

    # limit = 1.0  # giga-byte
    limit = limit * 1024 * 1024 * 1024  # byte

    onlyFiles = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

    accumulatedByte = 0
    nFolders = 0
    holdPath = ""
    holdFileName = ""
    holdByte = 0

    for n in range(0, len(onlyFiles)):
        if accumulatedByte == 0:
            # make new folder
            targetFolderPath = os.path.join(path, divideFolder_prefix + '%03d' % (start_num + nFolders))
            while (os.path.exists(targetFolderPath)):
                print("{} is already exist!!".format(targetFolderPath))
                nFolders = nFolders + 1
                targetFolderPath = os.path.join(path, divideFolder_prefix + '%03d' % (start_num + nFolders))
            os.makedirs(targetFolderPath, exist_ok=True)
            nFolders = nFolders + 1

            if holdPath != "":
                shutil.move(holdPath, os.path.join(targetFolderPath, holdFileName))
                print("\rcopy {} to {}".format(filePath, (os.path.join(targetFolderPath, holdFileName))), end='')

                accumulatedByte = holdByte

        filePath = os.path.join(path, onlyFiles[n])

        _, ext = os.path.splitext(filePath)
        if ext == '.py':
            continue
        if is_unzipsize == True:
            img = cv2.imread(filePath, flags=cv2.IMREAD_UNCHANGED)
            byte = sys.getsizeof(img)
        else:
            byte = os.path.getsize(filePath)

        if byte + accumulatedByte > limit:
            if byte > limit:
                print("{} is too big to copy for the current limitation (limit : {} GB, file size : {:.3f} GB)".format(
                    onlyFiles[n], limit / (1024.0 * 1024.0 * 1024.0), byte / (1024.0 * 1024.0 * 1024.0)))

            holdPath = filePath
            holdFileName = onlyFiles[n]
            holdByte = byte

            print("{} : {:.3f} GB".format(targetFolderPath, accumulatedByte / (1024.0 * 1024.0 * 1024.0)))

            accumulatedByte = 0
        else:
            shutil.move(filePath, os.path.join(targetFolderPath, onlyFiles[n]))
            print("\rcopy {} to {}".format(filePath, (os.path.join(targetFolderPath, onlyFiles[n]))), end='')

            accumulatedByte = accumulatedByte + byte

    if (start_num + nFolders) == 1:
        # if there is the only folder, don't make divided folder
        gather(path, True)
        is_divided = False
    else:
        is_divided = True

    return is_divided


def gather(path, delfolder):
    start_num = 0

    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]
    for n in range(0, len(onlyFolders)):
        folderPath = os.path.join(path, onlyFolders[n])
        onlyFiles = [f for f in os.listdir(folderPath) if os.path.isfile(os.path.join(folderPath, f))]

        for m in range(0, len(onlyFiles)):
            filePath = os.path.join(folderPath, onlyFiles[m])
            _, ext = os.path.splitext(filePath)
            if ext == '.py':
                continue

            shutil.move(filePath, os.path.join(path, onlyFiles[m]))
            print("\rcopy {} to {}".format(filePath, (os.path.join(path, onlyFiles[m]))), end='')

        if delfolder == True:
            os.rmdir(folderPath)

# def copy_folder(src_path, dst_path, delfolder, id=-1):


def gather_mp(path, delfolder):
    num_multiprocessor = 10

    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]
    num_folders = len(onlyFolders)
    if num_multiprocessor > num_folders:
        num_multiprocessor = num_folders
        step = 1
    else:
        step = num_folders / num_multiprocessor

    def gather_singleFolder(start, end, path, delfolder):
        for n in range(start, end):
            folderPath = os.path.join(path, onlyFolders[n])
            onlyFiles = [f for f in os.listdir(folderPath) if os.path.isfile(os.path.join(folderPath, f))]

            for m in range(0, len(onlyFiles)):
                filePath = os.path.join(folderPath, onlyFiles[m])
                _, ext = os.path.splitext(filePath)
                if ext == '.py':
                    continue

                shutil.move(filePath, os.path.join(path, onlyFiles[m]))
                print("\rcopy {} to {}".format(filePath, (os.path.join(path, onlyFiles[m]))), end='')

            if delfolder == True:
                os.rmdir(folderPath)

    # make arguments
    # for p in range(0, num_multiprocessor):


    # p = mp.Process(target=gather_singleFolder, args=())


def rename(path, start_num, prefix_format, is_folder):
    path = os.path.join(path, 'renamed')
    makedirs(path, exist_ok=True)

    if is_folder == True:
        onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]

        if len(onlyFolders) == 0:
            print('there is no folder in \'{}\' folder'.format(path))
            return -1

        for n in range(0, len(onlyFolders)):
            os.rename(os.path.join(path, onlyFolders[n]), os.path.join(path, prefix_format + '%04d' % (start_num + n)))

    else:  # file case
        onlyFiles = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

        if len(onlyFiles) == 0:
            print('there is no file in \'{}\' folder'.format(path))
            return -1

        for n in range(0, len(onlyFiles)):
            _, ext = os.path.splitext(onlyFiles[n])

            if ext == '.py':
                continue

            os.rename(os.path.join(path, onlyFiles[n]),
                      os.path.join(path, prefix_format + '%06d' % (start_num + n) + ext))


def makeVideoWithCutWithCut(path, videoPrefix, fps, resize_factor, scene_cutting, make_margin, margin_w=0, margin_h=0,
                            debug_mode=False):
    min_frame_per_cut = int(fps / 2)
    outputPath = './output/'
    makedirs(outputPath, exist_ok=True)

    print("image path = " + path)

    print_filename = True

    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    images = numpy.empty(len(onlyfiles), dtype=object)
    old_hist_1ch = 0
    old_hist_sobel = 0

    videoCnt = 0
    frame = 0
    cutting_proof = 0
    prevImg = 0

    numCuda = cv2.cuda.getCudaEnabledDeviceCount()
    if numCuda != 0:
        cv2.cuda.setDevice(0)

    dot = 0
    for n in range(0, len(onlyfiles)):
        print("\rmake video", end="")
        for i in range(0, dot):
            print(".", end="")
        for i in range(0, 5 - dot):
            print(" ", end="")
        if dot == 5:
            dot = 0
        else:
            dot = dot + 1

        # set size from source image
        img = cv2.imread(join(path, onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
        height, width, channels = img.shape

        if resize_factor != 1:
            resized_width = int(width * resize_factor)
            resized_height = int(height * resize_factor)
            resizedImg = cv2.resize(img, (resized_width, resized_height))
        else:
            ## use original size
            resized_width = width
            resized_height = height
            resizedImg = img

        if debug_mode == True:
            dispImg = cv2.resize(img, (int(resized_width / 2), int(resized_height / 2)))

        if make_margin == True:
            resizedImg = cv2.copyMakeBorder(resizedImg, margin_y, margin_y, margin_x, margin_x, cv2.BORDER_CONSTANT)
            resized_width = resized_width + (margin_x * 2)
            resized_height = resized_height + (margin_y * 2)

        # video initialize
        if videoCnt == 0:
            fourcc = cv2.VideoWriter_fourcc(*'DIVX')
            if scene_cutting == True:
                videoName = videoPrefix + '_%05d.avi' % videoCnt
            else:
                videoName = videoPrefix + '.avi'
            out = cv2.VideoWriter(join(outputPath, videoName), fourcc, fps, (resized_width, resized_height))
            videoCnt = videoCnt + 1

            if scene_cutting == True:
                print("make new cut, cut num : %d" % videoCnt)

        if scene_cutting == True:
            # 1ch histogram
            hist = cv2.calcHist([resizedImg], [0], None, [64], [0, 256])
            cv2.normalize(hist, hist, 0, 100, cv2.NORM_MINMAX)

            sum = 0
            if old_hist_1ch != 0:
                for i in range(0, len(hist)):
                    sum = sum + abs(hist[i] - old_hist_1ch[i])

            old_hist_1ch = hist

            # 3ch histogram

            # not yet

            # edge histogram
            gray = cv2.cvtColor(resizedImg, cv2.COLOR_BGR2GRAY)
            gray = cv2.GaussianBlur(gray, (3, 3), 0)
            sobelX = cv2.Sobel(gray, -1, 1, 0)
            sobelY = cv2.Sobel(gray, -1, 0, 1)

            sobel = abs(sobelX) + abs(sobelY)

            hist_sobel = cv2.calcHist([sobel], [0], None, [64], [0, 256])
            cv2.normalize(hist_sobel, hist_sobel, 0, 100, cv2.NORM_MINMAX)

            sum_sobel = 0
            if old_hist_sobel != 0:
                for i in range(0, len(hist_sobel)):
                    sum_sobel = sum_sobel + abs(hist_sobel[i] - old_hist_sobel[i])

            old_hist_sobel = hist_sobel

            if debug_mode == True:
                # cv2.imshow("sobel edge", sobel)
                # cv2.imshow('resized image ', resizedImg)
                cv2.putText(prevImg, 'Previous Image', (15, 30), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0))
                cv2.imshow('Previous Image', prevImg)
                prevImg = dispImg.copy()
                cv2.putText(dispImg, 'Current Image, #' + str(videoCnt - 1), (15, 30), cv2.FONT_HERSHEY_PLAIN, 1,
                            (0, 255, 0))
                cv2.putText(dispImg, "1ch histogram difference : %d" % sum, (15, 60), cv2.FONT_HERSHEY_PLAIN, 1,
                            (0, 255, 0))
                cv2.putText(dispImg, "edge histogram difference : %d" % sum_sobel, (15, 90), cv2.FONT_HERSHEY_PLAIN, 1,
                            (0, 255, 0))

                print("1ch histogram difference : %d" % sum)
                print("edge histogram difference : %d" % sum_sobel)

            if cutting_proof == 0:
                if int(sum) > 500:
                    # Manual selection off
                    # ch = cv2.waitKey(0)
                    # if ch is ord("y"):
                    # make new cut
                    fourcc = cv2.VideoWriter_fourcc(*'DIVX')
                    videoName = cutVideoPrefix + '%05d.avi' % videoCnt
                    out = cv2.VideoWriter(join(outputPath, videoName), fourcc, fps, (resized_width, resized_height))
                    videoCnt = videoCnt + 1
                    cutting_proof = min_frame_per_cut
                    out.write(resizedImg)
                    print("make new cut, cut num : %d" % videoCnt)
                    # else:
                    # out.write(resizedImg)
                    # print("no")

                    if debug_mode == True:
                        cv2.putText(dispImg, "/////SSAK-DDUK//////", (15, 120), cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 255))
                        cv2.imshow('Current Image', dispImg)
                        cv2.waitKey(500)
                else:
                    if debug_mode == True:
                        cv2.imshow('Current Image', dispImg)
                        cv2.waitKey(int(1000 / fps))
                    out.write(resizedImg)
            else:
                cutting_proof = cutting_proof - 1
                if debug_mode == True:
                    cv2.imshow('Current Image', dispImg)
                    cv2.waitKey(100)
                out.write(resizedImg)
            frame = frame + 1
        else:  # scene_cutting == False
            if print_filename == True:
                cv2.putText(resizedImg, onlyfiles[n], (15, 30), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255))
            out.write(resizedImg)

    print("\ndone")

    out.release()

    if debug_mode == True:
        cv2.destroyAllWindows()


def makeVideo(path, videoName, fps, resize_factor=1, high_qual=False):
    outputPath = join(path, '../output')
    makedirs(outputPath, exist_ok=True)

    print("image path = " + path)

    print_filename = True

    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

    videoCnt = 0
    frame = 0
    prevImg = 0

    numCuda = cv2.cuda.getCudaEnabledDeviceCount()
    if numCuda != 0:
        cv2.cuda.setDevice(0)

    dot = 0
    for n in range(0, len(onlyfiles)):
        try:
            print("\rmake video", end="")
            for i in range(0, dot):
                print(".", end="")
            for i in range(0, 5 - dot):
                print(" ", end="")
            if dot == 5:
                dot = 0
            else:
                dot = dot + 1

            # set size from source image
            # img = cv2.imread(join(path, onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
            _, ext = splitext(onlyfiles[n])
            if _IsThisImageFile(ext) == False:
                continue

            img = cv2.imread(join(path, onlyfiles[n]))
            height, width, channels = img.shape

            if resize_factor != 1:
                resized_width = int(width * resize_factor)
                resized_height = int(height * resize_factor)
                resizedImg = cv2.resize(img, (resized_width, resized_height), interpolation=None)
            else:
                ## use original size
                resized_width = width
                resized_height = height
                resizedImg = img

            if high_qual == True:
                resizedImg = cv2.cvtColor(resizedImg, cv2.COLOR_BGR2RGB)

            # video initialize
            if videoCnt == 0:
                if high_qual == True:
                    fourcc = cv2.VideoWriter_fourcc(*'YV12')
                    videoName = videoName + '.avi'
                else:
                    fourcc = cv2.VideoWriter_fourcc(*'DIVX')
                    videoName = videoName + '.avi'
                out = cv2.VideoWriter(join(outputPath, videoName), fourcc, fps, (resized_width, resized_height))
                videoCnt = videoCnt + 1
            cv2.putText(resizedImg, onlyfiles[n], (15, 30), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255))
            out.write(resizedImg)
        except:
            print('error, filename : %s' %onlyfiles[n])

    print("\ndone")

    out.release()


def makeCompVideo(pathA, pathB, videoName, fps, resize_factor):
    min_frame_per_cut = int(fps / 2)
    outputPath = join(pathB, 'output')
    makedirs(outputPath, exist_ok=True)

    print("image path A (Left) = " + pathA)
    print("image path B (Right) = " + pathB)

    onlyFilesA = [f for f in listdir(pathA) if isfile(join(pathA, f))]
    onlyFilesB = [f for f in listdir(pathB) if isfile(join(pathB, f))]

    videoCnt = 0

    numCuda = cv2.cuda.getCudaEnabledDeviceCount()
    if numCuda != 0:
        cv2.cuda.setDevice(0)

    dot = 0
    for n in range(0, len(onlyFilesA)):
        print("\rmake video", end="")
        for i in range(0, dot):
            print(".", end="")
        for i in range(0, 5 - dot):
            print(" ", end="")
        if dot == 5:
            dot = 0
        else:
            dot = dot + 1

        # set size from source image
        # imgA = cv2.imread(join(pathA, onlyFilesA[n]), flags=cv2.IMREAD_UNCHANGED)
        # imgB = cv2.imread(join(pathB, onlyFilesB[n]), flags=cv2.IMREAD_UNCHANGED)
        try:
            imgA = cv2.imread(join(pathA, onlyFilesA[n]))
            imgB = cv2.imread(join(pathB, onlyFilesB[n]))
            height, width, channels = imgB.shape
        except:
            out.release()

        if resize_factor != 1:
            resized_width = int(width * resize_factor)
            resized_height = int(height * resize_factor)
            resizedImgA = cv2.resize(imgA, (resized_width, resized_height))
            resizedImgB = cv2.resize(imgB, (resized_width, resized_height))
        else:
            ## use original size
            resized_width = width
            resized_height = height

            heightA, widthA, channels = imgA.shape

            if heightA != height or widthA != width:
                resizedImgA = cv2.resize(imgA, (resized_width, resized_height), interpolation=None)
            else:
                resizedImgA = imgA

            resizedImgB = imgB

        # if debug_mode == True:
        #     dispImg = cv2.resize(img, (int(resized_width / 2), int(resized_height / 2)))

        # if make_margin == True:
        #     resizedImg = cv2.copyMakeBorder(resizedImg, margin_y, margin_y, margin_x, margin_x, cv2.BORDER_CONSTANT)
        #     resized_width = resized_width + (margin_x * 2)
        #     resized_height = resized_height + (margin_y * 2)

        # video initialize
        if videoCnt == 0:
            fourcc = cv2.VideoWriter_fourcc(*'DIVX')

            out = cv2.VideoWriter(join(outputPath, videoName + '.avi'), fourcc, fps,
                                  (resized_width * 2, resized_height))
            videoCnt = 1
        # if print_filename == True:
        #     cv2.putText(resizedImg, onlyfiles[n], (15, 30), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255))
        out.write(cv2.hconcat([resizedImgA, resizedImgB]))

    print("\ndone")

    out.release()

    # if debug_mode == True:
    #     cv2.destroyAllWindows()


def tifToPng(path):
    outputPath = './'
    videoFolderTitle = 'png'
    cutPrefix = 'full'

    print("image path = " + imagePath)

    onlyfiles = [f for f in listdir(imagePath) if isfile(join(imagePath, f))]
    images = numpy.empty(len(onlyfiles), dtype=object)
    old_hist_1ch = 0
    old_hist_sobel = 0

    sceneCnt = 0
    folderPath = join(outputPath, videoFolderTitle, cutPrefix + '_%04d' % sceneCnt)
    makedirs(folderPath, 0o777, True)

    frame = 0

    resized_width = 0
    resized_height = 0
    cutting_proof = 0

    prevImg = 0

    numCuda = cv2.cuda.getCudaEnabledDeviceCount()
    if numCuda != 0:
        cv2.cuda.setDevice(0)

    sceneCnt = 0
    imgCnt = 0

    for n in range(0, len(onlyfiles)):
        # set size from source image
        img = cv2.imread(join(imagePath, onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
        height, width, channels = img.shape

        if resize_factor != 1:
            resized_width = int(width * resize_factor)
            resized_height = int(height * resize_factor)

            resizedImg = cv2.resize(img, (resized_width, resized_height))
        else:
            resizedImg = img

        if debug_mode is True:
            dispImg = cv2.resize(img, (int(resized_width / 2), int(resized_height / 2)))

        if make_margin == True:
            resizedImg = cv2.copyMakeBorder(resizedImg, y_margin, y_margin, x_margin, x_margin, cv2.BORDER_CONSTANT)
            resized_width = resized_width + (x_margin * 2)
            resized_height = resized_height + (y_margin * 2)

        if sceneCutting == True:
            # 1ch histogram
            hist = cv2.calcHist([resizedImg], [0], None, [64], [0, 256])
            cv2.normalize(hist, hist, 0, 100, cv2.NORM_MINMAX)

            sum = 0
            if old_hist_1ch != 0:
                for i in range(0, len(hist)):
                    sum = sum + abs(hist[i] - old_hist_1ch[i])

            old_hist_1ch = hist

            # 3ch histogram

            # not yet

            # edge histogram
            gray = cv2.cvtColor(resizedImg, cv2.COLOR_BGR2GRAY)
            gray = cv2.GaussianBlur(gray, (3, 3), 0)
            sobelX = cv2.Sobel(gray, -1, 1, 0)
            sobelY = cv2.Sobel(gray, -1, 0, 1)

            sobel = abs(sobelX) + abs(sobelY)

            hist_sobel = cv2.calcHist([sobel], [0], None, [64], [0, 256])
            cv2.normalize(hist_sobel, hist_sobel, 0, 100, cv2.NORM_MINMAX)

            sum_sobel = 0
            if old_hist_sobel != 0:
                for i in range(0, len(hist_sobel)):
                    sum_sobel = sum_sobel + abs(hist_sobel[i] - old_hist_sobel[i])

            old_hist_sobel = hist_sobel

            if debug_mode is True:
                # cv2.imshow("sobel edge", sobel)
                # cv2.imshow('resized image', resizedImg)
                cv2.putText(prevImg, 'Previous Image', (15, 30), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0))
                cv2.imshow('Previous Image', prevImg)
                prevImg = dispImg.copy()
                cv2.putText(dispImg, 'Current Image, #' + str(frame), (15, 30), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0))
                cv2.putText(dispImg, "1ch histogram difference : %d" % sum, (15, 60), cv2.FONT_HERSHEY_PLAIN, 1,
                            (0, 255, 0))
                cv2.putText(dispImg, "edge histogram difference : %d" % sum_sobel, (15, 90), cv2.FONT_HERSHEY_PLAIN, 1,
                            (0, 255, 0))

                print("1ch histogram difference : %d" % sum)
                print("edge histogram difference : %d" % sum_sobel)

            if cutting_proof == 0:
                if int(sum) > 500:
                    # Manual selection off
                    # ch = cv2.waitKey(0)
                    # if ch is ord("y"):

                    # make new cut (folder)

                    # minimum frames
                    if imgCnt > 30:
                        sceneCnt = sceneCnt + 1
                        print("make new cut, cut num : %d" % sceneCnt)
                    else:
                        print("find new cut, but latest cut is too small")
                    folderPath = join(outputPath, videoFolderTitle, cutPrefix + '_%04d' % sceneCnt)
                    makedirs(folderPath, 0o777, True)

                    # image number reset
                    imgCnt = 0

                    if debug_mode is True:
                        cv2.putText(dispImg, "/////SSAK-DDUK//////", (15, 120), cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 255))
                        cv2.imshow('Current Image', dispImg)
                        cv2.waitKey(500)
                else:
                    if debug_mode is True:
                        cv2.imshow('Current Image', dispImg)
                        cv2.waitKey(int(1000 / fps))
            else:
                cutting_proof = cutting_proof - 1
                if debug_mode is True:
                    cv2.imshow('Current Image', dispImg)
                    cv2.waitKey(100)
        else:
            if print_filename == True:
                cv2.putText(resizedImg, onlyfiles[n], (15, 30), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255))

        fileName = join(folderPath, '%06d.png' % imgCnt)
        cv2.imwrite(fileName, resizedImg)
        imgCnt = imgCnt + 1
        frame = frame + 1


# test1:
#   name: Test\L
#   gt_seq_dir: X:\uuK_\APEX_20201211\dpx\S070_0015_077_SB1845\v002\L\png
#   lr_seq_dir: X:\uuK_\APEX_20201211\dpx\S070_0015_077_SB1845\v002\L\png
#   filter_file: ~
#   num_workers: 4
#   pin_memory: true

list_fileName = './testScript.txt'
qt_folders = {"lt", "rt", "lb", "rb"}


def _printTestScript2(path, f, title, subTitles, test_num):
    cur_folder_name = os.path.normpath(path)

    if title == "":
        title = cur_folder_name

    ## test num
    f.write("\ttest{}\n".format(test_num))
    ## name (and target folder)
    if option == "":
        f.write("\t\tname: {}\n".format(title))
    else:
        f.write("\t\tname: {}\\{}\n".format(title, option))
    ## source folder
    f.write("\t\tgt_seq_dir: {}\n".format(os.path.dirname(path)))
    f.write("\t\tlr_seq_dir: {}\n".format(os.path.dirname(path)))

    ## default info.
    f.write("\t\tfilter_file: ~\n")
    f.write("\t\tnum_workers: 4\n")
    f.write("\t\tpin_memory: true\n")
    f.write("\n")

    return test_num + 1


def _makeTestScript(path, title, subTitles, test_num):
    subFolders16 = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]

    subFolders16

    if len(onlyFolders) == 0:
        f = open(list_fileName, "at", encoding='utf-8')
        cur_folder_name = os.path.normpath(path)
        for folderPath in qt_folders:
            if cur_folder_name == folderPath:
                subTitles.append(cur_folder_name)

        test_num = _printTestScript2()
        f.close()
        return test_num

    else:
        for folderPath in onlyFolders:
            if len(onlyFolders) > 2:
                ## main folder name
                title = folderPath

            if folderPath == 'L' or folderPath == 'l':
                lr = folderPath
            elif folderPath == 'R' or folderPath == 'r':
                lr = folderPath

            base = os.path.join(path, folderPath)
            test_num = autoForTecoValidSets(fileName, base, title, lr, test_num)
        return test_num


def makeTestScript(path):
    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]

    for folderPath in onlyFolders:
        title = folderPath

        _makeTestScript(os.path.join(path, title), title)


def _checkprohibitedFolder(path):
    cur_folder_name = _getCurDirName(path)

    folderList = {"org", "lt", "rt", "lb", "rb"}
    flag = False
    for folder in folderList:
        if cur_folder_name == cur_folder_name:
            flag = True
            break

    return flag


def _printTestScript(path, f, title, option, test_num):
    cur_folder_name = os.path.normpath(path)

    if title == "":
        title = cur_folder_name

    ## test num
    f.write("  test%03d:\n" % test_num)
    ## name (and target folder)
    if len(option) == 0:
        f.write("    name: {}\n".format(title))
    else:
        f.write("    name: {}\\".format(title))
        for subFolder in option:
            f.write("{}\\".format(subFolder))
    ## source folder
    # f.write("\t\tgt_seq_dir: {}\n".format(os.path.dirname(path)))
    # f.write("\t\tlr_seq_dir: {}\n".format(os.path.dirname(path)))
    f.write("    gt_seq_dir: {}\n".format(os.path.abspath(path)))
    f.write("    lr_seq_dir: {}\n".format(os.path.abspath(path)))

    ## default info.
    f.write("    filter_file: ~\n")
    f.write("    num_workers: 4\n")
    f.write("    pin_memory: true\n")
    f.write("\n")

    return test_num + 1


def _printTestScript_header(list_path):
    f = open(list_path, "at", encoding='utf-8')

    f.write("# basic configs\n")
    f.write("scale: 4\n")
    f.write("manual_seed: 0\n")
    f.write("verbose: false\n\n\n")

    # dataset configs
    f.write("dataset:\n")
    f.write("  degradation:\n")
    f.write("    type: BD\n")
    f.write("    sigma: 1.5\n")

    f.close()


def _printTestScript_tail(list_path, output_path):
    f = open(list_path, "at", encoding='utf-8')

    f.write("# model configs\n")
    f.write("model:\n")
    f.write("  name: FRVSR\n\n\n")

    f.write("  generator:\n")
    f.write("    name: FRNet  # frame-recurrent network\n")
    f.write("    in_nc: 3\n")
    f.write("    out_nc: 3\n")
    f.write("    nf: 64\n")
    f.write("    nb: 10\n")
    f.write("    load_path: pretrained_models / TecoGAN_BD_iter500000.pth\n\n\n")

    f.write("# test configs\n")
    f.write("test:\n")
    f.write("# whether to save the SR results\n")
    f.write("#  save_res: true\n")
    f.write("#  res_dir: results\n")
    f.write("  save_res: true\n")
    f.write("  res_dir: %s\n\n\n" % output_path)

    f.write("  # temporal padding\n")
    f.write("  padding_mode: reflect\n")
    f.write("  num_pad_front: 5\n")
    f.write("# basic configs\n")
    f.write("scale: 4\n")
    f.write("manual_seed: 0\n")
    f.write("verbose: false\n\n\n")

    # dataset configs
    f.write("dataset:\n")
    f.write("  degradation:\n")
    f.write("    type: BD\n")
    f.write("    sigma: 1.5\n")

    f.close()


def _printTestScript_test(path, list_path, title, test_num, option=[]):
    cur_folder_name = os.path.normpath(path)

    if title == "":
        title = cur_folder_name

    f = open(list_path, "at", encoding='utf-8')

    ## test num
    f.write("  test%03d:\n" % test_num)
    ## name (and target folder)
    if len(option) == 0:
        f.write("    name: {}\n".format(title))
    else:
        f.write("    name: {}".format(title))
        for subFolder in option:
            f.write("\\{}".format(subFolder))
        f.write("\n")
    ## source folder
    # f.write("\t\tgt_seq_dir: {}\n".format(os.path.dirname(path)))
    # f.write("\t\tlr_seq_dir: {}\n".format(os.path.dirname(path)))
    f.write("    gt_seq_dir: {}\n".format(os.path.abspath(path)))
    f.write("    lr_seq_dir: {}\n".format(os.path.abspath(path)))

    ## default info.
    f.write("    filter_file: ~\n")
    f.write("    num_workers: 4\n")
    f.write("    pin_memory: true\n")
    f.write("\n")

    f.close()

    return test_num + 1


def makeTecoValidSet(path, f, title, option, test_num):
    onlyFiles = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    if len(onlyFiles) == 0:
        return test_num
    if _checkprohibitedFolder(path) == True:
        return test_num

    divide(path, False)
    split(join(path, 'lt'), 0.3, True)
    split(join(path, 'rt'), 0.3, True)
    split(join(path, 'lb'), 0.3, True)
    is_splited = split(join(path, 'rb'), 0.3, True)

    if is_splited == False:
        test_num = _printTestScript(path, f, title, option, test_num)
    else:
        test_num = _printTestScript(join(path, 'lt'), f, title, option.append('lt'), test_num)
        test_num = _printTestScript(join(path, 'rt'), f, title, option.append('rt'), test_num)
        test_num = _printTestScript(join(path, 'lb'), f, title, option.append('lb'), test_num)
        test_num = _printTestScript(join(path, 'rb'), f, title, option.append('rb'), test_num)

    return test_num


def autoForTecoValidSets(fileName, path, title, option, test_num):
    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]

    if len(onlyFolders) == 0:
        if title == "":
            title = os.path.basename(path)

        divideFor16(path, False)

        f = open(fileName, "at", encoding='utf-8')
        test_num = makeTecoValidSet(join(path, "1st_lt"), f, title, option, test_num)
        test_num = makeTecoValidSet(join(path, "1st_lb"), f, title, option, test_num)
        test_num = makeTecoValidSet(join(path, "1st_rt"), f, title, option, test_num)
        test_num = makeTecoValidSet(join(path, "1st_rb"), f, title, option, test_num)
        f.close()
        return test_num

    else:
        for folderPath in onlyFolders:
            if len(onlyFolders) > 2:
                if _IsThisSubFolder() == False:
                    ## main folder name
                    title = folderPath
                else:
                    if folderPath == 'L' or folderPath == 'l':
                        option.append(folderPath)
                    elif folderPath == 'R' or folderPath == 'r':
                        option.append(folderPath)

            base = os.path.join(path, folderPath)
            test_num = autoForTecoValidSets(fileName, base, title, option, test_num)
        return test_num


def gatherAllDividedFolders(path):
    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]

    for folderPath in onlyFolders:
        if divideFolder_prefix in folderPath:
            # divied folders in here
            gather(path, True)
            break
        else:
            gatherAllDividedFolders(os.path.join(path, folderPath))


def moveAll(src, dst):
    # move src/*.* to dst/*.*
    onlyFolders = [f for f in os.listdir(src) if not os.path.isfile(os.path.join(src, f))]

    for folderPath in onlyFolders:
        shutil.move(join(src, folderPath), join(dst, folderPath))

    onlyFiles = [f for f in os.listdir(src) if os.path.isfile(os.path.join(src, f))]

    for fileName in onlyFiles:
        shutil.move(join(src, fileName), join(dst, fileName))

    os.rmdir(src)


def gatherModelNameFolders(path):
    modelFolderRule = 'TecoGAN_'

    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]

    for folderPath in onlyFolders:
        if modelFolderRule in folderPath:
            # model folders in here
            moveAll(os.path.join(path, folderPath), path)
            break
        else:
            gatherModelNameFolders(os.path.join(path, folderPath))


def mergeDividedImages(path, del_mode):
    mergeTarget = {'lt', 'lb', 'rt', 'rb'}

    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]

    exist_flag = True
    for folderPath in mergeTarget:
        folderPath = os.path.join(path, folderPath)
        exist_flag = exist_flag and os.path.exists(folderPath)

    if exist_flag == False:
        for folderPath in onlyFolders:
            mergeDividedImages(os.path.join(path, folderPath), del_mode=del_mode)

    else:
        merge(path, del_mode=del_mode)


def mergeMergedImages(path):
    mergeTarget = {'1st_lt', '1st_lb', '1st_rt', '1st_rb'}

    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]

    exist_flag = True
    for folderPath in mergeTarget:
        folderPath = os.path.join(path, folderPath)
        exist_flag = exist_flag and os.path.exists(folderPath)

    if exist_flag == False:
        for folderPath in onlyFolders:
            mergeMergedImages(os.path.join(path, folderPath))

    else:
        for folderPath in mergeTarget:
            # makedirs(join(path, folderPath[4:6]), exist_ok=True)
            shutil.move(join(path, folderPath, 'merged'), join(path, folderPath[4:6]))
            # os.rename(os.path.join(path, 'merged'), os.path.join(path, folderPath[4:6]))

        merge(path, False)


def autoForTecoResultMerge(path, del_mode):
    # 1. find split folders and gather
    gatherAllDividedFolders(path)

    # 2. find model name folder and gather
    gatherModelNameFolders(path)

    # 3. find divided folder and merge
    mergeDividedImages(path, del_mode=del_mode)

    # 4. find divided from divieid folder and merge
    mergeMergedImages(path)


####
def _make_TargetFolderMap(path, folderMap, target_prefix, forbidden_words):
    onlyfolders = [f for f in listdir(path) if not isfile(join(path, f))]

    flag = False
    if flag == False:
        if len(onlyfolders) != 0:
            for folderName in onlyfolders:
                # forbidden words check for main folders
                forbidden_flag = False
                for forbidden_word in forbidden_words:
                    if forbidden_word in folderName:
                        forbidden_flag = True
                        break

                if forbidden_flag == False:
                    if target_prefix in folderName:
                        folderMap.append(join(path, folderName))
                    else:
                        _make_TargetFolderMap(join(path, folderName), folderMap, target_prefix, forbidden_words)

    return folderMap


def _mergeProc_for_mp(id, folderMap, start, end, del_mode=False, targetPath='merged'):
    imgCnt = 0

    if start < 0:
        start = 0

    if end > len(folderMap):
        end = len(folderMap)

    print('[Proc id : %d] Start')

    paths = {}
    for i in range(start, end):
        paths['lt'] = folderMap[i]
        pivot_path = paths['lt']

        paths['rt'] = paths['lt'].replace('\\lt\\', '\\rt\\')
        paths['rt'] = paths['rt'].replace('/lt/', '/rt/')

        paths['lb'] = paths['lt'].replace('\\lt\\', '\\lb\\')
        paths['lb'] = paths['lb'].replace('/lt/', '/lb/')

        paths['rb'] = paths['lt'].replace('\\lt\\', '\\rb\\')
        paths['rb'] = paths['rb'].replace('/lt/', '/rb/')

        onlyfiles = [f for f in listdir(pivot_path) if isfile(join(pivot_path, f))]

        if len(onlyfiles) == 0:
            print('there is no file in \'{}\' folder'.format(pivot_path))
            return -1

        if targetPath == '':
            outputPath = join(pivot_path, '..\\..\\..')
        else:
            #### temp for test
            outputPath = join(pivot_path, '..\\..\\..', targetPath, str(id))
            makedirs(outputPath, exist_ok=True)

        print("[Proc id : %d] Divided images from %s (pivot folder)" % (id, pivot_path))
        for n in range(0, len(onlyfiles)):
            # set size from source image
            filepath = join(pivot_path, onlyfiles[n])
            _, ext = splitext(filepath)

            if ext != '.png' and ext != '.tif':
                if del_mode == True:
                    if onlyfiles[n] == 'Thumbs.db':
                        os.remove(filepath)
                continue

            # print("\r[%d/%d] merging...(file name : %s)\r" % (n + 1, len(onlyfiles), onlyfiles[n]), end='')
            print("[Proc id : %d][%d/%d] merging...(file name : %s)" % (id, n + 1, len(onlyfiles), onlyfiles[n]))
            try:
                # left-top
                img_lt = cv2.imread(join(paths['lt'], onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
                if len(img_lt) == 0:
                    print("there is no file : {}".format(join(paths['lt'], onlyfiles[n])))
                    continue
                # right-top
                img_rt = cv2.imread(join(paths['rt'], onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
                if len(img_rt) == 0:
                    print("there is no file : {}".format(join(paths['rt'], onlyfiles[n])))
                    continue
                # left-bottom
                img_lb = cv2.imread(join(paths['lb'], onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
                if len(img_lb) == 0:
                    print("there is no file : {}".format(join(paths['lb'], onlyfiles[n])))
                    continue
                # right-bottom
                img_rb = cv2.imread(join(paths['rb'], onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
                if len(img_rb) == 0:
                    print("there is no file : {}".format(join(paths['rb'], onlyfiles[n])))
                    continue
            except:
                print('some error has occured, file name : %s' % onlyfiles[n])
                continue

            if make_margin == True:
                # TODO : blend margin
                x_margin = x_margin
                y_margin = y_margin

            # merge
            img_upper = cv2.hconcat([img_lt, img_rt])
            img_lower = cv2.hconcat([img_lb, img_rb])
            img = cv2.vconcat([img_upper, img_lower])

            # fileName = join(outputPath, '%06d.png' % imgCnt)
            fileName = join(outputPath, onlyfiles[n])
            cv2.imwrite(fileName, img)

            if del_mode == True:
                del_path = join(path, 'lt', onlyfiles[n])
                if os.path.isfile(del_path) == True:
                    os.remove(del_path)
                del_path = join(path, 'rt', onlyfiles[n])
                if os.path.isfile(del_path) == True:
                    os.remove(del_path)
                del_path = join(path, 'lb', onlyfiles[n])
                if os.path.isfile(del_path) == True:
                    os.remove(del_path)
                del_path = join(path, 'rb', onlyfiles[n])
                if os.path.isfile(del_path) == True:
                    os.remove(del_path)

            imgCnt = imgCnt + 1

        if del_mode == True:
            src_folders = {'lt', 'rt', 'lb', 'rb'}
            for subFolder in src_folders:
                onlyfiles = [f for f in listdir(join(path, subFolder)) if isfile(join(path, subFolder, f))]
                if len(onlyfiles) == 0:
                    os.rmdir(join(path, subFolder))
                else:
                    print('Cannot delete %s folder, there is not empty folder. check inside' % subFolders)


    print('[Proc id : %d] End' % id)


def autoForTecoResultMerge_opt(path, additional_forbidden_words=[], del_mode=False, num_multiProc=1):
    ts = timeStamp.timeStamper
    # ts.start()
    # for make pivot ('lt') folder map
    forbidden_words = ['lb', 'rt', 'rb']

    # 1. make folder map for splited image folders
    folderMap = []
    pivot_folderMap = _make_TargetFolderMap(path, folderMap, divideFolder_prefix, forbidden_words)

    # 2. merge images with multiprocessing
    procs = []
    step = int(len(folderMap) / num_multiProc)
    for idx in range(0, num_multiProc):
        start = idx * step
        end = (idx + 1) * step

        proc = mp.Process(target=_mergeProc_for_mp, args=(idx, pivot_folderMap, start, end, del_mode))
        procs.append(proc)
        proc.start()

    for proc in procs:
        proc.join()

    # print('record : %.3f' %ts.lap())

resizefolderPath = 'resized'
# resizefolderPath = ''
def resizeAll(path, ratio, overwrite=False, w_target=0):
    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]

    if len(onlyFolders) != 0:
        for folderPath in onlyFolders:
            newPath = join(path, folderPath)

            resizeAll(newPath, ratio, overwrite, w_target=w_target)
    else:
        onlyFiles = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
        if len(onlyFiles) != 0:
            print("Folder path : %s" % path)

            if overwrite == True:
                dstFolderPath = path
            else:
                resizefolderPath = _getCurDirName(path)
                dstFolderPath = join(path, resizefolderPath)
                makedirs(dstFolderPath, exist_ok=True)

            for i in range(0, len(onlyFiles)):
                filename = onlyFiles[i]
                _, ext = os.path.splitext(filename)

                if ext == '.png' or ext == '.jpg' or ext == '.tif' or ext == '.tiff':
                    srcPath = join(path, filename)
                    dstPath = join(dstFolderPath, filename)

                    try:
                        src = cv2.imread(srcPath, flags=cv2.IMREAD_UNCHANGED)
                        h, w, _ = src.shape
                    except:
                        print('some error has occured, file path : %s' %srcPath)
                        continue
                    if w_target!=0:
                        if w != w_target:
                            continue
                    if ratio != 1:
                        targetH = int(h * ratio)
                        targetW = int(w * ratio)

                    if targetW != 0 and targetH != 0:
                        if ratio > 1:
                            dst = cv2.resize(src, dsize=(int(targetW), int(targetH)), interpolation=cv2.INTER_CUBIC)
                        else:
                            dst = cv2.resize(src, dsize=(int(targetW), int(targetH)), interpolation=cv2.INTER_AREA)
                    else:
                        dst = src

                    cv2.imwrite(dstPath, dst)

                    print("\rDownsizing ... [% 6d / % 6d]" % (i + 1, len(onlyFiles)), end='')
                else:
                    print("It's not a image file. filename : %s" % filename)

            print("\n")


# def blurFilter(src):
def blurFilter(path):
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

    if len(onlyfiles) != 0:
        param1 = 3
        param2 = param1
        param3 = 0
        folder1 = join(path, "..", 'gaussian_%d-%d-%d' % (param1, param2, param3))
        makedirs(folder1, exist_ok=True)

        param4 = 5
        param5 = param4
        param6 = 0
        folder2 = join(path, "..", 'gaussian_%d-%d-%d' % (param4, param5, param6))
        makedirs(folder2, exist_ok=True)

        param7 = 5
        param8 = 25
        param9 = param8
        folder3 = join(path, "..", 'biliteral_%d-%d-%d.png' % (param7, param8, param9))
        makedirs(folder3, exist_ok=True)

        for fileName in onlyfiles:
            src = cv2.imread(join(path, fileName), flags=cv2.IMREAD_UNCHANGED)

            dst = cv2.GaussianBlur(src, (param1, param2), param3)
            cv2.imwrite(join(folder1, fileName), dst)

            dst = cv2.GaussianBlur(src, (param4, param5), param6)
            cv2.imwrite(join(folder2, fileName), dst)

            dst = cv2.bilateralFilter(src, param7, param8, param9)
            cv2.imwrite(join(folder3, fileName), dst)


def whatTypeIsThisPath(path):
    imageFileExts = {".png", ".jpg", ".tif", ".tiff"}
    videoFileExts = {".mov", ".mp4", ".avi", ".mxf"}

    endNode = os.path.splitext(path)[-1]

    kind = ''
    if not '.' in endNode:
        kind = 'folder'

    for ext in imageFileExts:
        if ext in endNode.lower():
            kind = 'image'
            break

    for ext in imageFileExts:
        if ext in endNode.lower():
            kind = 'video'
            break



    return kind


def _IsThisImageFile(ext):
    imageFileExts = {".png", ".jpg", ".tif", ".tiff", ".TIF", ".TIFF"}

    flag = False
    for imageExt in imageFileExts:
        if ext == imageExt:
            flag = True

    return flag


def _make_imageFolderMap(path, folderMap, additional_forbidden_words=[]):
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    onlyfolders = [f for f in listdir(path) if not isfile(join(path, f))]

    forbidden_words = []

    for forbidden_key in additional_forbidden_words:
        forbidden_words.append(forbidden_key)

    flag = False
    if len(onlyfiles) != 0:
        flag = True
        for filename in onlyfiles:
            if filename == 'Thumbs.db':
                continue

            _, ext = os.path.splitext(filename)
            flag = flag and _IsThisImageFile(ext)

        if flag == True:
            # image folder has only image files
            folderName = _getCurDirName(path)

            # forbidden words check for image folder
            forbidden_flag = False
            for forbidden_word in forbidden_words:
                if forbidden_word in folderName:
                    forbidden_flag = True
                    break
            if forbidden_flag == False:
                print('[%03d] add tartget folder : %s' %(len(folderMap) + 1, path))
                folderMap.append(path)

    if flag == False:
        if len(onlyfolders) != 0:
            for folderPath in onlyfolders:
                # forbidden words check for main folders
                folderName = folderPath
                forbidden_flag = False
                for forbidden_word in forbidden_words:
                    if forbidden_word in folderName:
                        forbidden_flag = True
                        break
                if forbidden_flag == False:
                    _make_imageFolderMap(join(path, folderPath), folderMap, forbidden_key)

    return folderMap


def _getSampleImage(path):
    onlyfolders = [f for f in listdir(path) if not isfile(join(path, f))]

    if len(onlyfolders) != 0:
        for foldername in onlyfolders:
            img = _getSampleImage(join(path, foldername))

            if len(img) != 0:
                return img

    else:
        onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

        if len(onlyfiles) != 0:
            flag = False
            for filename in onlyfiles:
                _, ext = os.path.splitext(filename)
                flag = _IsThisImageFile(ext)
                if flag == True:
                    break

            if flag == True:
                return cv2.imread(join(path, filename), flags=cv2.IMREAD_UNCHANGED)

        return []


def _autoForTecoValidSets(path, blur=False, list_path='', splitFirst=False, splitLimit=0.2, forbidden_key='', divideMode=4, complete_marking=True):
    test_num = 0
    print('====================================================')
    print('Run auto-generation scrip for TecoGan Validation set')
    print('Target path : ', path)
    print('Divide mode : ', divideMode)
    print('Split size : %.3f GB', splitLimit)
    print('====================================================')
    print('')

    forbidden_words = ['Bak', 'lt', 'lb', 'rt', 'rb']
    if forbidden_key != '':
        forbidden_words.append(forbidden_key)

    # 1. make map
    folderMap = []
    folderMap = _make_imageFolderMap(path, folderMap, forbidden_words)

    # 2. check image size
    # TODO : size check
    divideMode = 4

    # 3. make validation set
    if list_path == '':
        # make list file to default path
        list_path = join(path, 'list.txt')

    ## check image size for dividing
    if len(folderMap) != 0:
        img = _getSampleImage(folderMap[0])

        if len(img) != 0:
            height, width, _ = img.shape
            bit = img.dtype
        else:
            print('there is no image\npath : %s' % folderMap[0])
            return -1

    if divideMode == 16:
        # 1/16 divide
        for folderPath in folderMap:
            divideFor16(folderPath, makeBlurForMpeg=blur)

            for subFolder16 in subFolders16:
                divide(join(folderPath, subFolder16))
                is_split = False
                for subFolder in subFolders:
                    is_split = split(join(folderPath, subFolder16, subFolder), splitLimit, True)
                    if is_split == True:
                        test_num = _printTestScript_test(join(folderPath, subFolder16, subFolder), list_path,
                                                         _getCurDirName(folderPath), test_num, {subFolder16, subFolder})

                if is_split == False:
                    test_num = _printTestScript_test(join(folderPath, subFolder16), list_path,
                                                     _getCurDirName(folderPath), test_num, subFolders16)
    elif divideMode == 4:
        # 1/4 divide
        for folderPath in folderMap:
            backup_postfix = '_orgBak'
            try:
                divide(folderPath, backup_postfix=backup_postfix)
            except:
                ##TODO roll-back
                # backup_postfix
                print('cannot complete divide job')

            is_split = False
            for subFolder in subFolders:
                is_split = split(join(folderPath, subFolder), splitLimit, True)
                if is_split == True:
                    test_num = _printTestScript_test(join(folderPath, subFolder), list_path, _getCurDirName(folderPath),
                                                     test_num, {subFolder})

            if is_split == False:
                test_num = _printTestScript_test(join(folderPath, subFolder), list_path, _getCurDirName(folderPath),
                                                 test_num)

            if complete_marking == True:
                # makedirs(path, exist_ok=True)
                os.rename(folderPath, folderPath + '_done')
    else:
        # no divide
        for folderPath in folderMap:
            print("TODO")


def _reMergeAll(path):
    onlyfolders = [f for f in listdir(path) if not isfile(join(path, f))]

    flag = False
    for folderPath in onlyfolders:
        if '1st' in folderPath:
            flag = True

    if flag == True:
        folderPath = '1st_lt'
        merge(join(path, folderPath), targetPath="")
        os.rename(join(path, folderPath), join(path, folderPath[4:6]))

        folderPath = '1st_lb'
        merge(join(path, folderPath), targetPath="")
        os.rename(join(path, folderPath), join(path, folderPath[4:6]))

        folderPath = '1st_rt'
        merge(join(path, folderPath), targetPath="")
        os.rename(join(path, folderPath), join(path, folderPath[4:6]))

        folderPath = '1st_rb'
        merge(join(path, folderPath), targetPath="")
        os.rename(join(path, folderPath), join(path, folderPath[4:6]))

        merge(path, targetPath=_getCurDirName(path))

    else:
        for folderPath in onlyfolders:
            _reMergeAll(join(path, folderPath))


def _resizeAll(path, targetPath='', targetW=0, targetH=0, ratio=1, mask=False, overwrite=False):
    onlyfolders = [f for f in listdir(path) if not isfile(join(path, f))]

    # if len(onlyfolders) == 0:
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

    if overwrite == True:
        targetPath = path
    else:
        if targetPath == '':
            targetPath = join(path, 'resized')
        makedirs(targetPath, exist_ok=True)

    for fileName in onlyfiles:
        # img = cv2.imread(join(path, fileName), flags=cv2.IMREAD_UNCHANGED)
        img = cv2.imread(join(path, fileName))
        try:
            h, w, _ = img.shape
        except:
            # print('It is not Image file (maybe?)')
            return -1
        # resized = cv2.resize(img, dsize=(int(1920), int(1024)))

        if ratio != 1:
            targetH = int(h * ratio)
            targetW = int(w * ratio)

        if targetW != 0 and targetH != 0:
            if ratio > 1:
                resized = cv2.resize(img, dsize=(int(targetW), int(targetH)), interpolation=cv2.INTER_CUBIC)
            else:
                resized = cv2.resize(img, dsize=(int(targetW), int(targetH)), interpolation=cv2.INTER_AREA)
        else:
            resized = img

        if mask == True:
            y = 0
            for line in resized:
                x = 0
                for pixel in line:
                    b, g, r = pixel

                    if g == 0 and b == 0 and r == 0:
                        # pixel for inpainting, set mask color
                        resized[y][x][0] = 0
                        resized[y][x][1] = 0
                        resized[y][x][2] = 0

                    else:
                        resized[y][x][0] = 255
                        resized[y][x][1] = 255
                        resized[y][x][2] = 255

                    x = x + 1
                y = y + 1

        name, ext = os.path.splitext(fileName)

        cv2.imwrite(join(targetPath, name + '.png'), resized)

    # else:
    #     for folderName in onlyfolders:
    #         _resizeAll(join(path, folderName))


def sampling(path, targetPath, unit):
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

    if targetPath == '':
        targetPath = join(path, 'sampled')
    makedirs(targetPath, exist_ok=True)

    cnt = unit - 1
    for fileName in onlyfiles:
        if cnt >= unit - 1:
            shutil.copy(join(path, fileName), join(targetPath, fileName))
            cnt = 0
        else:
            cnt += 1


def maskMerge(src_path, dst_path):
    path = src_path
    onlyfolders = [f for f in listdir(path) if not isfile(join(path, f))]

    pivots = []
    folderNames = []
    fileLists = []

    for foldername in onlyfolders:
        path = join(src_path, foldername)
        onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

        pivots.append(0)
        folderNames.append(path)
        fileLists.append(onlyfiles)

    flag = True
    while (flag == True):
        imgNumber = []
        for i in range(0, len(pivots)):
            pivot = pivots[i]
            imgNumber.append(fileLists[i][pivot] - '.png')

from os import listdir, makedirs
from os.path import isfile, join, splitext
import os
import shutil
import sys
## need to install
import cv2
import numpy

# import timeStamp
import multiprocessing as mp

divideFolder_prefix = 'dividedSource_'

def _getCurDirName(path):
    return (os.path.split(os.path.abspath(path)))[1]


make_margin = False
x_margin = 50
y_margin = 50


def imageSub(src, sub, x, y, w=0, h=0):
    return dst


def makeEdge(src):
    return dst

def divide(path, debug_mode=False, backup_postfix='_orgBak'):
    make_orgBackup = True

    imgCnt = 0
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

    if len(onlyfiles) == 0:
        print('there is no file in \'{}\' folder'.format(path))
        return -1

    makedirs(path, exist_ok=True)
    makedirs(join(path, 'lt'), exist_ok=True)
    makedirs(join(path, 'rt'), exist_ok=True)
    makedirs(join(path, 'lb'), exist_ok=True)
    makedirs(join(path, 'rb'), exist_ok=True)

    if make_orgBackup == True:
        makedirs(join(path, '..', _getCurDirName(path) + backup_postfix), exist_ok=True)

    dot = 0
    print("\nfolder path : " + path)
    print("")
    for n in range(0, len(onlyfiles)):
        print("\rdivide images (%.1f/100%%)" %(n/len(onlyfiles)*100), end="")
        for i in range(0, dot):
            print(".", end="")
        for i in range(0, 5 - dot):
            print(" ", end="")
        if dot == 5:
            dot = 0
        else:
            dot = dot + 1
        # set size from source image
        filepath = join(path, onlyfiles[n])
        onlyname, ext = splitext(onlyfiles[n])

        if ext != '.png' and ext != '.tif':
            continue

        img = cv2.imread(filepath, flags=cv2.IMREAD_UNCHANGED)
        height, width, channels = img.shape

        crop_width = int(width * 0.5)
        crop_height = int(height * 0.5)

        # left-top
        img_lt = img[0:crop_height, 0:crop_width]
        # right-top
        img_rt = img[0:crop_height, crop_width:width]
        # left-bottom
        img_lb = img[crop_height:height, 0:crop_width]
        # right-bottom
        img_rb = img[crop_height:height, crop_width:width]

        if debug_mode == True:
            cv2.imshow('left-top', img_lt)
            cv2.imshow('right-top', img_rt)
            cv2.imshow('left-bottom', img_lb)
            cv2.imshow('right-bottom', img_rb)

        if make_margin == True:
            # TODO
            x_margin = x_margin
            y_margin = y_margin

        fileName = join(path, 'lt', (onlyname + '.png'))
        cv2.imwrite(fileName, img_lt)
        fileName = join(path, 'rt', onlyname + '.png')
        cv2.imwrite(fileName, img_rt)
        fileName = join(path, 'lb', onlyname + '.png')
        cv2.imwrite(fileName, img_lb)
        fileName = join(path, 'rb', onlyname + '.png')
        cv2.imwrite(fileName, img_rb)

        if make_orgBackup == True:
            shutil.move(filepath, join(path, '..', _getCurDirName(path) + '_orgBak', onlyfiles[n]))

        if debug_mode == True:
            cv2.waitKey(1000 / fps)

        imgCnt = imgCnt + 1

    if debug_mode == True:
        cv2.destroyAllWindows()


subFolders = {'lt', 'rt', 'lb', 'rb'}
subFolders16 = {'1st_lt', '1st_rt', '1st_lb', '1st_rb'}
param1 = 3  # kernal width size for gaussian blur
param2 = param1  # kernal height size for gaussian blur
param3 = 0  # sigma X for gaussian blur


def divideFor16(path, debug_mode=False, makeBlurForMpeg=False):
    makeOrgFolder = True

    imgCnt = 0
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

    if len(onlyfiles) == 0:
        print('there is no file in \'{}\' folder'.format(path))
        return -1

    makedirs(path, exist_ok=True)
    for folderPath in subFolders16:
        makedirs(join(path, folderPath), exist_ok=True)

    if makeOrgFolder == True:
        makedirs(join(path, _getCurDirName(path)), exist_ok=True)

    dot = 0
    print("\nfolder path : " + path)
    for n in range(0, len(onlyfiles)):
        print("\rdivide images (%.1f/100%%)" %(n/len(onlyfiles)*100), end="")
        for i in range(0, dot):
            print(".", end="")
        for i in range(0, 5 - dot):
            print(" ", end="")
        if dot == 5:
            dot = 0
        else:
            dot = dot + 1
        # set size from source image
        filepath = join(path, onlyfiles[n])
        onlyname, ext = splitext(onlyfiles[n])

        if ext != '.png' and ext != '.tif':
            continue

        img = cv2.imread(filepath, flags=cv2.IMREAD_UNCHANGED)
        height, width, channels = img.shape

        if makeBlurForMpeg == True:
            img = cv2.GaussianBlur(img, (param1, param2), param3)

        crop_width = int(width * 0.5)
        crop_height = int(height * 0.5)

        # left-top
        img_lt = img[0:crop_height, 0:crop_width]
        # right-top
        img_rt = img[0:crop_height, crop_width:width]
        # left-bottom
        img_lb = img[crop_height:height, 0:crop_width]
        # right-bottom
        img_rb = img[crop_height:height, crop_width:width]

        if debug_mode == True:
            cv2.imshow('left-top', img_lt)
            cv2.imshow('right-top', img_rt)
            cv2.imshow('left-bottom', img_lb)
            cv2.imshow('right-bottom', img_rb)

        if make_margin == True:
            # TODO
            x_margin = x_margin
            y_margin = y_margin

        fileName = join(path, '1st_lt', (onlyname + '.png'))
        cv2.imwrite(fileName, img_lt)
        fileName = join(path, '1st_rt', onlyname + '.png')
        cv2.imwrite(fileName, img_rt)
        fileName = join(path, '1st_lb', onlyname + '.png')
        cv2.imwrite(fileName, img_lb)
        fileName = join(path, '1st_rb', onlyname + '.png')
        cv2.imwrite(fileName, img_rb)

        if makeOrgFolder == True:
            shutil.move(filepath, join(path, _getCurDirName(path), onlyfiles[n]))

        if debug_mode == True:
            cv2.waitKey(1000 / fps)

        imgCnt = imgCnt + 1

    if debug_mode == True:
        cv2.destroyAllWindows()


def merge(path, debug_mode=False, targetPath='merged', del_mode=False):
    imgCnt = 0
    onlyfiles = [f for f in listdir(join(path, 'lt')) if isfile(join(path, 'lt', f))]

    if len(onlyfiles) == 0:
        print('there is no file in \'{}\' folder'.format(path))
        return -1

    if targetPath == '':
        outputPath = path
    else:
        outputPath = join(path, targetPath)
        makedirs(outputPath, exist_ok=True)

    print("Divided images from %s" % path)
    for n in range(0, len(onlyfiles)):
        # set size from source image
        filepath = join(path, onlyfiles[n])
        _, ext = splitext(filepath)

        if ext != '.png' and ext != '.tif':
            if del_mode == True:
                if onlyfiles[n] == 'Thumbs.db':
                    os.remove(filepath)
            continue

        print("\r[%d/%d] merging...(file name : %s)\r" % (n + 1, len(onlyfiles), onlyfiles[n]), end='')
        try:
            # left-top
            img_lt = cv2.imread(join(path, 'lt', onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
            if len(img_lt) == 0:
                print("there is no file : {}".format(join(path, 'lt', onlyfiles[n])))
                continue
            # right-top
            img_rt = cv2.imread(join(path, 'rt', onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
            if len(img_rt) == 0:
                print("there is no file : {}".format(join(path, 'rt', onlyfiles[n])))
                continue
            # left-bottom
            img_lb = cv2.imread(join(path, 'lb', onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
            if len(img_lb) == 0:
                print("there is no file : {}".format(join(path, 'lb', onlyfiles[n])))
                continue
            # right-bottom
            img_rb = cv2.imread(join(path, 'rb', onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
            if len(img_rb) == 0:
                print("there is no file : {}".format(join(path, 'rb', onlyfiles[n])))
                continue
        except:
            print('some error has occured, file name : %s' %onlyfiles[n])
            continue
        if debug_mode == True:
            cv2.imshow('left-top', img_lt)
            cv2.imshow('right-top', img_rt)
            cv2.imshow('left-bottom', img_lb)
            cv2.imshow('right-bottom', img_rb)

        if make_margin == True:
            # TODO : blend margin
            x_margin = x_margin
            y_margin = y_margin

        # merge
        img_upper = cv2.hconcat([img_lt, img_rt])
        img_lower = cv2.hconcat([img_lb, img_rb])
        img = cv2.vconcat([img_upper, img_lower])

        # fileName = join(outputPath, '%06d.png' % imgCnt)
        fileName = join(outputPath, onlyfiles[n])
        cv2.imwrite(fileName, img)

        if debug_mode == True:
            cv2.imshow('merged', img)
            cv2.waitKey(1000 / fps)

        if del_mode == True:
            del_path = join(path, 'lt', onlyfiles[n])
            if os.path.isfile(del_path) == True:
                os.remove(del_path)
            del_path = join(path, 'rt', onlyfiles[n])
            if os.path.isfile(del_path) == True:
                os.remove(del_path)
            del_path = join(path, 'lb', onlyfiles[n])
            if os.path.isfile(del_path) == True:
                os.remove(del_path)
            del_path = join(path, 'rb', onlyfiles[n])
            if os.path.isfile(del_path) == True:
                os.remove(del_path)

        imgCnt = imgCnt + 1

    if del_mode == True:
        src_folders = {'lt', 'rt', 'lb', 'rb'}
        for subFolder in src_folders:
            onlyfiles = [f for f in listdir(join(path, subFolder)) if isfile(join(path, subFolder, f))]
            if len(onlyfiles) == 0:
                os.rmdir(join(path, subFolder))
            else:
                print('Cannot delete %s folder, there is not empty folder. check inside' %subFolders)

    if debug_mode is True:
        cv2.destroyAllWindows()

    print("Merge complete")


def split(path, limit, is_unzipsize=True):
    start_num = 0

    # limit = 1.0  # giga-byte
    limit = limit * 1024 * 1024 * 1024  # byte

    onlyFiles = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

    accumulatedByte = 0
    nFolders = 0
    holdPath = ""
    holdFileName = ""
    holdByte = 0

    for n in range(0, len(onlyFiles)):
        if accumulatedByte == 0:
            # make new folder
            targetFolderPath = os.path.join(path, divideFolder_prefix + '%03d' % (start_num + nFolders))
            while (os.path.exists(targetFolderPath)):
                print("{} is already exist!!".format(targetFolderPath))
                nFolders = nFolders + 1
                targetFolderPath = os.path.join(path, divideFolder_prefix + '%03d' % (start_num + nFolders))
            os.makedirs(targetFolderPath, exist_ok=True)
            nFolders = nFolders + 1

            if holdPath != "":
                shutil.move(holdPath, os.path.join(targetFolderPath, holdFileName))
                print("\rcopy {} to {}".format(filePath, (os.path.join(targetFolderPath, holdFileName))), end='')

                accumulatedByte = holdByte

        filePath = os.path.join(path, onlyFiles[n])

        _, ext = os.path.splitext(filePath)
        if ext == '.py':
            continue
        if is_unzipsize == True:
            img = cv2.imread(filePath, flags=cv2.IMREAD_UNCHANGED)
            byte = sys.getsizeof(img)
        else:
            byte = os.path.getsize(filePath)

        if byte + accumulatedByte > limit:
            if byte > limit:
                print("{} is too big to copy for the current limitation (limit : {} GB, file size : {:.3f} GB)".format(
                    onlyFiles[n], limit / (1024.0 * 1024.0 * 1024.0), byte / (1024.0 * 1024.0 * 1024.0)))

            holdPath = filePath
            holdFileName = onlyFiles[n]
            holdByte = byte

            print("{} : {:.3f} GB".format(targetFolderPath, accumulatedByte / (1024.0 * 1024.0 * 1024.0)))

            accumulatedByte = 0
        else:
            shutil.move(filePath, os.path.join(targetFolderPath, onlyFiles[n]))
            print("\rcopy {} to {}".format(filePath, (os.path.join(targetFolderPath, onlyFiles[n]))), end='')

            accumulatedByte = accumulatedByte + byte

    if (start_num + nFolders) == 1:
        # if there is the only folder, don't make divided folder
        gather(path, True)
        is_divided = False
    else:
        is_divided = True

    return is_divided


def gather(path, delfolder):
    start_num = 0

    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]
    for n in range(0, len(onlyFolders)):
        folderPath = os.path.join(path, onlyFolders[n])
        onlyFiles = [f for f in os.listdir(folderPath) if os.path.isfile(os.path.join(folderPath, f))]

        for m in range(0, len(onlyFiles)):
            filePath = os.path.join(folderPath, onlyFiles[m])
            _, ext = os.path.splitext(filePath)
            if ext == '.py':
                continue

            shutil.move(filePath, os.path.join(path, onlyFiles[m]))
            print("\rcopy {} to {}".format(filePath, (os.path.join(path, onlyFiles[m]))), end='')

        if delfolder == True:
            os.rmdir(folderPath)

# def copy_folder(src_path, dst_path, delfolder, id=-1):


def gather_mp(path, delfolder):
    num_multiprocessor = 10

    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]
    num_folders = len(onlyFolders)
    if num_multiprocessor > num_folders:
        num_multiprocessor = num_folders
        step = 1
    else:
        step = num_folders / num_multiprocessor

    def gather_singleFolder(start, end, path, delfolder):
        for n in range(start, end):
            folderPath = os.path.join(path, onlyFolders[n])
            onlyFiles = [f for f in os.listdir(folderPath) if os.path.isfile(os.path.join(folderPath, f))]

            for m in range(0, len(onlyFiles)):
                filePath = os.path.join(folderPath, onlyFiles[m])
                _, ext = os.path.splitext(filePath)
                if ext == '.py':
                    continue

                shutil.move(filePath, os.path.join(path, onlyFiles[m]))
                print("\rcopy {} to {}".format(filePath, (os.path.join(path, onlyFiles[m]))), end='')

            if delfolder == True:
                os.rmdir(folderPath)

    # make arguments
    # for p in range(0, num_multiprocessor):


    # p = mp.Process(target=gather_singleFolder, args=())


def rename(path, start_num, prefix_format, is_folder):
    path = os.path.join(path, 'renamed')
    makedirs(path, exist_ok=True)

    if is_folder == True:
        onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]

        if len(onlyFolders) == 0:
            print('there is no folder in \'{}\' folder'.format(path))
            return -1

        for n in range(0, len(onlyFolders)):
            os.rename(os.path.join(path, onlyFolders[n]), os.path.join(path, prefix_format + '%04d' % (start_num + n)))

    else:  # file case
        onlyFiles = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

        if len(onlyFiles) == 0:
            print('there is no file in \'{}\' folder'.format(path))
            return -1

        for n in range(0, len(onlyFiles)):
            _, ext = os.path.splitext(onlyFiles[n])

            if ext == '.py':
                continue

            os.rename(os.path.join(path, onlyFiles[n]),
                      os.path.join(path, prefix_format + '%06d' % (start_num + n) + ext))


def makeVideoWithCutWithCut(path, videoPrefix, fps, resize_factor, scene_cutting, make_margin, margin_w=0, margin_h=0,
                            debug_mode=False):
    min_frame_per_cut = int(fps / 2)
    outputPath = './output/'
    makedirs(outputPath, exist_ok=True)

    print("image path = " + path)

    print_filename = True

    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    images = numpy.empty(len(onlyfiles), dtype=object)
    old_hist_1ch = 0
    old_hist_sobel = 0

    videoCnt = 0
    frame = 0
    cutting_proof = 0
    prevImg = 0

    numCuda = cv2.cuda.getCudaEnabledDeviceCount()
    if numCuda != 0:
        cv2.cuda.setDevice(0)

    dot = 0
    for n in range(0, len(onlyfiles)):
        print("\rmake video", end="")
        for i in range(0, dot):
            print(".", end="")
        for i in range(0, 5 - dot):
            print(" ", end="")
        if dot == 5:
            dot = 0
        else:
            dot = dot + 1

        # set size from source image
        img = cv2.imread(join(path, onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
        height, width, channels = img.shape

        if resize_factor != 1:
            resized_width = int(width * resize_factor)
            resized_height = int(height * resize_factor)
            resizedImg = cv2.resize(img, (resized_width, resized_height))
        else:
            ## use original size
            resized_width = width
            resized_height = height
            resizedImg = img

        if debug_mode == True:
            dispImg = cv2.resize(img, (int(resized_width / 2), int(resized_height / 2)))

        if make_margin == True:
            resizedImg = cv2.copyMakeBorder(resizedImg, margin_y, margin_y, margin_x, margin_x, cv2.BORDER_CONSTANT)
            resized_width = resized_width + (margin_x * 2)
            resized_height = resized_height + (margin_y * 2)

        # video initialize
        if videoCnt == 0:
            if high_qual == True:
                fourcc = cv2.VideoWriter_fourcc(*'YV12')
            else:
                fourcc = cv2.VideoWriter_fourcc(*'DIVX')
            if scene_cutting == True:
                videoName = videoPrefix + '_%05d.avi' % videoCnt
            else:
                videoName = videoPrefix + '.avi'
            out = cv2.VideoWriter(join(outputPath, videoName), fourcc, fps, (resized_width, resized_height))
            videoCnt = videoCnt + 1

            if scene_cutting == True:
                print("make new cut, cut num : %d" % videoCnt)

        if scene_cutting == True:
            # 1ch histogram
            hist = cv2.calcHist([resizedImg], [0], None, [64], [0, 256])
            cv2.normalize(hist, hist, 0, 100, cv2.NORM_MINMAX)

            sum = 0
            if old_hist_1ch != 0:
                for i in range(0, len(hist)):
                    sum = sum + abs(hist[i] - old_hist_1ch[i])

            old_hist_1ch = hist

            # 3ch histogram

            # not yet

            # edge histogram
            gray = cv2.cvtColor(resizedImg, cv2.COLOR_BGR2GRAY)
            gray = cv2.GaussianBlur(gray, (3, 3), 0)
            sobelX = cv2.Sobel(gray, -1, 1, 0)
            sobelY = cv2.Sobel(gray, -1, 0, 1)

            sobel = abs(sobelX) + abs(sobelY)

            hist_sobel = cv2.calcHist([sobel], [0], None, [64], [0, 256])
            cv2.normalize(hist_sobel, hist_sobel, 0, 100, cv2.NORM_MINMAX)

            sum_sobel = 0
            if old_hist_sobel != 0:
                for i in range(0, len(hist_sobel)):
                    sum_sobel = sum_sobel + abs(hist_sobel[i] - old_hist_sobel[i])

            old_hist_sobel = hist_sobel

            if debug_mode == True:
                # cv2.imshow("sobel edge", sobel)
                # cv2.imshow('resized image ', resizedImg)
                cv2.putText(prevImg, 'Previous Image', (15, 30), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0))
                cv2.imshow('Previous Image', prevImg)
                prevImg = dispImg.copy()
                cv2.putText(dispImg, 'Current Image, #' + str(videoCnt - 1), (15, 30), cv2.FONT_HERSHEY_PLAIN, 1,
                            (0, 255, 0))
                cv2.putText(dispImg, "1ch histogram difference : %d" % sum, (15, 60), cv2.FONT_HERSHEY_PLAIN, 1,
                            (0, 255, 0))
                cv2.putText(dispImg, "edge histogram difference : %d" % sum_sobel, (15, 90), cv2.FONT_HERSHEY_PLAIN, 1,
                            (0, 255, 0))

                print("1ch histogram difference : %d" % sum)
                print("edge histogram difference : %d" % sum_sobel)

            if cutting_proof == 0:
                if int(sum) > 500:
                    # Manual selection off
                    # ch = cv2.waitKey(0)
                    # if ch is ord("y"):
                    # make new cut
                    fourcc = cv2.VideoWriter_fourcc(*'DIVX')
                    videoName = cutVideoPrefix + '%05d.avi' % videoCnt
                    out = cv2.VideoWriter(join(outputPath, videoName), fourcc, fps, (resized_width, resized_height))
                    videoCnt = videoCnt + 1
                    cutting_proof = min_frame_per_cut
                    out.write(resizedImg)
                    print("make new cut, cut num : %d" % videoCnt)
                    # else:
                    # out.write(resizedImg)
                    # print("no")

                    if debug_mode == True:
                        cv2.putText(dispImg, "/////SSAK-DDUK//////", (15, 120), cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 255))
                        cv2.imshow('Current Image', dispImg)
                        cv2.waitKey(500)
                else:
                    if debug_mode == True:
                        cv2.imshow('Current Image', dispImg)
                        cv2.waitKey(int(1000 / fps))
                    out.write(resizedImg)
            else:
                cutting_proof = cutting_proof - 1
                if debug_mode == True:
                    cv2.imshow('Current Image', dispImg)
                    cv2.waitKey(100)
                out.write(resizedImg)
            frame = frame + 1
        else:  # scene_cutting == False
            if print_filename == True:
                cv2.putText(resizedImg, onlyfiles[n], (15, 30), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255))
            out.write(resizedImg)

    print("\ndone")

    out.release()

    if debug_mode == True:
        cv2.destroyAllWindows()

def makeVideo(path, videoName, fps, resize_factor=1, high_qual=False, output='../../output'):
    outputPath = join(path, output)
    makedirs(outputPath, exist_ok=True)

    print("image path = " + path)

    print_filename = True

    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

    videoCnt = 0
    frame = 0
    prevImg = 0

    numCuda = cv2.cuda.getCudaEnabledDeviceCount()
    if numCuda != 0:
        cv2.cuda.setDevice(0)

    dot = 0
    for n in range(0, len(onlyfiles)):
        try:
            print("\rmake video", end="")
            for i in range(0, dot):
                print(".", end="")
            for i in range(0, 5 - dot):
                print(" ", end="")
            if dot == 5:
                dot = 0
            else:
                dot = dot + 1

            # set size from source image
            # img = cv2.imread(join(path, onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
            _, ext = splitext(onlyfiles[n])
            if _IsThisImageFile(ext) == False:
                continue

            img = cv2.imread(join(path, onlyfiles[n]))
            height, width, channels = img.shape

            if resize_factor != 1:
                resized_width = int(width * resize_factor)
                resized_height = int(height * resize_factor)
                resizedImg = cv2.resize(img, (resized_width, resized_height), interpolation=None)
            else:
                ## use original size
                resized_width = width
                resized_height = height
                resizedImg = img

            if high_qual == True:
                resizedImg = cv2.cvtColor(resizedImg, cv2.COLOR_BGR2RGB)

            # video initialize
            if videoCnt == 0:
                if high_qual == True:
                    fourcc = cv2.VideoWriter_fourcc(*'YV12')
                    videoName = videoName + '.avi'
                else:
                    fourcc = cv2.VideoWriter_fourcc(*'DIVX')
                    videoName = videoName + '.avi'
                out = cv2.VideoWriter(join(outputPath, videoName), fourcc, fps, (resized_width, resized_height))
                videoCnt = videoCnt + 1
            cv2.putText(resizedImg, onlyfiles[n], (15, 30), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255))
            out.write(resizedImg)
        except:
            print('error, filename : %s' %onlyfiles[n])

    print("\ndone")

    out.release()

    min_frame_per_cut = int(fps / 2)
    outputPath = join(pathB, '..', 'output')
    makedirs(outputPath, exist_ok=True)

    if concat_h == True:
        print("image path A (Left) = " + pathA)
        print("image path B (Right) = " + pathB)
    else:
        print("image path A (Top) = " + pathA)
        print("image path B (Bottom) = " + pathB)

    onlyFilesA = [f for f in listdir(pathA) if isfile(join(pathA, f))]
    onlyFilesB = [f for f in listdir(pathB) if isfile(join(pathB, f))]

    videoCnt = 0

    numCuda = cv2.cuda.getCudaEnabledDeviceCount()
    if numCuda != 0:
        cv2.cuda.setDevice(0)

    dot = 0
    for n in range(0, len(onlyFilesA)):
        print("\rmake video", end="")
        for i in range(0, dot):
            print(".", end="")
        for i in range(0, 5 - dot):
            print(" ", end="")
        if dot == 5:
            dot = 0
        else:
            dot = dot + 1

        # set size from source image
        # imgA = cv2.imread(join(pathA, onlyFilesA[n]), flags=cv2.IMREAD_UNCHANGED)
        # imgB = cv2.imread(join(pathB, onlyFilesB[n]), flags=cv2.IMREAD_UNCHANGED)
        try:
            imgA = cv2.imread(join(pathA, onlyFilesA[n]))
            imgB = cv2.imread(join(pathB, onlyFilesB[n]))
            height, width, channels = imgB.shape
        except:
            out.release()

        if resize_factor != 1:
            resized_width = int(width * resize_factor)
            resized_height = int(height * resize_factor)
            resizedImgA = cv2.resize(imgA, (resized_width, resized_height))
            resizedImgB = cv2.resize(imgB, (resized_width, resized_height))
        else:
            ## use original size
            resized_width = width
            resized_height = height

            heightA, widthA, channels = imgA.shape

            if heightA != height or widthA != width:
                resizedImgA = cv2.resize(imgA, (resized_width, resized_height), interpolation=None)
            else:
                resizedImgA = imgA

            resizedImgB = imgB

        # if debug_mode == True:
        #     dispImg = cv2.resize(img, (int(resized_width / 2), int(resized_height / 2)))

        # if make_margin == True:
        #     resizedImg = cv2.copyMakeBorder(resizedImg, margin_y, margin_y, margin_x, margin_x, cv2.BORDER_CONSTANT)
        #     resized_width = resized_width + (margin_x * 2)
        #     resized_height = resized_height + (margin_y * 2)

        # video initialize
        if videoCnt == 0:
            fourcc = cv2.VideoWriter_fourcc(*'DIVX')

            if concat_h == True:
                out = cv2.VideoWriter(join(outputPath, videoName + '.avi'), fourcc, fps,
                                  (resized_width * 2, resized_height))
            else:
                out = cv2.VideoWriter(join(outputPath, videoName + '.avi'), fourcc, fps,
                                      (resized_width , resized_height * 2))

            videoCnt = 1
        # if print_filename == True:
        #     cv2.putText(resizedImg, onlyfiles[n], (15, 30), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255))
        if concat_h == True:
            out.write(cv2.hconcat([resizedImgA, resizedImgB]))
        else:
            out.write(cv2.vconcat([resizedImgA, resizedImgB]))

    print("\ndone")

    out.release()

    # if debug_mode == True:
    #     cv2.destroyAllWindows()


def tifToPng(path):
    outputPath = './'
    videoFolderTitle = 'png'
    cutPrefix = 'full'

    print("image path = " + imagePath)

    onlyfiles = [f for f in listdir(imagePath) if isfile(join(imagePath, f))]
    images = numpy.empty(len(onlyfiles), dtype=object)
    old_hist_1ch = 0
    old_hist_sobel = 0

    sceneCnt = 0
    folderPath = join(outputPath, videoFolderTitle, cutPrefix + '_%04d' % sceneCnt)
    makedirs(folderPath, 0o777, True)

    frame = 0

    resized_width = 0
    resized_height = 0
    cutting_proof = 0

    prevImg = 0

    numCuda = cv2.cuda.getCudaEnabledDeviceCount()
    if numCuda != 0:
        cv2.cuda.setDevice(0)

    sceneCnt = 0
    imgCnt = 0

    for n in range(0, len(onlyfiles)):
        # set size from source image
        img = cv2.imread(join(imagePath, onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
        height, width, channels = img.shape

        if resize_factor != 1:
            resized_width = int(width * resize_factor)
            resized_height = int(height * resize_factor)

            resizedImg = cv2.resize(img, (resized_width, resized_height))
        else:
            resizedImg = img

        if debug_mode is True:
            dispImg = cv2.resize(img, (int(resized_width / 2), int(resized_height / 2)))

        if make_margin == True:
            resizedImg = cv2.copyMakeBorder(resizedImg, y_margin, y_margin, x_margin, x_margin, cv2.BORDER_CONSTANT)
            resized_width = resized_width + (x_margin * 2)
            resized_height = resized_height + (y_margin * 2)

        if sceneCutting == True:
            # 1ch histogram
            hist = cv2.calcHist([resizedImg], [0], None, [64], [0, 256])
            cv2.normalize(hist, hist, 0, 100, cv2.NORM_MINMAX)

            sum = 0
            if old_hist_1ch != 0:
                for i in range(0, len(hist)):
                    sum = sum + abs(hist[i] - old_hist_1ch[i])

            old_hist_1ch = hist

            # 3ch histogram

            # not yet

            # edge histogram
            gray = cv2.cvtColor(resizedImg, cv2.COLOR_BGR2GRAY)
            gray = cv2.GaussianBlur(gray, (3, 3), 0)
            sobelX = cv2.Sobel(gray, -1, 1, 0)
            sobelY = cv2.Sobel(gray, -1, 0, 1)

            sobel = abs(sobelX) + abs(sobelY)

            hist_sobel = cv2.calcHist([sobel], [0], None, [64], [0, 256])
            cv2.normalize(hist_sobel, hist_sobel, 0, 100, cv2.NORM_MINMAX)

            sum_sobel = 0
            if old_hist_sobel != 0:
                for i in range(0, len(hist_sobel)):
                    sum_sobel = sum_sobel + abs(hist_sobel[i] - old_hist_sobel[i])

            old_hist_sobel = hist_sobel

            if debug_mode is True:
                # cv2.imshow("sobel edge", sobel)
                # cv2.imshow('resized image', resizedImg)
                cv2.putText(prevImg, 'Previous Image', (15, 30), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0))
                cv2.imshow('Previous Image', prevImg)
                prevImg = dispImg.copy()
                cv2.putText(dispImg, 'Current Image, #' + str(frame), (15, 30), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0))
                cv2.putText(dispImg, "1ch histogram difference : %d" % sum, (15, 60), cv2.FONT_HERSHEY_PLAIN, 1,
                            (0, 255, 0))
                cv2.putText(dispImg, "edge histogram difference : %d" % sum_sobel, (15, 90), cv2.FONT_HERSHEY_PLAIN, 1,
                            (0, 255, 0))

                print("1ch histogram difference : %d" % sum)
                print("edge histogram difference : %d" % sum_sobel)

            if cutting_proof == 0:
                if int(sum) > 500:
                    # Manual selection off
                    # ch = cv2.waitKey(0)
                    # if ch is ord("y"):

                    # make new cut (folder)

                    # minimum frames
                    if imgCnt > 30:
                        sceneCnt = sceneCnt + 1
                        print("make new cut, cut num : %d" % sceneCnt)
                    else:
                        print("find new cut, but latest cut is too small")
                    folderPath = join(outputPath, videoFolderTitle, cutPrefix + '_%04d' % sceneCnt)
                    makedirs(folderPath, 0o777, True)

                    # image number reset
                    imgCnt = 0

                    if debug_mode is True:
                        cv2.putText(dispImg, "/////SSAK-DDUK//////", (15, 120), cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 255))
                        cv2.imshow('Current Image', dispImg)
                        cv2.waitKey(500)
                else:
                    if debug_mode is True:
                        cv2.imshow('Current Image', dispImg)
                        cv2.waitKey(int(1000 / fps))
            else:
                cutting_proof = cutting_proof - 1
                if debug_mode is True:
                    cv2.imshow('Current Image', dispImg)
                    cv2.waitKey(100)
        else:
            if print_filename == True:
                cv2.putText(resizedImg, onlyfiles[n], (15, 30), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255))

        fileName = join(folderPath, '%06d.png' % imgCnt)
        cv2.imwrite(fileName, resizedImg)
        imgCnt = imgCnt + 1
        frame = frame + 1


# test1:
#   name: Test\L
#   gt_seq_dir: X:\uuK_\APEX_20201211\dpx\S070_0015_077_SB1845\v002\L\png
#   lr_seq_dir: X:\uuK_\APEX_20201211\dpx\S070_0015_077_SB1845\v002\L\png
#   filter_file: ~
#   num_workers: 4
#   pin_memory: true

list_fileName = './testScript.txt'
qt_folders = {"lt", "rt", "lb", "rb"}


def _printTestScript2(path, f, title, subTitles, test_num):
    cur_folder_name = os.path.normpath(path)

    if title == "":
        title = cur_folder_name

    ## test num
    f.write("\ttest{}\n".format(test_num))
    ## name (and target folder)
    if option == "":
        f.write("\t\tname: {}\n".format(title))
    else:
        f.write("\t\tname: {}\\{}\n".format(title, option))
    ## source folder
    f.write("\t\tgt_seq_dir: {}\n".format(os.path.dirname(path)))
    f.write("\t\tlr_seq_dir: {}\n".format(os.path.dirname(path)))

    ## default info.
    f.write("\t\tfilter_file: ~\n")
    f.write("\t\tnum_workers: 4\n")
    f.write("\t\tpin_memory: true\n")
    f.write("\n")

    return test_num + 1


def _makeTestScript(path, title, subTitles, test_num):
    subFolders16 = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]

    subFolders16

    if len(onlyFolders) == 0:
        f = open(list_fileName, "at", encoding='utf-8')
        cur_folder_name = os.path.normpath(path)
        for folderPath in qt_folders:
            if cur_folder_name == folderPath:
                subTitles.append(cur_folder_name)

        test_num = _printTestScript2()
        f.close()
        return test_num

    else:
        for folderPath in onlyFolders:
            if len(onlyFolders) > 2:
                ## main folder name
                title = folderPath

            if folderPath == 'L' or folderPath == 'l':
                lr = folderPath
            elif folderPath == 'R' or folderPath == 'r':
                lr = folderPath

            base = os.path.join(path, folderPath)
            test_num = autoForTecoValidSets(fileName, base, title, lr, test_num)
        return test_num


def makeTestScript(path):
    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]

    for folderPath in onlyFolders:
        title = folderPath

        _makeTestScript(os.path.join(path, title), title)


def _checkprohibitedFolder(path):
    cur_folder_name = _getCurDirName(path)

    folderList = {"org", "lt", "rt", "lb", "rb"}
    flag = False
    for folder in folderList:
        if cur_folder_name == cur_folder_name:
            flag = True
            break

    return flag


def _printTestScript(path, f, title, option, test_num):
    cur_folder_name = os.path.normpath(path)

    if title == "":
        title = cur_folder_name

    ## test num
    f.write("  test%03d:\n" % test_num)
    ## name (and target folder)
    if len(option) == 0:
        f.write("    name: {}\n".format(title))
    else:
        f.write("    name: {}\\".format(title))
        for subFolder in option:
            f.write("{}\\".format(subFolder))
    ## source folder
    # f.write("\t\tgt_seq_dir: {}\n".format(os.path.dirname(path)))
    # f.write("\t\tlr_seq_dir: {}\n".format(os.path.dirname(path)))
    f.write("    gt_seq_dir: {}\n".format(os.path.abspath(path)))
    f.write("    lr_seq_dir: {}\n".format(os.path.abspath(path)))

    ## default info.
    f.write("    filter_file: ~\n")
    f.write("    num_workers: 4\n")
    f.write("    pin_memory: true\n")
    f.write("\n")

    return test_num + 1


def _printTestScript_header(list_path):
    f = open(list_path, "at", encoding='utf-8')

    f.write("# basic configs\n")
    f.write("scale: 4\n")
    f.write("manual_seed: 0\n")
    f.write("verbose: false\n\n\n")

    # dataset configs
    f.write("dataset:\n")
    f.write("  degradation:\n")
    f.write("    type: BD\n")
    f.write("    sigma: 1.5\n")

    f.close()


def _printTestScript_tail(list_path, output_path):
    f = open(list_path, "at", encoding='utf-8')

    f.write("# model configs\n")
    f.write("model:\n")
    f.write("  name: FRVSR\n\n\n")

    f.write("  generator:\n")
    f.write("    name: FRNet  # frame-recurrent network\n")
    f.write("    in_nc: 3\n")
    f.write("    out_nc: 3\n")
    f.write("    nf: 64\n")
    f.write("    nb: 10\n")
    f.write("    load_path: pretrained_models / TecoGAN_BD_iter500000.pth\n\n\n")

    f.write("# test configs\n")
    f.write("test:\n")
    f.write("# whether to save the SR results\n")
    f.write("#  save_res: true\n")
    f.write("#  res_dir: results\n")
    f.write("  save_res: true\n")
    f.write("  res_dir: %s\n\n\n" % output_path)

    f.write("  # temporal padding\n")
    f.write("  padding_mode: reflect\n")
    f.write("  num_pad_front: 5\n")
    f.write("# basic configs\n")
    f.write("scale: 4\n")
    f.write("manual_seed: 0\n")
    f.write("verbose: false\n\n\n")

    # dataset configs
    f.write("dataset:\n")
    f.write("  degradation:\n")
    f.write("    type: BD\n")
    f.write("    sigma: 1.5\n")

    f.close()


def _printTestScript_test(path, list_path, title, test_num, option=[]):
    cur_folder_name = os.path.normpath(path)

    if title == "":
        title = cur_folder_name

    f = open(list_path, "at", encoding='utf-8')

    ## test num
    f.write("  test%03d:\n" % test_num)
    ## name (and target folder)
    if len(option) == 0:
        f.write("    name: {}\n".format(title))
    else:
        f.write("    name: {}".format(title))
        for subFolder in option:
            f.write("\\{}".format(subFolder))
        f.write("\n")
    ## source folder
    # f.write("\t\tgt_seq_dir: {}\n".format(os.path.dirname(path)))
    # f.write("\t\tlr_seq_dir: {}\n".format(os.path.dirname(path)))
    f.write("    gt_seq_dir: {}\n".format(os.path.abspath(path)))
    f.write("    lr_seq_dir: {}\n".format(os.path.abspath(path)))

    ## default info.
    f.write("    filter_file: ~\n")
    f.write("    num_workers: 4\n")
    f.write("    pin_memory: true\n")
    f.write("\n")

    f.close()

    return test_num + 1


def makeTecoValidSet(path, f, title, option, test_num):
    onlyFiles = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    if len(onlyFiles) == 0:
        return test_num
    if _checkprohibitedFolder(path) == True:
        return test_num

    divide(path, False)
    split(join(path, 'lt'), 0.3, True)
    split(join(path, 'rt'), 0.3, True)
    split(join(path, 'lb'), 0.3, True)
    is_splited = split(join(path, 'rb'), 0.3, True)

    if is_splited == False:
        test_num = _printTestScript(path, f, title, option, test_num)
    else:
        test_num = _printTestScript(join(path, 'lt'), f, title, option.append('lt'), test_num)
        test_num = _printTestScript(join(path, 'rt'), f, title, option.append('rt'), test_num)
        test_num = _printTestScript(join(path, 'lb'), f, title, option.append('lb'), test_num)
        test_num = _printTestScript(join(path, 'rb'), f, title, option.append('rb'), test_num)

    return test_num


def autoForTecoValidSets(fileName, path, title, option, test_num):
    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]

    if len(onlyFolders) == 0:
        if title == "":
            title = os.path.basename(path)

        divideFor16(path, False)

        f = open(fileName, "at", encoding='utf-8')
        test_num = makeTecoValidSet(join(path, "1st_lt"), f, title, option, test_num)
        test_num = makeTecoValidSet(join(path, "1st_lb"), f, title, option, test_num)
        test_num = makeTecoValidSet(join(path, "1st_rt"), f, title, option, test_num)
        test_num = makeTecoValidSet(join(path, "1st_rb"), f, title, option, test_num)
        f.close()
        return test_num

    else:
        for folderPath in onlyFolders:
            if len(onlyFolders) > 2:
                if _IsThisSubFolder() == False:
                    ## main folder name
                    title = folderPath
                else:
                    if folderPath == 'L' or folderPath == 'l':
                        option.append(folderPath)
                    elif folderPath == 'R' or folderPath == 'r':
                        option.append(folderPath)

            base = os.path.join(path, folderPath)
            test_num = autoForTecoValidSets(fileName, base, title, option, test_num)
        return test_num


def gatherAllDividedFolders(path):
    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]

    for folderPath in onlyFolders:
        if divideFolder_prefix in folderPath:
            # divied folders in here
            gather(path, True)
            break
        else:
            gatherAllDividedFolders(os.path.join(path, folderPath))


def moveAll(src, dst):
    # move src/*.* to dst/*.*
    onlyFolders = [f for f in os.listdir(src) if not os.path.isfile(os.path.join(src, f))]

    for folderPath in onlyFolders:
        shutil.move(join(src, folderPath), join(dst, folderPath))

    onlyFiles = [f for f in os.listdir(src) if os.path.isfile(os.path.join(src, f))]

    for fileName in onlyFiles:
        shutil.move(join(src, fileName), join(dst, fileName))

    os.rmdir(src)


def gatherModelNameFolders(path):
    modelFolderRule = 'TecoGAN_'

    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]

    for folderPath in onlyFolders:
        if modelFolderRule in folderPath:
            # model folders in here
            moveAll(os.path.join(path, folderPath), path)
            break
        else:
            gatherModelNameFolders(os.path.join(path, folderPath))


def mergeDividedImages(path, del_mode):
    mergeTarget = {'lt', 'lb', 'rt', 'rb'}

    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]

    exist_flag = True
    for folderPath in mergeTarget:
        folderPath = os.path.join(path, folderPath)
        exist_flag = exist_flag and os.path.exists(folderPath)

    if exist_flag == False:
        for folderPath in onlyFolders:
            mergeDividedImages(os.path.join(path, folderPath), del_mode=del_mode)

    else:
        merge(path, del_mode=del_mode)


def mergeMergedImages(path):
    mergeTarget = {'1st_lt', '1st_lb', '1st_rt', '1st_rb'}

    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]

    exist_flag = True
    for folderPath in mergeTarget:
        folderPath = os.path.join(path, folderPath)
        exist_flag = exist_flag and os.path.exists(folderPath)

    if exist_flag == False:
        for folderPath in onlyFolders:
            mergeMergedImages(os.path.join(path, folderPath))

    else:
        for folderPath in mergeTarget:
            # makedirs(join(path, folderPath[4:6]), exist_ok=True)
            shutil.move(join(path, folderPath, 'merged'), join(path, folderPath[4:6]))
            # os.rename(os.path.join(path, 'merged'), os.path.join(path, folderPath[4:6]))

        merge(path, False)


def autoForTecoResultMerge(path, del_mode):
    # 1. find split folders and gather
    gatherAllDividedFolders(path)

    # 2. find model name folder and gather
    gatherModelNameFolders(path)

    # 3. find divided folder and merge
    mergeDividedImages(path, del_mode=del_mode)

    # 4. find divided from divieid folder and merge
    mergeMergedImages(path)


####
def _make_TargetFolderMap(path, folderMap, target_prefix, forbidden_words):
    onlyfolders = [f for f in listdir(path) if not isfile(join(path, f))]

    flag = False
    if flag == False:
        if len(onlyfolders) != 0:
            for folderName in onlyfolders:
                # forbidden words check for main folders
                forbidden_flag = False
                for forbidden_word in forbidden_words:
                    if forbidden_word in folderName:
                        forbidden_flag = True
                        break

                if forbidden_flag == False:
                    if target_prefix in folderName:
                        folderMap.append(join(path, folderName))
                    else:
                        _make_TargetFolderMap(join(path, folderName), folderMap, target_prefix, forbidden_words)

    return folderMap


def _mergeProc_for_mp(id, folderMap, start, end, del_mode=False, targetPath='merged'):
    imgCnt = 0

    if start < 0:
        start = 0

    if end > len(folderMap):
        end = len(folderMap)

    print('[Proc id : %d] Start')

    paths = {}
    for i in range(start, end):
        paths['lt'] = folderMap[i]
        pivot_path = paths['lt']

        paths['rt'] = paths['lt'].replace('\\lt\\', '\\rt\\')
        paths['rt'] = paths['rt'].replace('/lt/', '/rt/')

        paths['lb'] = paths['lt'].replace('\\lt\\', '\\lb\\')
        paths['lb'] = paths['lb'].replace('/lt/', '/lb/')

        paths['rb'] = paths['lt'].replace('\\lt\\', '\\rb\\')
        paths['rb'] = paths['rb'].replace('/lt/', '/rb/')

        onlyfiles = [f for f in listdir(pivot_path) if isfile(join(pivot_path, f))]

        if len(onlyfiles) == 0:
            print('there is no file in \'{}\' folder'.format(pivot_path))
            return -1

        if targetPath == '':
            outputPath = join(pivot_path, '..\\..\\..')
        else:
            #### temp for test
            outputPath = join(pivot_path, '..\\..\\..', targetPath, str(id))
            makedirs(outputPath, exist_ok=True)

        print("[Proc id : %d] Divided images from %s (pivot folder)" % (id, pivot_path))
        for n in range(0, len(onlyfiles)):
            # set size from source image
            filepath = join(pivot_path, onlyfiles[n])
            _, ext = splitext(filepath)

            if ext != '.png' and ext != '.tif':
                if del_mode == True:
                    if onlyfiles[n] == 'Thumbs.db':
                        os.remove(filepath)
                continue

            # print("\r[%d/%d] merging...(file name : %s)\r" % (n + 1, len(onlyfiles), onlyfiles[n]), end='')
            print("[Proc id : %d][%d/%d] merging...(file name : %s)" % (id, n + 1, len(onlyfiles), onlyfiles[n]))
            try:
                # left-top
                img_lt = cv2.imread(join(paths['lt'], onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
                if len(img_lt) == 0:
                    print("there is no file : {}".format(join(paths['lt'], onlyfiles[n])))
                    continue
                # right-top
                img_rt = cv2.imread(join(paths['rt'], onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
                if len(img_rt) == 0:
                    print("there is no file : {}".format(join(paths['rt'], onlyfiles[n])))
                    continue
                # left-bottom
                img_lb = cv2.imread(join(paths['lb'], onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
                if len(img_lb) == 0:
                    print("there is no file : {}".format(join(paths['lb'], onlyfiles[n])))
                    continue
                # right-bottom
                img_rb = cv2.imread(join(paths['rb'], onlyfiles[n]), flags=cv2.IMREAD_UNCHANGED)
                if len(img_rb) == 0:
                    print("there is no file : {}".format(join(paths['rb'], onlyfiles[n])))
                    continue
            except:
                print('some error has occured, file name : %s' % onlyfiles[n])
                continue

            if make_margin == True:
                # TODO : blend margin
                x_margin = x_margin
                y_margin = y_margin

            # merge
            img_upper = cv2.hconcat([img_lt, img_rt])
            img_lower = cv2.hconcat([img_lb, img_rb])
            img = cv2.vconcat([img_upper, img_lower])

            # fileName = join(outputPath, '%06d.png' % imgCnt)
            fileName = join(outputPath, onlyfiles[n])
            cv2.imwrite(fileName, img)

            if del_mode == True:
                del_path = join(path, 'lt', onlyfiles[n])
                if os.path.isfile(del_path) == True:
                    os.remove(del_path)
                del_path = join(path, 'rt', onlyfiles[n])
                if os.path.isfile(del_path) == True:
                    os.remove(del_path)
                del_path = join(path, 'lb', onlyfiles[n])
                if os.path.isfile(del_path) == True:
                    os.remove(del_path)
                del_path = join(path, 'rb', onlyfiles[n])
                if os.path.isfile(del_path) == True:
                    os.remove(del_path)

            imgCnt = imgCnt + 1

        if del_mode == True:
            src_folders = {'lt', 'rt', 'lb', 'rb'}
            for subFolder in src_folders:
                onlyfiles = [f for f in listdir(join(path, subFolder)) if isfile(join(path, subFolder, f))]
                if len(onlyfiles) == 0:
                    os.rmdir(join(path, subFolder))
                else:
                    print('Cannot delete %s folder, there is not empty folder. check inside' % subFolders)


    print('[Proc id : %d] End' % id)


def autoForTecoResultMerge_opt(path, additional_forbidden_words=[], del_mode=False, num_multiProc=1):
    ts = timeStamp.timeStamper
    # ts.start()
    # for make pivot ('lt') folder map
    forbidden_words = ['lb', 'rt', 'rb']

    # 1. make folder map for splited image folders
    folderMap = []
    pivot_folderMap = _make_TargetFolderMap(path, folderMap, divideFolder_prefix, forbidden_words)

    # 2. merge images with multiprocessing
    procs = []
    step = int(len(folderMap) / num_multiProc)
    for idx in range(0, num_multiProc):
        start = idx * step
        end = (idx + 1) * step

        proc = mp.Process(target=_mergeProc_for_mp, args=(idx, pivot_folderMap, start, end, del_mode))
        procs.append(proc)
        proc.start()

    for proc in procs:
        proc.join()

    # print('record : %.3f' %ts.lap())

resizefolderPath = 'resized'
# resizefolderPath = ''
def resizeAll(path, ratio, overwrite=False, w_target=0):
    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]

    if len(onlyFolders) != 0:
        for folderPath in onlyFolders:
            newPath = join(path, folderPath)

            resizeAll(newPath, ratio, overwrite, w_target=w_target)
    else:
        onlyFiles = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
        if len(onlyFiles) != 0:
            print("Folder path : %s" % path)

            if overwrite == True:
                dstFolderPath = path
            else:
                resizefolderPath = _getCurDirName(path)
                dstFolderPath = join(path, resizefolderPath)
                makedirs(dstFolderPath, exist_ok=True)

            for i in range(0, len(onlyFiles)):
                filename = onlyFiles[i]
                _, ext = os.path.splitext(filename)

                if ext == '.png' or ext == '.jpg' or ext == '.tif' or ext == '.tiff':
                    srcPath = join(path, filename)
                    dstPath = join(dstFolderPath, filename)

                    try:
                        src = cv2.imread(srcPath, flags=cv2.IMREAD_UNCHANGED)
                        h, w, _ = src.shape
                    except:
                        print('some error has occured, file path : %s' %srcPath)
                        continue
                    if w_target!=0:
                        if w != w_target:
                            continue
                    if ratio != 1:
                        targetH = int(h * ratio)
                        targetW = int(w * ratio)

                    if targetW != 0 and targetH != 0:
                        if ratio > 1:
                            dst = cv2.resize(src, dsize=(int(targetW), int(targetH)), interpolation=cv2.INTER_CUBIC)
                        else:
                            dst = cv2.resize(src, dsize=(int(targetW), int(targetH)), interpolation=cv2.INTER_AREA)
                    else:
                        dst = src

                    cv2.imwrite(dstPath, dst)

                    print("\rDownsizing ... [% 6d / % 6d]" % (i + 1, len(onlyFiles)), end='')
                else:
                    print("It's not a image file. filename : %s" % filename)

            print("\n")

def _resizer_mp(id, folder_map, start, end, ratio, overwrite=False, w_target=0):
    if start < 0:
        start = 0
    if end >= len(folder_map):
        end = len(folder_map)

    for i in range(start, end):
        path = folder_map[i]

        onlyFiles = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
        if len(onlyFiles) != 0:
            print("[Proc id : %d] Folder path : %s" % (id, path))

            if overwrite == True:
                dstFolderPath = path
            else:
                resizefolderPath = _getCurDirName(path)
                dstFolderPath = join(path, resizefolderPath)
                makedirs(dstFolderPath, exist_ok=True)

            for i in range(0, len(onlyFiles)):
                filename = onlyFiles[i]
                _, ext = os.path.splitext(filename)

                if ext == '.png' or ext == '.jpg' or ext == '.tif' or ext == '.tiff':
                    srcPath = join(path, filename)
                    dstPath = join(dstFolderPath, filename)

                    try:
                        src = cv2.imread(srcPath, flags=cv2.IMREAD_UNCHANGED)
                        h, w, _ = src.shape
                    except:
                        print('[Proc id : %d] some error has occured, file path : %s' % (id, srcPath))
                        continue
                    if w_target != 0:
                        if w != w_target:
                            continue
                    if ratio != 1:
                        targetH = int(h * ratio)
                        targetW = int(w * ratio)

                    if targetW != 0 and targetH != 0:
                        if ratio > 1:
                            dst = cv2.resize(src, dsize=(int(targetW), int(targetH)), interpolation=cv2.INTER_CUBIC)
                        else:
                            dst = cv2.resize(src, dsize=(int(targetW), int(targetH)), interpolation=cv2.INTER_AREA)
                    else:
                        dst = src

                    cv2.imwrite(dstPath, dst)

                    print("\r[Proc id : %d] Downsizing ... [% 6d / % 6d]" % (id, i + 1, len(onlyFiles)), end='')
                else:
                    print("[Proc id : %d] It's not a image file. filename : %s" % (id, filename))

            print("\n")


def resizeAll_opt(path, ratio, overwrite=False, w_target=0, num_multiProc=4):
    # 1. make folder map for splited image folders
    forbidden_words = ['merged']
    folderMap = []
    pivot_folderMap = _make_TargetFolderMap(path, folderMap, divideFolder_prefix, forbidden_words)

    # 2. merge images with multiprocessing
    procs = []
    step = int(len(folderMap) / num_multiProc)
    for idx in range(0, num_multiProc):
        start = idx * step
        end = (idx + 1) * step
        proc = mp.Process(target=_resizer_mp, args=(idx, pivot_folderMap, start, end, ratio, overwrite, w_target))
        procs.append(proc)
        proc.start()

    for proc in procs:
        proc.join()


# def blurFilter(src):
def blurFilter(path):
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

    if len(onlyfiles) != 0:
        param1 = 3
        param2 = param1
        param3 = 0
        folder1 = join(path, "..", 'gaussian_%d-%d-%d' % (param1, param2, param3))
        makedirs(folder1, exist_ok=True)

        param4 = 5
        param5 = param4
        param6 = 0
        folder2 = join(path, "..", 'gaussian_%d-%d-%d' % (param4, param5, param6))
        makedirs(folder2, exist_ok=True)

        param7 = 5
        param8 = 25
        param9 = param8
        folder3 = join(path, "..", 'biliteral_%d-%d-%d.png' % (param7, param8, param9))
        makedirs(folder3, exist_ok=True)

        for fileName in onlyfiles:
            src = cv2.imread(join(path, fileName), flags=cv2.IMREAD_UNCHANGED)

            dst = cv2.GaussianBlur(src, (param1, param2), param3)
            cv2.imwrite(join(folder1, fileName), dst)

            dst = cv2.GaussianBlur(src, (param4, param5), param6)
            cv2.imwrite(join(folder2, fileName), dst)

            dst = cv2.bilateralFilter(src, param7, param8, param9)
            cv2.imwrite(join(folder3, fileName), dst)


def _IsThisImageFile(ext):
    imageFileExts = {".png", ".jpg", ".tif", ".tiff", ".TIF", ".TIFF"}

    flag = False
    for imageExt in imageFileExts:
        if ext == imageExt:
            flag = True

    return flag


def _make_imageFolderMap(path, folderMap, additional_forbidden_words=[]):
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    onlyfolders = [f for f in listdir(path) if not isfile(join(path, f))]

    forbidden_words = []

    for forbidden_key in additional_forbidden_words:
        forbidden_words.append(forbidden_key)

    flag = False
    if len(onlyfiles) != 0:
        flag = True
        for filename in onlyfiles:
            if filename == 'Thumbs.db':
                continue

            _, ext = os.path.splitext(filename)
            flag = flag and _IsThisImageFile(ext)

        if flag == True:
            # image folder has only image files
            folderName = _getCurDirName(path)

            # forbidden words check for image folder
            forbidden_flag = False
            for forbidden_word in forbidden_words:
                if forbidden_word in folderName:
                    forbidden_flag = True
                    break
            if forbidden_flag == False:
                print('[%03d] add tartget folder : %s' %(len(folderMap) + 1, path))
                folderMap.append(path)

    if flag == False:
        if len(onlyfolders) != 0:
            for folderPath in onlyfolders:
                # forbidden words check for main folders
                folderName = folderPath
                forbidden_flag = False
                for forbidden_word in forbidden_words:
                    if forbidden_word in folderName:
                        forbidden_flag = True
                        break
                if forbidden_flag == False:
                    _make_imageFolderMap(join(path, folderPath), folderMap, forbidden_key)

    return folderMap


def _getSampleImage(path):
    onlyfolders = [f for f in listdir(path) if not isfile(join(path, f))]

    if len(onlyfolders) != 0:
        for foldername in onlyfolders:
            img = _getSampleImage(join(path, foldername))

            if len(img) != 0:
                return img

    else:
        onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

        if len(onlyfiles) != 0:
            flag = False
            for filename in onlyfiles:
                _, ext = os.path.splitext(filename)
                flag = _IsThisImageFile(ext)
                if flag == True:
                    break

            if flag == True:
                return cv2.imread(join(path, filename), flags=cv2.IMREAD_UNCHANGED)

        return []


def _autoForTecoValidSets(path, blur=False, list_path='', splitFirst=False, splitLimit=0.2, forbidden_key='', divideMode=4, complete_marking=True):
    test_num = 0
    print('====================================================')
    print('Run auto-generation scrip for TecoGan Validation set')
    print('Target path : ', path)
    print('Divide mode : ', divideMode)
    print('Split size : %.3f GB', splitLimit)
    print('====================================================')
    print('')

    forbidden_words = ['Bak', 'lt', 'lb', 'rt', 'rb']
    if forbidden_key != '':
        forbidden_words.append(forbidden_key)

    # 1. make map
    folderMap = []
    folderMap = _make_imageFolderMap(path, folderMap, forbidden_words)

    # 2. check image size
    # TODO : size check
    divideMode = 4

    # 3. make validation set
    if list_path == '':
        # make list file to default path
        list_path = join(path, 'list.txt')

    ## check image size for dividing
    if len(folderMap) != 0:
        img = _getSampleImage(folderMap[0])

        if len(img) != 0:
            height, width, _ = img.shape
            bit = img.dtype
        else:
            print('there is no image\npath : %s' % folderMap[0])
            return -1

    if divideMode == 16:
        # 1/16 divide
        for folderPath in folderMap:
            divideFor16(folderPath, makeBlurForMpeg=blur)

            for subFolder16 in subFolders16:
                divide(join(folderPath, subFolder16))
                is_split = False
                for subFolder in subFolders:
                    is_split = split(join(folderPath, subFolder16, subFolder), splitLimit, True)
                    if is_split == True:
                        test_num = _printTestScript_test(join(folderPath, subFolder16, subFolder), list_path,
                                                         _getCurDirName(folderPath), test_num, {subFolder16, subFolder})

                if is_split == False:
                    test_num = _printTestScript_test(join(folderPath, subFolder16), list_path,
                                                     _getCurDirName(folderPath), test_num, subFolders16)
    elif divideMode == 4:
        # 1/4 divide
        for folderPath in folderMap:
            backup_postfix = '_orgBak'
            try:
                divide(folderPath, backup_postfix=backup_postfix)
            except:
                ##TODO roll-back
                # backup_postfix
                print('cannot complete divide job')

            is_split = False
            for subFolder in subFolders:
                is_split = split(join(folderPath, subFolder), splitLimit, True)
                if is_split == True:
                    test_num = _printTestScript_test(join(folderPath, subFolder), list_path, _getCurDirName(folderPath),
                                                     test_num, {subFolder})

            if is_split == False:
                test_num = _printTestScript_test(join(folderPath, subFolder), list_path, _getCurDirName(folderPath),
                                                 test_num)

            if complete_marking == True:
                # makedirs(path, exist_ok=True)
                os.rename(folderPath, folderPath + '_done')
    else:
        # no divide
        for folderPath in folderMap:
            print("TODO")


def _reMergeAll(path):
    onlyfolders = [f for f in listdir(path) if not isfile(join(path, f))]

    flag = False
    for folderPath in onlyfolders:
        if '1st' in folderPath:
            flag = True

    if flag == True:
        folderPath = '1st_lt'
        merge(join(path, folderPath), targetPath="")
        os.rename(join(path, folderPath), join(path, folderPath[4:6]))

        folderPath = '1st_lb'
        merge(join(path, folderPath), targetPath="")
        os.rename(join(path, folderPath), join(path, folderPath[4:6]))

        folderPath = '1st_rt'
        merge(join(path, folderPath), targetPath="")
        os.rename(join(path, folderPath), join(path, folderPath[4:6]))

        folderPath = '1st_rb'
        merge(join(path, folderPath), targetPath="")
        os.rename(join(path, folderPath), join(path, folderPath[4:6]))

        merge(path, targetPath=_getCurDirName(path))

    else:
        for folderPath in onlyfolders:
            _reMergeAll(join(path, folderPath))


def _resizeAll(path, targetPath='', targetW=0, targetH=0, ratio=1, mask=False, overwrite=False):
    onlyfolders = [f for f in listdir(path) if not isfile(join(path, f))]

    # if len(onlyfolders) == 0:
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

    if overwrite == True:
        targetPath = path
    else:
        if targetPath == '':
            targetPath = join(path, 'resized')
        makedirs(targetPath, exist_ok=True)

    for fileName in onlyfiles:
        # img = cv2.imread(join(path, fileName), flags=cv2.IMREAD_UNCHANGED)
        img = cv2.imread(join(path, fileName))
        try:
            h, w, _ = img.shape
        except:
            # print('It is not Image file (maybe?)')
            return -1
        # resized = cv2.resize(img, dsize=(int(1920), int(1024)))

        if ratio != 1:
            targetH = int(h * ratio)
            targetW = int(w * ratio)

        if targetW != 0 and targetH != 0:
            if ratio > 1:
                resized = cv2.resize(img, dsize=(int(targetW), int(targetH)), interpolation=cv2.INTER_CUBIC)
            else:
                resized = cv2.resize(img, dsize=(int(targetW), int(targetH)), interpolation=cv2.INTER_AREA)
        else:
            resized = img

        if mask == True:
            y = 0
            for line in resized:
                x = 0
                for pixel in line:
                    b, g, r = pixel

                    if g == 0 and b == 0 and r == 0:
                        # pixel for inpainting, set mask color
                        resized[y][x][0] = 0
                        resized[y][x][1] = 0
                        resized[y][x][2] = 0

                    else:
                        resized[y][x][0] = 255
                        resized[y][x][1] = 255
                        resized[y][x][2] = 255

                    x = x + 1
                y = y + 1

        name, ext = os.path.splitext(fileName)

        cv2.imwrite(join(targetPath, name + '.png'), resized)

    # else:
    #     for folderName in onlyfolders:
    #         _resizeAll(join(path, folderName))


def sampling(path, targetPath, unit):
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

    if targetPath == '':
        targetPath = join(path, 'sampled')
    makedirs(targetPath, exist_ok=True)

    cnt = unit - 1
    for fileName in onlyfiles:
        if cnt >= unit - 1:
            shutil.copy(join(path, fileName), join(targetPath, fileName))
            cnt = 0
        else:
            cnt += 1


def maskMerge(src_path, dst_path):
    path = src_path
    onlyfolders = [f for f in listdir(path) if not isfile(join(path, f))]

    pivots = []
    folderNames = []
    fileLists = []

    for foldername in onlyfolders:
        path = join(src_path, foldername)
        onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

        pivots.append(0)
        folderNames.append(path)
        fileLists.append(onlyfiles)

    flag = True
    while (flag == True):
        imgNumber = []
        for i in range(0, len(pivots)):
            pivot = pivots[i]
            imgNumber.append(fileLists[i][pivot] - '.png')


def makeCutTransition(path, map={}, numberRule='Last', digitLength=6, makeCutVideo=True):
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

    vid_path = '../vid'

    folder_no = 0
    outpath = join(path, '%03d' %folder_no)
    makedirs(outpath, exist_ok=True)

    pivot = 0
    start = map[pivot]

    if map == None or len(map) ==0:
        print('error, map is empty!')
        return -1
    elif len(map) > 1:
        end = map[pivot+1]
    else:
        # set max num for digit length
        end = 1000000

    for filename in onlyfiles:
        name, ext = splitext(filename)
        try:
            no = name[len(name) - 6:]
            no = int(no)
        except:
            continue

        if start <= no and no < end:
            src = join(path, filename)
            dst = join(outpath, filename)
            print('file move [%s] to [%s]' %(src, dst))
            shutil.move(src, dst)
        elif no >= end:
            pivot = pivot + 1
            if pivot < len(map):
                if makeCutVideo == True:
                    makeVideo(outpath, '%03d' % folder_no, 24000/1001, output=vid_path)

                ## start new folder
                folder_no = folder_no + 1
                outpath = join(path, '%03d' % folder_no)
                makedirs(outpath, exist_ok=True)

                start = map[pivot]
            else:
                print('done')
                break
            if pivot + 1 < len(map):
                end = map[pivot + 1]
            else:
                # set max num for digit length
                end = 1000000

    #
    # for start in map:
    #     start = int(start)


def gather_custom(path):

    image_path = join(path, 'images')
    video_path = join(path, 'videos')
    zip_path = join(path, 'zips')
    else_path = join(path, 'else')

    makedirs(image_path, exist_ok=True)
    makedirs(video_path, exist_ok=True)
    makedirs(zip_path, exist_ok=True)
    makedirs(else_path, exist_ok=True)

    image_cnt = 0

    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]
    for n in range(0, len(onlyFolders)):
        folderPath = os.path.join(path, onlyFolders[n])
        onlyFiles = [f for f in os.listdir(folderPath) if os.path.isfile(os.path.join(folderPath, f))]

        for m in range(0, len(onlyFiles)):
            filePath = os.path.join(folderPath, onlyFiles[m])
            _, ext = os.path.splitext(filePath)
            if ext.lower == '.bmp' or ext.lower == '.jpg' or ext.lower == '.jpeg' or ext.lower == '.tif' or ext.lower == '.tiff' or ext.lower == '.png':
                continue

            shutil.move(filePath, os.path.join(path, onlyFiles[m]))
            print("\rcopy {} to {}".format(filePath, (os.path.join(path, onlyFiles[m]))), end='')


def spliter(path, limit=10000, limit_size_or_number=True, is_unzipsize=True, prev):
    start_num = 0

    # limit = 1.0  # giga-byte
    limit = limit * 1024 * 1024 * 1024  # byte

    onlyFiles = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

    accumulatedByte = 0
    nFolders = 0
    holdPath = ""
    holdFileName = ""
    holdByte = 0

    for n in range(0, len(onlyFiles)):
        if accumulatedByte == 0:
            # make new folder
            targetFolderPath = os.path.join(path, divideFolder_prefix + '%03d' % (start_num + nFolders))
            while (os.path.exists(targetFolderPath)):
                print("{} is already exist!!".format(targetFolderPath))
                nFolders = nFolders + 1
                targetFolderPath = os.path.join(path, divideFolder_prefix + '%03d' % (start_num + nFolders))
            os.makedirs(targetFolderPath, exist_ok=True)
            nFolders = nFolders + 1

            if holdPath != "":
                shutil.move(holdPath, os.path.join(targetFolderPath, holdFileName))
                print("\rcopy {} to {}".format(filePath, (os.path.join(targetFolderPath, holdFileName))), end='')

                accumulatedByte = holdByte

        filePath = os.path.join(path, onlyFiles[n])

        _, ext = os.path.splitext(filePath)
        if ext == '.py':
            continue
        if is_unzipsize == True:
            img = cv2.imread(filePath, flags=cv2.IMREAD_UNCHANGED)
            byte = sys.getsizeof(img)
        else:
            byte = os.path.getsize(filePath)

        if byte + accumulatedByte > limit:
            if byte > limit:
                print("{} is too big to copy for the current limitation (limit : {} GB, file size : {:.3f} GB)".format(
                    onlyFiles[n], limit / (1024.0 * 1024.0 * 1024.0), byte / (1024.0 * 1024.0 * 1024.0)))

            holdPath = filePath
            holdFileName = onlyFiles[n]
            holdByte = byte

            print("{} : {:.3f} GB".format(targetFolderPath, accumulatedByte / (1024.0 * 1024.0 * 1024.0)))

            accumulatedByte = 0
        else:
            shutil.move(filePath, os.path.join(targetFolderPath, onlyFiles[n]))
            print("\rcopy {} to {}".format(filePath, (os.path.join(targetFolderPath, onlyFiles[n]))), end='')

            accumulatedByte = accumulatedByte + byte

    if (start_num + nFolders) == 1:
        # if there is the only folder, don't make divided folder
        gather(path, True)
        is_divided = False
    else:
        is_divided = True

    return is_divided



if __name__ == '__main__':
    ## run with custom args
    # auto = 'TecoResultMerge'
    # auto = 'TecoValidSet'
    auto = ''
    # mode = 'resize'
    mode = ''
    path = r'F:\res\EA0053824004'
    # path = r'D:\results\ENM\CE0004341A03'
    # path = ''
    # param1 = '0.5'
    param1 = True
    # param1 = ''

#     root = r'G:\ValidationData_Temp\RUSTCITY\DCDM(SX_SCENE)'
#
#     map = [354109,
# 354439,
# 354556,
# 354572,
# 354591,
# 354617,
# 354665,
# 354706,
# 354758,
# 355089,
# 355225,
# 355305,
# 355334,
# 355381,
# 355470,
# 355491,
# 355532,
# 355662,
# 356172,
# 356329,
# 356369,
# 356772,
# 356836,
# 356887,
# 356936,
# 357390,
# 357886,
# 357973,
# 358034,
# 358152,
# 358318,
# 358364,
# 358705,
# 359256,
# 359279]
#     src = r'rus_2d_r04_v1708_1015_201113_10mj_g_xyz(sx2)'
#     post_fix = '2048x858'
#     path = join(root, src, post_fix)
#     makeCutTransition(path, map)

def maskMerge(src_path, dst_path):
    path = src_path
    onlyfolders = [f for f in listdir(path) if not isfile(join(path, f))]

    pivots = []
    folderNames = []
    fileLists = []

    for foldername in onlyfolders:
        path = join(src_path, foldername)
        onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

        pivots.append(0)
        folderNames.append(path)
        fileLists.append(onlyfiles)

    flag = True
    while (flag == True):
        imgNumber = []
        for i in range(0, len(pivots)):
            pivot = pivots[i]
            imgNumber.append(fileLists[i][pivot] - '.png')



if __name__ == '__main__':
    ## run with custom args
    # auto = 'TecoResultMerge'
    # auto = 'TecoValidSet'
    auto = ''
    # mode = 'resize'
    mode = ''
    path = r'G:\ENM\res\EA0048025002'
    # path = r'D:\results\ENM\CE0004341A03'
    # path = ''
    # param1 = '0.5'
    param1 = True
    # param1 = ''

    makeCompVideo(r"C:\Users\DoubleUK\Desktop\Moving\47\org", r"C:\Users\DoubleUK\Desktop\Moving\47\seg", concat_h=False)


    import argparse

    parser = argparse.ArgumentParser()
    ## common options
    parser.add_argument('--auto', type=str, default='',
                        help='auto combination for something, option : TecoValidSet, TecoResultMerge #TODO TecoTraining')
    parser.add_argument('--mode', type=str, default='', help='divide, merge, resize, rename, split and gather')
    parser.add_argument('--path', type=str, default='./', help='target folder path')
    # parser.add_argument('--path', type=str, default='E:/test')
    parser.add_argument('--debug_mode', type=bool, default=False, help='True, False')
    ## resize options
    parser.add_argument('--size', type=float, default=1.0, help='resize target magnification')
    parser.add_argument('--width', type=int, default=0, help='resize/margin width')
    parser.add_argument('--height', type=int, default=0, help='resize/margin height')
    ## split options
    parser.add_argument('--limit', type=float, default=0.2, help='split unit (GB)')
    parser.add_argument('--unzipsize', type=bool, default=True,
                        help='measure image size from their unzipped size (pixels * channels * depth')
    ## gather options
    parser.add_argument('--delfolder', type=bool, default=True, help='delete folder after gather ')
    ## rename options
    parser.add_argument('--startnum', type=int, default=801, help='renaming from this number')
    parser.add_argument('--prefix', type=str, default='', help='rename/video output prefix')
    parser.add_argument('--isfolder', type=bool, default=False, help='rename target (True = Folder, False = File)')
    ## makeVideoWithCut option

    parser.add_argument('--fps', type=int, default=24, help='fps for make video')

    parser.add_argument('--scene_cutting', type=bool, default=False, help='scene cutting algorithm on/off')
    parser.add_argument('--make_margin', type=bool, default=False, help='make video with margin')

    parser.add_argument('--unit', type=int, default=24, help='sampling unit')

    args = parser.parse_args()

    if args.mode == '' and args.auto == '':
        # run with pycharm
        try:
            args.auto = auto
            args.mode = mode
            args.path = path
        except:
            print("customized running set-up, program will be terminated.")

    ## auto mode
    if args.auto == 'TecoValidSet':
        fileName = join(path, "list.txt")
        if os.path.exists(fileName) == True:
            os.remove(fileName)
        # autoForTecoValidSets(fileName, args.path, "", "", 0)
        _autoForTecoValidSets(args.path, list_path=fileName, splitLimit=0.2, forbidden_key=param1)
    elif args.auto == 'TecoValidSet8':
        divideFor16(args.path, args.debug_mode)

        fileName = "./list.txt"
        if os.path.exists(fileName) == True:
            os.remove(fileName)
        autoForTecoValidSets(fileName, args.path, "", "", 0)
    elif args.auto == 'TecoResultMerge':
        autoForTecoResultMerge(args.path, param1)
    else:
        ## normal mode
        if args.mode == 'divide':
            divide(args.path, args.debug_mode)
        elif args.mode == 'divide8':
            divideFor16(args.path, args.debug_mode)
        elif args.mode == 'merge':
            merge(args.path, args.debug_mode)
        elif args.mode == 'resize':
            _resizeAll(path, ratio=param1)
        elif args.mode == 'split':
            if args.limit == 0.0:
                print('split function need limitation, input limit by use --limit option')
            else:
                split(args.path, args.limit, args.unzipsize)
        elif args.mode == 'gather':
            gather(args.path, args.delfolder)
        elif args.mode == 'rename':
            rename(args.path, args.startnum, args.prefix, args.isfolder)
        elif args.mode == 'makeVideoWithCut':
            makeVideoWithCut(args.path, args.prefix, args.fps, args.size, args.scene_cutting, args.make_margin,
                             args.width, args.height, args.debug_mode)
        elif args.mode == 'sampling':
            sampling(args.path, args.unit)
        else:
            raise Exception("Unknow --mode")




    import argparse

    parser = argparse.ArgumentParser()
    ## common options
    parser.add_argument('--auto', type=str, default='',
                        help='auto combination for something, option : TecoValidSet, TecoResultMerge #TODO TecoTraining')
    parser.add_argument('--mode', type=str, default='', help='divide, merge, resize, rename, split and gather')
    parser.add_argument('--path', type=str, default='./', help='target folder path')
    # parser.add_argument('--path', type=str, default='E:/test')
    parser.add_argument('--debug_mode', type=bool, default=False, help='True, False')
    ## resize options
    parser.add_argument('--size', type=float, default=1.0, help='resize target magnification')
    parser.add_argument('--width', type=int, default=0, help='resize/margin width')
    parser.add_argument('--height', type=int, default=0, help='resize/margin height')
    ## split options
    parser.add_argument('--limit', type=float, default=0.2, help='split unit (GB)')
    parser.add_argument('--unzipsize', type=bool, default=True,
                        help='measure image size from their unzipped size (pixels * channels * depth')
    ## gather options
    parser.add_argument('--delfolder', type=bool, default=True, help='delete folder after gather ')
    ## rename options
    parser.add_argument('--startnum', type=int, default=801, help='renaming from this number')
    parser.add_argument('--prefix', type=str, default='', help='rename/video output prefix')
    parser.add_argument('--isfolder', type=bool, default=False, help='rename target (True = Folder, False = File)')
    ## makeVideoWithCut option

    parser.add_argument('--fps', type=int, default=24, help='fps for make video')
    parser.add_argument('--scene_cutting', type=bool, default=False, help='scene cutting algorithm on/off')
    parser.add_argument('--make_margin', type=bool, default=False, help='make video with margin')

    parser.add_argument('--unit', type=int, default=24, help='sampling unit')

    args = parser.parse_args()

    if args.mode == '' and args.auto == '':
        # run with pycharm
        try:
            args.auto = auto
            args.mode = mode
            args.path = path
        except:
            print("customized running set-up, program will be terminated.")

    ## auto mode
    if args.auto == 'TecoValidSet':
        fileName = join(path, "list.txt")
        if os.path.exists(fileName) == True:
            os.remove(fileName)
        # autoForTecoValidSets(fileName, args.path, "", "", 0)
        _autoForTecoValidSets(args.path, list_path=fileName, splitLimit=0.2, forbidden_key=param1)
    elif args.auto == 'TecoValidSet8':
        divideFor16(args.path, args.debug_mode)

        fileName = "./list.txt"
        if os.path.exists(fileName) == True:
            os.remove(fileName)
        autoForTecoValidSets(fileName, args.path, "", "", 0)
    elif args.auto == 'TecoResultMerge':
        autoForTecoResultMerge(args.path, param1)
    else:
        ## normal mode
        if args.mode == 'divide':
            divide(args.path, args.debug_mode)
        elif args.mode == 'divide8':
            divideFor16(args.path, args.debug_mode)
        elif args.mode == 'merge':
            merge(args.path, args.debug_mode)
        elif args.mode == 'resize':
            _resizeAll(path, ratio=param1)
        elif args.mode == 'split':
            if args.limit == 0.0:
                print('split function need limitation, input limit by use --limit option')
            else:
                split(args.path, args.limit, args.unzipsize)
        elif args.mode == 'gather':
            gather(args.path, args.delfolder)
        elif args.mode == 'rename':
            rename(args.path, args.startnum, args.prefix, args.isfolder)
        elif args.mode == 'makeVideoWithCut':
            makeVideoWithCut(args.path, args.prefix, args.fps, args.size, args.scene_cutting, args.make_margin,
                             args.width, args.height, args.debug_mode)
        elif args.mode == 'sampling':
            sampling(args.path, args.unit)
        else:
            raise Exception("Unknow --mode")


