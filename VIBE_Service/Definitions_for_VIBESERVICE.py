SERVER_IP_PREFIX = "192.168.0."
SERVER_LIST = {
    "30",
    "32",
    "33"
}

SERVER_STATE_XML_PATH = r"c:\server_state_data.xml"

SERVER_STATE_IDLE = 0
SERVER_STATE_WORKING = 1
SERVER_STATE_DISCONNECTED = 2
SERVER_STATE_UNUSABLE = 3       # by user or other reason


GIT_VIBE_URL = "http://192.168.0.30/sxdev_git/VIBE.git"

ALGORITHM_CODE_SR = 0
ALGORITHM_CODE_FS = 1
ALGORITHM_CODE_CM = 2

# per MB
QUANTUM_SIZE = {
    200,    # for SR
    200,    # for FS
    200     # for CM
}

